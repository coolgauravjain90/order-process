function updateClock() {
                // Gets the current time
                var now = new Date();
 
                // Get the hours, minutes and seconds from the current time
                var hours = now.getHours();
                var minutes = now.getMinutes();
                var seconds = now.getSeconds();
 
                // Format hours, minutes and seconds
				if (hours==0) {
					hours=12;
				}
                if (hours < 10) {
                    hours = "0" + hours;
                }
                if (minutes < 10) {
                    minutes = "0" + minutes;
                }
                if (seconds < 10) {
                    seconds = "0" + seconds;
                }
				
				var wishTime="Good Morning!"
				if(hours>=12 && hours<17) wishTime="Good Afternoon!";
				if(hours>=17 && hours<24) wishTime="Good Evening!";
				
				var DayNight="PM"
				if (hours<12) DayNight="AM";
				if (hours>12) hours=hours-12;
 
                // Gets the element we want to inject the clock into
                var elem = document.getElementById('clock');
 
                // Sets the elements inner HTML value to our clock data
                elem.innerHTML = hours + ':' + minutes + ':' + seconds +" "+DayNight +" "+ '|' + " " + wishTime;
            }