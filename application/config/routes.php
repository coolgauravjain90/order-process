<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';

$route['login.php'] = 'login/index';
$route['loginValidate.php'] = 'login/validate';
$route['logout.php'] = 'login/logout';
$route['home.php'] = 'login/home';
$route['invoice_edit.php'] = 'edit_invoice/index';

$route['purchase_view.php'] = 'purchase_product/index';

$route['allinvoice.php'] = 'invoice_detail/index';

$route['memo_purchase_product.php'] = 'credit_memo/index';
$route['view_credit_memo.php'] = 'credit_memo/view_credit_memo_invoice_items';

$route['purchaseform.php'] = 'purchase_invoice_entry/index';
$route['purchaseform_one.php'] = 'purchase_invoice_entry/purchaseform_one';
$route['purchaseform_two.php'] = 'purchase_invoice_entry/purchaseform_two';

$route['discount_rule.php'] = 'discount_rule_table/index';
$route['create_discount_rule.php'] = 'discount_rule_table/create_discount_rule';

$route['orders.php'] = 'c_order_process/index';
$route['ready_to_ship_orders.php'] = 'c_order_process/get_ready_to_ship_orders';
$route['pending_orders.php'] = 'c_order_process/get_pending_orders';

$route['order_items.php'] = 'c_order_process/order_items';
$route['pending_order_items.php'] = 'c_order_process/order_pending_order_items';
$route['procured_order_items.php'] = 'c_order_process/order_procured_order_items';

$route['pending_export.php'] = 'c_order_process/export_excel_pending_order_items';
$route['export_ready_to_ship_orders.php'] = 'c_order_process/export_ready_to_ship_orders';

$route['pending_email.php'] = 'c_order_process/pending_orders_send_mail';

$route['foreign_orders.php'] = 'c_order_process/get_foreign_orders';

$route['holded_orders.php'] = 'c_order_process/get_holded_orders';

$route['pending_shipment.php'] = 'c_shipment/get_pending_shipment';
$route['delivered_shipment.php'] = 'c_shipment/get_delivered_shipment';
$route['returned_shipment.php'] = 'c_shipment/get_returned_shipment';

$route['manage_products.php'] = 'c_manage_products/manage_products';
$route['manage_brands.php'] = 'c_manage_products/manage_brands';
$route['manage_inventory.php'] = 'c_manage_products/manage_all_inventory';

$route['manage_discount_rule.php'] = 'c_manage_suppliers/manage_discount_rules';
$route['manage_suppliers.php'] = 'c_manage_suppliers/manage_suppliers';


/* End of file routes.php */
/* Location: ./application/config/routes.php */