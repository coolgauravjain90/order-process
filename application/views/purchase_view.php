<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Purchase Products</title>
<div class="home-title blue-gradient">Purchase View
</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_purchase_product');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td>SKU: <input type="text" id="search_sku" name="search_sku" /></td>
	<td>Name: <input type="text" id="search_name" name="search_name" /></td>
	<td>Brand: <input type="text" id="search_brand" name="search_brand" /></td>
	<td>Invoice No: <input type="text" id="search_invoice_id" name="search_invoice_id" /></td>
	<td>Purchase Date: <input type="text" id="search_date" name="search_date" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$(document).on('keyup change', '#search_purchase_product', function(){
 var search_sku = $("input#search_sku").val();
 var search_name = $("input#search_name").val();
 var search_brand = $("input#search_brand").val();
 var search_invoice_id = $("input#search_invoice_id").val();
 var search_date = $("input#search_date").val();
 var string = 'search_sku='+search_sku+'&search_name='+search_name+'&search_brand='+search_brand+'&search_invoice_id='+search_invoice_id+'&search_date='+search_date;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>purchase_product/search_purchase",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.product").remove();
			for(var i=0;i<data['products'].length;i++){
				col = '<td>'+data['products'][i].id+'</td>';
				col += '<td>'+data['products'][i].sku+'</td>';
				col += '<td>'+data['products'][i].item_id+'</td>';
				col += '<td>'+data['products'][i].product_code+'</td>';
				col += '<td>'+data['products'][i].name+'</td>';
				col += '<td>'+data['products'][i].brand_name+'</td>';
				col += '<td>'+data['products'][i].weight+'</td>';
				
				col += '<td><a href="#" class="topopup" id="'+data['products'][i].invoice_id+'">'+data['products'][i].invoice_no+'</a></td>';
				
				col += '<td>'+data['products'][i].after_tax+'</td>';
				col += '<td>'+data['products'][i].date+'</td>';
				col += '<td>'+data['products'][i].exp_date+'</td>';
				col += '<td>'+data['products'][i].qty+'</td>';
				
				$('#purchase_products').append('<tr class="product" id="purchase_'+data['products'][i].id+'">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>
<script src="<?php echo site_url('assets/js/popup_loader.js'); ?>"></script>
<script>
$(function() {
$('#search_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="purchase_products">
<tr>
	<th class="blue-gradient">Id</th>
	<th class="blue-gradient">SKU</th>
	<th class="blue-gradient">Item Id</th>
	<th class="blue-gradient">Item Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Weight</th>
	<th class="blue-gradient">Invoice No.</th>
	<th class="blue-gradient">Inc. Tax Price</th>
	<th class="blue-gradient">Purchase Date</th>
	<th class="blue-gradient">Exp. Date</th>
	<th class="blue-gradient">Qty</th>
</tr>
<?php foreach($products as $product) {
echo "<tr id=purchase_$product->id class=product>";
		echo '<td>'.$product->id.'</td>';
		echo '<td>'.$product->sku.'</td>';
		echo '<td>'.$product->item_id.'</td>';
		echo '<td>'.$product->product_code.'</td>';
		echo '<td>'.$product->name.'</td>';
		echo '<td>'.$product->brand_name.'</td>';
		echo '<td>'.$product->weight.'</td>';
		echo '<td>'.anchor('', $product->invoice_no, array('class'=>'topopup','id'=>$product->invoice_id)).'</td>';
		echo '<td>'.$product->after_tax.'</td>';
		echo '<td>'.$product->date.'</td>';
		echo '<td>'.$product->exp_date.'</td>';
		echo '<td>'.$product->qty.'</td>';
		  
echo '</tr>';
} ?>
<tr id="links"><th colspan="12"><?php echo $links; ?></th></tr>
</table>
</div>

<div id="toPopup">
	<div class="close"></div>
	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
<div id="container">
<table id="popup_content" class="table table-bordered">
<tr>
	<th class="blue-gradient">Invoice Id</th>
	<th class="blue-gradient">Supplier Name</th>
	<th class="blue-gradient">Invoice No.</th>
	<th class="blue-gradient">Invoice Date</th>
</tr>
</table>
</div>
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>

<script type="text/javascript">
$(document).on('click', 'a.topopup', function(){
	
	var invoice_id = $(this).attr('id');  
	$.ajax({
	  type: "POST",  
	  url: '<?php echo base_url();?>purchase_product/get_invoice_detail/'+invoice_id,  
	  dataType: 'json',
	  success: function(data) {
		$(".pops").remove();
		var col = '';
		col = '<td>'+data[0].id+'</td>';
		col += '<td>'+data[0].name+'</td>';
		col += '<td>'+data[0].invoice_no+'</td>';
		col += '<td>'+data[0].date+'</td>';
		$('#popup_content').append('<tr style="text-align:center" class="pops">'+col+'</tr>');
	  },
	  error: function(data) {
		$(".pops").remove();
	  }
	});  
	return false;  
});
</script>
<script src="<?php echo site_url('assets/js/popup_loader.js'); ?>"></script>

<?php 
$this->load->view('component/footer');
?>