<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>

<title>OrderProcess | Edit Invoice Entry</title>
<div class="home-title blue-gradient">Edit Invoice Entry</div>
<br>

<div id="container">
<?php echo form_open_multipart(''); ?>
 <table>
	<tr>
		<td>
			<label>Supplier</label>
			<?php echo form_dropdown('supplier', $dropdown, 0, 'id="supplier"'); ?>
			<?php echo form_error('supplier')?>
		</td>
		<td>
			<label>Invoice Date</label>
			<input type="text" id="invoice_date" name="invoice_date" required/><input type="hidden" id="update_date" name="update_date" />
		</td>
		<td>
			<label>Invoice No.</label>
			<input type="text" id="invoice_no" name="invoice_no" value="<?php echo $this->input->post('invoice_no'); ?>" required/>
			<?php echo form_error('invoice_no')?>
		</td>
		<td>
			<label>Total Invoice Value</label>
			<input type="text" id="invoice_value" name="invoice_value" value="<?php echo $this->input->post('invoice_value'); ?>" disabled />
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><input type="button" name="search" value="Search" class="search btn btn-primary" disabled /></td>
		
		<td colspan="2" align="center"><?php echo anchor('invoice_edit.php','Back', array('class' => 'btn btn-primary back', 'name' => 'back')); ?></td>
	</tr>
</table>
 <?php echo form_close(); ?>
 </div>
<script>
 $(function() {
    $( "#invoice_date" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true, pickerClass: 'my-picker'});
  });
 </script>
 <script src="<?php echo site_url('assets/js/invoice_date.js'); ?>"></script>
 
<div id="container" style="position:relative; float:right">
<table>
	<tr>
		<td><label>Invoiced Amount Status<label></td>
	</tr>
	<tr>
		<td id="balance"></td>
	</tr>
</table>
 </div>
 
 <script type="text/javascript">
 /*Save invoice value, supplier & get purchase product list for invoice date is 0 */
 
 $(document).on('focusout', '#invoice_no', function(){
	
	var invoice_no = $("input#invoice_no").val();
	var supplier = $("select#supplier").val();
	var invoice_date = $("input#invoice_date").val();
	var dataString = 'supplier='+supplier+'&invoice_no='+invoice_no+'&invoice_date='+invoice_date;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url();?>edit_invoice/validate_supplier_invoice",
		dataType:'json',
		data:dataString,
		success: function(data) {
			if(data) {
				$('.search').attr("disabled", false);
				$('input#invoice_value').val(data);
			} else {
				alert('This Invoice no  '+invoice_no+' ,against supplier '+supplier+ ' on this date '+invoice_date+' is not exist.');
			}
		}
	});
	return false;
 });
 
 
 $(document).on('click', '.search', function(){
	$('.search').attr("disabled", true);
	
	var supplier = $("select#supplier").val();  
	var invoice_date = $("input#invoice_date").val();  
	var invoice_no = $("input#invoice_no").val();  
	
	var dataString = 'supplier='+ supplier + '&invoice_date=' + invoice_date + '&invoice_no=' + invoice_no;

	$.ajax({  
	  type: "POST",  
	  url: "<?php echo base_url();?>edit_invoice/search_product_invoiced",  
	  dataType: 'json',
	  data: dataString,
	  success: function(data) {
			var col = '';
			var cols = '';
			for(var i=0;i<data.length;i++){
				$('#product_list').show();
				$('#product_inventory').show();
			
			if((data[i].invoice_id)==0) {
				col = '<td><input class="in" type="checkbox" name="in" id="in_'+data[i].id+'" required/><input type="hidden" name="id" id="id_'+data[i].id+'" value="'+data[i].id+'" required/></td>';
				col += '<td>'+data[i].id+'</td>';
				col += '<td>'+data[i].sku+'</td>';
				col += '<td>'+data[i].brand_name+'</td>';
				col += '<td>'+data[i].name+'</td>';
				$('#product_list').append('<tr id="list_'+data[i].id+'">'+col+'</tr>');
				
			} else {
				cols = '<td>'+data[i].id+'</td>';
				cols += '<td class="product_name" id="product_name_'+data[i].id+'">'+data[i].name+'</td>';
				cols += '<td class="brand_name" id="brand_name_'+data[i].id+'">'+data[i].brand_name+'</td>';
				cols += '<td class="sku" id="product_sku_'+data[i].id+'">'+data[i].sku+'</td>';
				cols += '<td class="item_id">'+data[i].item_id+'</td>';
				cols += '<td class="item_code">'+data[i].product_code+'</td>';
				cols += '<td>'+data[i].mrp+'</td>';
				
				cols += '<td><input type="hidden" name="id" id="id_'+data[i].id+'" value="'+data[i].id+'" required/><input type="hidden" name="invoice_id" id="invoice_id_'+data[i].id+'" value="'+data[i].invoice_id+'" required/><input type="text" class="after_tax" name="after_tax" id="after_tax_'+data[i].id+'" value="'+data[i].after_tax+'" required/></td>';
				
				cols += '<td><input type="number" min="1" max="'+data[i].qty+'" class="qty_change" name="qty" id="qty_'+data[i].id+'" value="'+data[i].qty+'" required/></td>';
				
				cols += '<td><input type=button class="remove" id="remove_'+data[i].id+'" title="Remove This Row" value="Remove" /></td>';
				
				$('#product_inventory').append('<tr class="inventory" id="product_'+data[i].id+'">'+cols+'</tr>');
				}
			}
			
	  },
	  error: function(data) {
		$('.search').attr("disabled", false);
	  }
	});  
	return false;  
});
</script>


<div id="container">
<table align="center" class="table table-condensed table-striped table-bordered" id="product_list" style="display:none">
</table>
</div>

<script type="text/javascript">

/* Create form for purchase products by id for this particular invoice date and id */
$(document).on('click', '.in', function(){
	var fid = $(this).attr('id');
	var f_id = fid.match(/[0-9 -()+]+$/,fid);
	var id = $("input#id_"+f_id).val();
	var dataString = 'id='+id;
	$("#list_"+f_id).fadeOut('fast', function() {$(this).remove();});
	$.ajax({  
		type: "POST",  
		url: "<?php echo base_url();?>edit_invoice/get_purchase_products",  
		dataType: 'json',
		data: dataString,
		success: function(data) {	
			var col = '';
			var node = {};
			for(var i=0;i<data.length;i++){
				$('#product_inventory').show();
				col = '<td>'+data[i].id+'</td>';
				col += '<td class="product_name" id="product_name_'+data[i].id+'">'+data[i].name+'</td>';
				col += '<td class="brand_name" id="brand_name_'+data[i].id+'">'+data[i].brand_name+'</td>';
				col += '<td class="sku" id="product_sku_'+data[i].id+'">'+data[i].sku+'</td>';
				col += '<td class="item_id">'+data[i].item_id+'</td>';
				col += '<td class="item_code">'+data[i].product_code+'</td>';
				col += '<td>'+data[i].mrp+'</td>';
				
				col += '<td><input type="hidden" name="id" id="id_'+data[i].id+'" value="'+data[i].id+'" required/><input type="hidden" name="invoice_id" id="invoice_id_'+data[i].id+'" value="" required/><input type="text" class="after_tax" name="after_tax" id="after_tax_'+data[i].id+'" value="'+data[i].after_tax+'" required/></td>';
				
				col += '<td><input type="number" min="1" max="'+data[i].qty+'" class="qty_change" name="qty" id="qty_'+data[i].id+'" value="'+data[i].qty+'" required/></td>';
				
				col += '<td><input type=button class="remove" id="remove_'+data[i].id+'" title="Remove This Row" value="Remove" /></td>';
				
				$('#product_inventory').append('<tr class="inventory" id="product_'+data[i].id+'">'+col+'</tr>');
				
			}
		},
		error: function(data) {
			alert('error');
		}
	});  
	return false;  
});
</script>


<div id="container">
<form action="" class="inventory_product">
<table align="center" class="table table-striped table-bordered" id="product_inventory" style="display:none">
<tr><td colspan="10"><input type="button" name="update" value="Update" class="update btn btn-primary" disabled /></td></tr>
	<tr>
		<th class="blue-gradient">Id</th>
		<th class="blue-gradient">Name</th>
		<th class="blue-gradient">Brand</th>
		<th class="blue-gradient">SKU</th>
		<th class="blue-gradient">Item Id</th>
		<th class="blue-gradient">Item Code</th>
		<th class="blue-gradient">MRP</th>
		<th class="blue-gradient">Our Price</th>
		<th class="blue-gradient">Qty</th>
		<th class="blue-gradient">Action</th>
	</tr>
</table>
</form>
</div>

<script type="text/javascript">

/*Removing Row product_inventory table*/
$(document).on('click', '.remove', function(){
	var fid = $(this).attr('id');
	var f_id = fid.match(/[0-9 -()+]+$/,fid);
	var name = $('#product_name_'+f_id).html();
	var sku = $('#product_sku_'+f_id).html();
	var brand_name = $('#brand_name_'+f_id).html();
	if (confirm("Are you sure you want to remove this row?")) {
		
		col = '<td><input class="in" type="checkbox" name="in" id="in_'+f_id+'" required/><input type="hidden" name="id" id="id_'+f_id+'" value="'+f_id+'" required/></td>';
		col += '<td>'+f_id+'</td>';
		col += '<td>'+sku+'</td>';
		col += '<td>'+brand_name+'</td>';
		col += '<td>'+name+'</td>';
		$('#product_list').append('<tr id="list_'+f_id+'">'+col+'</tr>');
		$("#product_"+f_id).remove();
	}
});

/* If qty change Qty column in form#inventory_product, Creating new Clone in Db with updated qty */
$(document).on('change focusout', '.qty_change', function () {
	var qty_id = $(this).attr('id');
	var qty_value = $('#'+qty_id).val();
	$(this).attr("max", qty_value);
	var fid = qty_id.match(/[0-9 -()+]+$/,qty_id);
	$("#"+qty_id).val(qty_value);
});


/* Update Invoice value, unit price, tax, qty, price after tax for form#inventory_product */
$(document).on('click', '.update', function(){
	$('.update').attr("disabled", true);
	
	var supplier = $("select#supplier").val();  
	var invoice_date = $("input#invoice_date").val();  
	var invoice_no = $("input#invoice_no").val();  
	var update_date = $("input#update_date").val();
	
	var dataValues = 'supplier='+ supplier + '&invoice_date=' + invoice_date + '&invoice_no=' + invoice_no + '&update_date=' +update_date;
if(confirm("Are you sure? All the information is correct.")) {
	$.ajax({  
	  type: "POST",  
	  url: "<?php echo base_url();?>edit_invoice/update_invoice_entry/",  
	  dataType: 'json',
	  data: dataValues,
	  success: function(data) {
			
			var ids = $(".inventory_product input[name=id]").serializeArray();
			var invoice_id = data;
			for(var i=0;i<ids.length;i++) {
				var iddss = ids[i].value;
				$(".inventory_product input#invoice_id_"+iddss).val(data);
			}
			for(var i=0;i<ids.length;i++) {
				$('.update').attr("disabled", true);
				var id = ids[i].value;
				var after_tax = $(".inventory_product input#after_tax_"+id).val();
				var qty = $(".inventory_product input#qty_"+id).val();
				
				var dataString = 'id=' + id + '&invoice_id=' + invoice_id + '&after_tax=' + after_tax + '&qty=' + qty;
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url().'edit_invoice/update_purchase_product/'?>",
					dataType: 'json',
					data: dataString,
					success: function(response){
						alert('Successful');
					}, 
					error:function() {
						alert('Update Error');
					}
				});
			}
			return false;
	    },	
	   error: function(data) {
			alert('invoice Error');
		}
	});
	return false;
}
});


/* Invoice Amount Checking */

$(document).on('mouseover keyup', '.inventory_product', function() {
	
	var ids = $(".inventory_product input[name=id]").serializeArray();
	var total_invoice_value = 0;
	var invoice_value = parseFloat($('input#invoice_value').val());
	
	for(var i=0;i<ids.length;i++){
		var id = ids[i].value;
		var after_tax = parseFloat($(".inventory_product input#after_tax_"+id).val());
		var qty = parseInt($(".inventory_product input#qty_"+id).val());
		total_invoice_value += after_tax*qty;
	}
	
	var balance = invoice_value-total_invoice_value;
	if(((total_invoice_value+10) == invoice_value) || ((total_invoice_value-10) == invoice_value) || (total_invoice_value==invoice_value) ) {
		$("td#balance").html('Now You can Update All Information.');
		$('.update').attr("disabled", false);
	} else if((total_invoice_value+10) > invoice_value) {
		$('.update').attr("disabled", true);
		$("td#balance").html('Invoiced Amount Exceeded : '+(-balance));
	} else if((total_invoice_value-10) < invoice_value) {
		$('.update').attr("disabled", true);
		$("td#balance").html('Invoiced Amount Left : '+balance);
	}

});

</script>


<?php $this->load->view('component/footer'); ?>