<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Manage Discount Rule</title>
<div class="home-title blue-gradient">Manage Discount Rule</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_discount_rule');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Rule Name: <input type="text" id="search_rule_name" name="search_rule_name" /></td>
	<td class="blue-gradient">Supplier: <input type="text" id="search_supplier" name="search_supplier" /></td>
	<td class="blue-gradient">Brand: <input type="text" id="search_brand" name="search_brand" /></td>
	<td class="blue-gradient">Status: <input type="text" id="search_status" name="search_status" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$(document).on('change', '#search_discount_rule', function(){
	var search_rule_name = $("input#search_rule_name").val();
	var search_supplier = $("input#search_supplier").val();
	var search_brand = $("input#search_brand").val();
	var search_status = $("input#search_status").val();
	
	var string = 'search_rule_name='+search_rule_name+'&search_supplier='+search_supplier+'&search_brand='+search_brand+'&search_status='+search_status;
	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_manage_suppliers/search_discount_rule",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.rules").remove();
			var col = '';
			for(var i=0;i<data.length;i++){
			
				col = '<td>'+data[i].name+'</td>';
				col += '<td>'+data[i].supplier_name+'</td>';
				col += '<td>'+data[i].brand_name+'</td>';
				col += '<td>'+data[i].segment+'</td>';
				
				col += '<td id="dis_'+data[i].id+'"><span id="discount_per_'+data[i].id+'" class="discounts editable">'+data[i].discount+'</span></td>';
				
				col += '<td id="rp_'+data[i].id+'"><span id="rule_priority_'+data[i].id+'" class="rule_priority editable">'+data[i].rule_priority+'</span></td>';
				
				col += '<td id="sp_'+data[i].id+'"><span id="supplier_priority_'+data[i].id+'" class="supplier_priority editable">'+data[i].supplier_priority+'</span></td>';
				
				col += '<td id="st_'+data[i].id+'"><span id="status_'+data[i].id+'" class="status editable">'+data[i].status+'</span></td>';
				
				$('#manage_discount_rule').append('<tr id="rule_'+data[i].id+'" class="rules">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="manage_discount_rule">
<tr>
	<th class="blue-gradient">Rule Name</th>
	<th class="blue-gradient">Supplier</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Segment</th>
	<th class="blue-gradient">Discount%</th>
	<th class="blue-gradient">Rule Priority</th>
	<th class="blue-gradient">Supplier Priority</th>
	<th class="blue-gradient">Status</th>
</tr>
<?php foreach($rules as $rule) {
echo "<tr id='rule_$rule->id' class='rules'>";
	echo '<td>'.$rule->name.'</td>';
	echo '<td>'.$rule->supplier_name.'</td>';
	echo '<td>'.$rule->brand_name.'</td>';
	echo '<td>'.$rule->segment.'</td>';
	
	echo "<td id='dis_$rule->id'><span id='discount_per_$rule->id' class='discounts editable'>".$rule->discount."</span></td>";
	
	echo "<td id='rp_$rule->id'><span id='rule_priority_$rule->id' class='rule_priority editable'>".$rule->rule_priority."</span></td>";
	
	echo "<td id='sp_$rule->id'><span id='supplier_priority_$rule->id' class='supplier_priority editable'>".$rule->supplier_priority."</span></td>";
	
	echo "<td id='st_$rule->id'><span id='status_$rule->id' class='status editable'>".$rule->status."</span></td>";
echo '</tr>';
} ?>
<tr id="links"><th colspan="8"><?php echo $links; ?></th></tr>
</table>
</div>

<script type="text/javascript">

$(document).on('click', 'span.discounts', function(){
	var discount_id = $(this).attr('id');
	var id = discount_id.match(/[0-9 -()+]+$/,discount_id);
	var discount_val = $("#"+discount_id).html();
	$("#dis_"+id).html('<input type="text" class="text_discount_val" name="text_discount_val" id="discount_val_'+id+'" value="'+discount_val+'" />');
	
});
$(document).on('keydown', '.text_discount_val', function(e){
if(e.which == 13){
	var discount_id = $(this).attr('id');
	var id = discount_id.match(/[0-9 -()+]+$/,discount_id);
	var discount_val = $("#"+discount_id).val();
	if(discount_val.length >0) {
		$("#discount_val_"+id).replaceWith('<span id="discount_per_'+id+'" class="discounts editable">'+discount_val+'</span>');
		var dataString = 'text_discount_val='+discount_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_discount_discount_rule/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});


$(document).on('click', 'span.rule_priority', function(){
	var rp_id = $(this).attr('id');
	var id = rp_id.match(/[0-9 -()+]+$/,rp_id);
	var rp_val = $("#"+rp_id).html();
	$("#rp_"+id).html('<input type="text" class="text_rp_val" name="text_rp_val" id="rp_val_'+id+'" value="'+rp_val+'" />');
	
});
$(document).on('keydown', '.text_rp_val', function(e){
if(e.which == 13){
	var rp_id = $(this).attr('id');
	var id = rp_id.match(/[0-9 -()+]+$/,rp_id);
	var rp_val = $("#"+rp_id).val();
	if(rp_val.length >0) {
		$("#rp_val_"+id).replaceWith('<span id="rule_priority_'+id+'" class="rule_priority editable">'+rp_val+'</span>');
		var dataString = 'text_rp_val='+rp_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_rule_priority_discount_rule/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.supplier_priority', function(){
	var sp_id = $(this).attr('id');
	var id = sp_id.match(/[0-9 -()+]+$/,sp_id);
	var sp_val = $("#"+sp_id).html();
	$("#sp_"+id).html('<input type="text" class="text_sp_val" name="text_sp_val" id="sp_val_'+id+'" value="'+sp_val+'" />');
	
});
$(document).on('keydown', '.text_sp_val', function(e){
if(e.which == 13){
	var sp_id = $(this).attr('id');
	var id = sp_id.match(/[0-9 -()+]+$/,sp_id);
	var sp_val = $("#"+sp_id).val();
	if(sp_val.length >0) {
		$("#sp_val_"+id).replaceWith('<span id="supplier_priority_'+id+'" class="supplier_priority editable">'+sp_val+'</span>');
		var dataString = 'text_sp_val='+sp_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_supplier_priority_discount_rule/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.status', function(){
	var st_id = $(this).attr('id');
	var id = st_id.match(/[0-9 -()+]+$/,st_id);
	var st_val = $("#"+st_id).html();
	$("#st_"+id).html('<input type="text" class="text_st_val" name="text_st_val" id="st_val_'+id+'" value="'+st_val+'" />');
	
});

$(document).on('keydown', '.text_st_val', function(e){
if(e.which == 13){
	var st_id = $(this).attr('id');
	var id = st_id.match(/[0-9 -()+]+$/,st_id);
	var st_val = $("#"+st_id).val();
	if(st_val.length >0) {
		$("#st_val_"+id).replaceWith('<span id="status_'+id+'" class="status editable">'+st_val+'</span>');
		var dataString = 'text_st_val='+st_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_status_discount_rule/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});
</script>