<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Manage Suppliers</title>
<div class="home-title blue-gradient">Manage Suppliers</div>
<br>

<div id="container">
<table align="center" class="table table-striped table-bordered">
<tr>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">About</th>
	<th class="blue-gradient">Contact Person</th>
	<th class="blue-gradient">Mobile</th>
	<th class="blue-gradient">Phone</th>
	<th class="blue-gradient">Email</th>
	<th class="blue-gradient">Secondary Email</th>
	<th class="blue-gradient">Address</th>
	<th class="blue-gradient">City</th>
	<th class="blue-gradient">State</th>
	<th class="blue-gradient">Pincode</th>
	<th class="blue-gradient">Vat No.</th>
	<th class="blue-gradient">Cst No.</th>
	<th class="blue-gradient">Status</th>
	<th class="blue-gradient">Remove</th>
</tr>
<?php foreach($suppliers as $supplier) {
echo "<tr id='supplier_$supplier->id' class='suppliers'>";
	echo '<td>'.$supplier->name.'</td>';
	
	if($supplier->about) {
		echo "<td id='abt_$supplier->id'><span id='about_$supplier->id' class='about editable'>".$supplier->about."</span></td>";
	} else {
		echo "<td id='abt_$supplier->id'><span id='about_$supplier->id' class='about editable'>empty</span></td>";
	}
	
	if($supplier->pocontact) { 
		echo "<td id='po_name_$supplier->id'><span id='pocontact_$supplier->id' class='pocontact editable'>".$supplier->pocontact."</span></td>";
	} else {
		echo "<td id='po_name_$supplier->id'><span id='pocontact_$supplier->id' class='pocontact editable'>empty</span></td>";
	}
	
	if($supplier->mobile) {
		echo "<td id='mob_$supplier->id'><span id='mobile_$supplier->id' class='mobile editable'>".$supplier->mobile."</span></td>";
	} else {
		echo "<td id='mob_$supplier->id'><span id='mobile_$supplier->id' class='mobile editable'>0</span></td>";
	}
	
	if($supplier->phone) {
		echo "<td id='ph_$supplier->id'><span id='phone_$supplier->id' class='phone editable'>".$supplier->phone."</span></td>";
	} else {
		echo "<td id='ph_$supplier->id'><span id='phone_$supplier->id' class='phone editable'>0</span></td>";
	}
	
	if($supplier->email) {
		echo "<td id='mail_$supplier->id'><span id='email_$supplier->id' class='email editable'>".$supplier->email."</span></td>";
	} else {
		echo "<td id='mail_$supplier->id'><span id='email_$supplier->id' class='email editable'>0</span></td>";
	}
	
	if($supplier->email2) {
		echo "<td id='sec_mail_$supplier->id'><span id='sec_email_$supplier->id' class='sec_email editable'>".$supplier->email2."</span></td>";
	} else {
		echo "<td id='sec_mail_$supplier->id'><span id='sec_email_$supplier->id' class='sec_email editable'>--</span></td>";
	}
	
	if($supplier->street1) {
		echo "<td id='strt_$supplier->id'><span id='street_$supplier->id' class='street editable'>".$supplier->street1."</span></td>";
	} else {
		echo "<td id='strt_$supplier->id'><span id='street_$supplier->id' class='street editable'>--</span></td>";
	}	
	
	if($supplier->city) {
		echo "<td id='ct_$supplier->id'><span id='city_$supplier->id' class='city editable'>".$supplier->city."</span></td>";
	} else {
		echo "<td id='ct_$supplier->id'><span id='city_$supplier->id' class='city editable'>none</span></td>";
	}
	
	if($supplier->state) {
		echo "<td id='stat_$supplier->id'><span id='state_$supplier->id' class='state editable'>".$supplier->state."</span></td>";
	} else {
		echo "<td id='stat_$supplier->id'><span id='state_$supplier->id' class='state editable'>--</span></td>";
	}
	
	if($supplier->pincode) {
		echo "<td id='po_code_$supplier->id'><span id='pincode_$supplier->id' class='pincode editable'>".$supplier->pincode."</span></td>";
	} else {
		echo "<td id='po_code_$supplier->id'><span id='pincode_$supplier->id' class='pincode editable'>--</span></td>";
	}
	
	if($supplier->vat_no) {
		echo "<td id='vt_no_$supplier->id'><span id='vat_no_$supplier->id' class='vat_no editable'>".$supplier->vat_no."</span></td>";
	} else {
		echo "<td id='vt_no_$supplier->id'><span id='vat_no_$supplier->id' class='vat_no editable'>--</span></td>";
	}
	
	if($supplier->cst_no) {
		echo "<td id='ct_no_$supplier->id'><span id='cst_no_$supplier->id' class='cst_no editable'>".$supplier->cst_no."</span></td>";
	} else {
		echo "<td id='ct_no_$supplier->id'><span id='cst_no_$supplier->id' class='cst_no editable'>--</span></td>";
	}
	
	if($supplier->status) {
		echo "<td id='st_$supplier->id'><span id='status_$supplier->id' class='status editable'>".$supplier->status."</span></td>";
	} else {
		echo "<td id='st_$supplier->id'><span id='status_$supplier->id' class='status editable'>empty</span></td>";
	}
	
	echo "<td><input type='button' class='remove btn' id='remove_$supplier->id' value='Remove' /></td>";
	
echo '</tr>';
} ?>
<tr id="links"><th colspan="13"><?php echo $links; ?></th></tr>
</table>
</div>
<script type="text/javascript">
$(document).on('click', '.remove', function(){
	var rid = $(this).attr('id');
	var id = rid.match(/[0-9 -()+]+$/,rid);
	$.ajax({
		type:"POST",
		url:'<?php echo base_url(); ?>c_manage_suppliers/remove_suppliers/'+id,
		dataType:'json',
		success: function(data) {
			$('tr#supplier_'+id).remove();
		}
	});
	return false;
});


$(document).on('click', 'span.about', function(){
	var abt_id = $(this).attr('id');
	var id = abt_id.match(/[0-9 -()+]+$/,abt_id);
	var abt_val = $("#"+abt_id).html();
	$("#abt_"+id).html('<textarea class="text_about_val" name="text_about_val" id="about_val_'+id+'">'+abt_val+'</textarea>');
	
});
$(document).on('keydown', '.text_about_val', function(e){
if(e.which == 13){
	var abt_id = $(this).attr('id');
	var id = abt_id.match(/[0-9 -()+]+$/,abt_id);
	var abt_val = $("#"+abt_id).val();
	if(abt_val.length >0) {
		$("#about_val_"+id).replaceWith('<span id="about_'+id+'" class="about editable">'+abt_val+'</span>');
		var dataString = 'text_about_val='+abt_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_about_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});


$(document).on('click', 'span.pocontact', function(){
	var po_id = $(this).attr('id');
	var id = po_id.match(/[0-9 -()+]+$/,po_id);
	var po_val = $("#"+po_id).html();
	$("#po_name_"+id).html('<input type="text" class="text_po_val" name="text_po_val" id="po_val_'+id+'" value="'+po_val+'" />');
	
});
$(document).on('keydown', '.text_po_val', function(e){
if(e.which == 13){
	var po_id = $(this).attr('id');
	var id = po_id.match(/[0-9 -()+]+$/,po_id);
	var po_val = $("#"+po_id).val();
	if(po_val.length >0) {
		$("#po_val_"+id).replaceWith('<span id="pocontact_'+id+'" class="pocontact editable">'+po_val+'</span>');
		var dataString = 'text_po_val='+po_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_pocontact_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.mobile', function(){
	var mob_id = $(this).attr('id');
	var id = mob_id.match(/[0-9 -()+]+$/,mob_id);
	var mob_val = $("#"+mob_id).html();
	$("#mob_"+id).html('<input type="text" class="text_mob_val" name="text_mob_val" id="mob_val_'+id+'" value="'+mob_val+'" />');
	
});
$(document).on('keydown', '.text_mob_val', function(e){
if(e.which == 13){
	var mob_id = $(this).attr('id');
	var id = mob_id.match(/[0-9 -()+]+$/,mob_id);
	var mob_val = $("#"+mob_id).val();
	if(mob_val.length >0) {
		$("#mob_val_"+id).replaceWith('<span id="mobile_'+id+'" class="mobile editable">'+mob_val+'</span>');
		var dataString = 'text_mob_val='+mob_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_mobile_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.phone', function(){
	var ph_id = $(this).attr('id');
	var id = ph_id.match(/[0-9 -()+]+$/,ph_id);
	var ph_val = $("#"+ph_id).html();
	$("#ph_"+id).html('<input type="text" class="text_ph_val" name="text_ph_val" id="ph_val_'+id+'" value="'+ph_val+'" />');
	
});
$(document).on('keydown', '.text_ph_val', function(e){
if(e.which == 13){
	var ph_id = $(this).attr('id');
	var id = ph_id.match(/[0-9 -()+]+$/,ph_id);
	var ph_val = $("#"+ph_id).val();
	if(ph_val.length >0) {
		$("#ph_val_"+id).replaceWith('<span id="phone_'+id+'" class="phone editable">'+ph_val+'</span>');
		var dataString = 'text_ph_val='+ph_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_phone_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.email', function(){
	var e_id = $(this).attr('id');
	var id = e_id.match(/[0-9 -()+]+$/,e_id);
	var e_val = $("#"+e_id).html();
	$("#mail_"+id).html('<input type="text" class="text_email_val" name="text_email_val" id="mail_val_'+id+'" value="'+e_val+'" />');
	
});
$(document).on('keydown', '.text_email_val', function(e){
if(e.which == 13){
	var e_id = $(this).attr('id');
	var id = e_id.match(/[0-9 -()+]+$/,e_id);
	var e_val = $("#"+e_id).val();
	if(e_val.length >0) {
		$("#mail_val_"+id).replaceWith('<span id="email_'+id+'" class="email editable">'+e_val+'</span>');
		var dataString = 'text_email_val='+e_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_email_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.sec_email', function(){
	var e_id = $(this).attr('id');
	var id = e_id.match(/[0-9 -()+]+$/,e_id);
	var e_val = $("#"+e_id).html();
	$("#sec_mail_"+id).html('<input type="text" class="text_sec_email_val" name="text_sec_email_val" id="email_val_'+id+'" value="'+e_val+'" />');
	
});
$(document).on('keydown', '.text_sec_email_val', function(e){
if(e.which == 13){
	var e_id = $(this).attr('id');
	var id = e_id.match(/[0-9 -()+]+$/,e_id);
	var e_val = $("#"+e_id).val();
	if(e_val.length >0) {
		$("#email_val_"+id).replaceWith('<span id="sec_email_'+id+'" class="sec_email editable">'+e_val+'</span>');
		var dataString = 'text_sec_email_val='+e_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_email2_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.street', function(){
	var strt_id = $(this).attr('id');
	var id = strt_id.match(/[0-9 -()+]+$/,strt_id);
	var strt_val = $("#"+strt_id).html();
	$("#strt_"+id).html('<textarea class="text_strt_val" name="text_strt_val" id="strt_val_'+id+'">'+strt_val+'</textarea>');
	
});
$(document).on('keydown', '.text_strt_val', function(e){
if(e.which == 13){
	var strt_id = $(this).attr('id');
	var id = strt_id.match(/[0-9 -()+]+$/,strt_id);
	var strt_val = $("#"+strt_id).val();
	if(strt_val.length >0) {
		$("#strt_val_"+id).replaceWith('<span id="street_'+id+'" class="street editable">'+strt_val+'</span>');
		var dataString = 'text_strt_val='+strt_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_street_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});


$(document).on('click', 'span.city', function(){
	var c_id = $(this).attr('id');
	var id = c_id.match(/[0-9 -()+]+$/,c_id);
	var c_val = $("#"+c_id).html();
	$("#ct_"+id).html('<input type="text" class="text_city_val" name="text_city_val" id="city_val_'+id+'" value="'+c_val+'" />');
	
});
$(document).on('keydown', '.text_city_val', function(e){
if(e.which == 13){
	var c_id = $(this).attr('id');
	var id = c_id.match(/[0-9 -()+]+$/,c_id);
	var c_val = $("#"+c_id).val();
	if(c_val.length >0) {
		$("#city_val_"+id).replaceWith('<span id="city_'+id+'" class="city editable">'+c_val+'</span>');
		var dataString = 'text_city_val='+c_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_city_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.state', function(){
	var state_id = $(this).attr('id');
	var id = state_id.match(/[0-9 -()+]+$/,state_id);
	var state_val = $("#"+state_id).html();
	$("#stat_"+id).html('<input type="text" class="text_state_val" name="text_state_val" id="state_val_'+id+'" value="'+state_val+'" />');
	
});
$(document).on('keydown', '.text_state_val', function(e){
if(e.which == 13){
	var state_id = $(this).attr('id');
	var id = state_id.match(/[0-9 -()+]+$/,state_id);
	var state_val = $("#"+state_id).val();
	if(state_val.length >0) {
		$("#state_val_"+id).replaceWith('<span id="state_'+id+'" class="state editable">'+state_val+'</span>');
		var dataString = 'text_state_val='+state_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_state_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.pincode', function(){
	var pin_id = $(this).attr('id');
	var id = pin_id.match(/[0-9 -()+]+$/,pin_id);
	var pin_val = $("#"+pin_id).html();
	$("#po_code_"+id).html('<input type="text" class="text_pincode_val" name="text_pincode_val" id="pincode_val_'+id+'" value="'+pin_val+'" />');
	
});
$(document).on('keydown', '.text_pincode_val', function(e){
if(e.which == 13){
	var pin_id = $(this).attr('id');
	var id = pin_id.match(/[0-9 -()+]+$/,pin_id);
	var pin_val = $("#"+pin_id).val();
	if(pin_val.length >0) {
		$("#pincode_val_"+id).replaceWith('<span id="pincode_'+id+'" class="pincode editable">'+pin_val+'</span>');
		var dataString = 'text_pincode_val='+pin_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_pincode_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.cst_no', function(){
	var cst_id = $(this).attr('id');
	var id = cst_id.match(/[0-9 -()+]+$/,cst_id);
	var cst_val = $("#"+cst_id).html();
	$("#ct_no_"+id).html('<input type="text" class="text_cst_val" name="text_cst_val" id="cst_val_'+id+'" value="'+cst_val+'" />');
	
});
$(document).on('keydown', '.text_cst_val', function(e){
if(e.which == 13){
	var cst_id = $(this).attr('id');
	var id = cst_id.match(/[0-9 -()+]+$/,cst_id);
	var cst_val = $("#"+cst_id).val();
	if(cst_val.length >0) {
		$("#cst_val_"+id).replaceWith('<span id="cst_no_'+id+'" class="cst_no editable">'+cst_val+'</span>');
		var dataString = 'text_cst_val='+cst_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_cst_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.vat_no', function(){
	var vat_id = $(this).attr('id');
	var id = vat_id.match(/[0-9 -()+]+$/,vat_id);
	var vat_val = $("#"+vat_id).html();
	$("#vt_no_"+id).html('<input type="text" class="text_vat_val" name="text_vat_val" id="vat_val_'+id+'" value="'+vat_val+'" />');
	
});
$(document).on('keydown', '.text_vat_val', function(e){
if(e.which == 13){
	var vat_id = $(this).attr('id');
	var id = vat_id.match(/[0-9 -()+]+$/,vat_id);
	var vat_val = $("#"+vat_id).val();
	if(vat_val.length >0) {
		$("#vat_val_"+id).replaceWith('<span id="vat_no_'+id+'" class="vat_no editable">'+vat_val+'</span>');
		var dataString = 'text_vat_val='+vat_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_vat_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});

$(document).on('click', 'span.status', function(){
	var st_id = $(this).attr('id');
	var id = st_id.match(/[0-9 -()+]+$/,st_id);
	var st_val = $("#"+st_id).html();
	$("#st_"+id).html('<input type="text" class="text_sta_val" name="text_sta_val" id="st_val_'+id+'" value="'+st_val+'" />');
	
});

$(document).on('keydown', '.text_sta_val', function(e){
if(e.which == 13){
	var st_id = $(this).attr('id');
	var id = st_id.match(/[0-9 -()+]+$/,st_id);
	var st_val = $("#"+st_id).val();
	if(st_val.length >0) {
		$("#st_val_"+id).replaceWith('<span id="status_'+id+'" class="status editable">'+st_val+'</span>');
		var dataString = 'text_sta_val='+st_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/submit_status_suppliers/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
	}
}
});
</script>
<br/>
<div id="flip" class="home-title blue-gradient">Add New Supplier</div>
<div id="panel" style="display:none">
<div id="container">
<?php $attributes = array('class' => 'new_supplier');?>
<div class="alert-error">
	<?php echo validation_errors(); ?>
</div>
<?php echo form_open_multipart('', $attributes); ?>
 <table>
	<tr>
		<td>
			<label>Name</label>
			<input type="text" name="supplier_name" id="supplier_name" required/>
		</td>
		<td>
			<label>Contact Person</label>
			<input type="text" name="supplier_pocontact" id="supplier_pocontact" required/>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<label>Address</label>
			<textarea name="supplier_adress" id="supplier_adress"></textarea>
		</td>
	</tr>
	<tr>
		<td>
			<label>City</label>
			<input type="text" name="supplier_city" id="supplier_city" />
		</td>
		<td>
			<label>State</label>
			<input type="text" name="supplier_state" id="supplier_state" required/>
		</td>
	</tr>
	<tr>
		<td>
			<label>Pincode</label>
			<input type="text" name="supplier_pincode" id="supplier_pincode" required/>
		</td>
		<td>
			<label>Mobile</label>
			<input type="text" name="supplier_mobile" id="supplier_mobile" required/>
		</td>
	</tr>
	<tr>
		<td>
			<label>Phone No</label>
			<input type="text" name="supplier_ph_no" id="supplier_ph_no" required/>
		</td>
		<td>
			<label>Email</label>
			<input type="email" name="supplier_email" id="supplier_email" required/>
		</td>
	</tr>
	<tr>
		<td>
			<label>Seccondary Email</label>
			<input type="email" name="supplier_sec_email" id="supplier_sec_email" required/>
		</td>
		<td>
			<label>Status</label>
			<?php $options = array(
                  '1'  => 'Enable',
                  '0'    => 'Disable'
             ); 
			 echo form_dropdown('supplier_status', $options,1, 'id="supplier_status"'); 
			 ?>
		</td>
	</tr>
	<tr><td align="center" colspan="2"><input type="button" value="Create" name="create" class="create btn btn-primary" /></td></tr>
</table>
 <?php echo form_close(); ?>
 </div>
</div>

<script type="text/javascript">
$("#flip").click(function(){
    $("#panel").slideToggle("slow");
});

$(document).on('click', '.create', function(){
	$('.new_supplier input').attr('disabled', true);
	var name = $('#supplier_name').val();
	var contact_person = $('#supplier_pocontact').val();
	var address = $('#supplier_adress').val();
	var city = $('#supplier_city').val();
	var state = $('#supplier_state').val();
	var pincode = $('#supplier_pincode').val();
	var mobile = $('#supplier_mobile').val();
	var phone = $('#supplier_ph_no').val();
	var email = $('#supplier_email').val();
	var semail = $('#supplier_sec_email').val();
	var status = $('#supplier_status').val();
	
	var dataString = 'supplier_name='+name+'&supplier_pocontact='+contact_person+'&supplier_adress='+address+'&supplier_city='+city+'&supplier_state='+state+'&supplier_pincode='+pincode+'&supplier_mobile='+mobile+'&supplier_ph_no='+phone+'&supplier_email='+email+'&supplier_sec_email='+semail+'&supplier_status='+status;
	
	$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_manage_suppliers/add_new_supplier',
			dataType:'json',
			data:dataString,
			success: function(data) {
				$('.new_supplier input').attr('disabled', false);
				$('.new_supplier input').val('');
				$('.new_supplier input.create').val('Create');
			}
		});
		return false;
});
</script>