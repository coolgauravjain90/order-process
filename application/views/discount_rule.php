<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>

<title>Discount Rule</title>
<div class="home-title blue-gradient">Discount Rule</div>
<br>

<div id="container">
<?php $attributes = array('class' => 'discount_rule');?>
<div class="alert-error">
	<?php echo validation_errors(); ?>
</div>
<?php echo form_open_multipart('create_discount_rule.php', $attributes); ?>
 <table>
	<tr>
		<td>
			<label>Name</label>
			<input type="text" name="name" id="name" required/>
		</td>
		<td>
			<label>Supplier</label>
			<?php echo form_dropdown('supplier', $supplier, 0, 'id="supplier"'); ?>
		</td>
	</tr>
	<tr>
		<td>
			<label>Brand</label>
			<?php echo form_dropdown('brand', $brand, 0, 'id="brand"'); ?>
		</td>
		<td>
			<label>Brand Segment</label>
			<input type="text" name="brand_segment" id="brand_segment" />
		</td>
	</tr>
	<tr>
		<td>
			<label>Discount(%)</label>
			<input type="text" name="discount" id="discount" required/>
		</td>
		<td>
			<label>Status</label>
			<?php $options = array(
                  '1'  => 'Enable',
                  '0'    => 'Disable'
             ); 
			 echo form_dropdown('status', $options, 1, 'id="status"'); 
			 ?>
		</td>
	</tr>
	<tr>
		<td>
			<label>Rule Priority</label>
			<input type="number" min="0" name="rule_priority" id="rule_priority" required/>
		</td>
		<td>
			<label>Supplier Priority</label>
			<input type="number" min="0" name="supplier_priority" id="supplier_priority" required/>
		</td>
	</tr>
	<tr><td align="center" colspan="2"><input type="submit" value="Create" name="create" class="create btn btn-primary" /></td></tr>
</table>
 <?php echo form_close(); ?>
 </div>
 
<?php
$this->load->view('component/footer'); 	
?>