<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=pending_order_items.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<?php 
$this->load->view('component/header_login');
?>

<div id="container">
<table align="center" class="table table-striped table-bordered board">
<tr>
	<th class="blue-gradient">Order No</th>
	<th class="blue-gradient">System Status</th>
	<th class="blue-gradient">Internal Status</th>
	<th class="blue-gradient">SKU</th>
	<th class="blue-gradient">Item Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Qty</th>
	<th class="blue-gradient">Size</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">MRP</th>
	<th class="blue-gradient">Sale Price</th>
	<th class="blue-gradient">Pending Qty</th>
	<th class="blue-gradient">Supplier</th>
	<th class="blue-gradient">Email Sent</th>
</tr>
<?php foreach($items as $item) {
echo "<tr>";

		echo "<td>".$item->order_no."</td>";
		
		echo "<td>".$item->system_status."</td>";
		
		echo "<td>".$item->internal_status."</td>";
		
		echo "<td>".$item->sku."</td>";
		
		echo '<td>'.$item->item_code.'</td>';
		echo '<td>'.$item->name.'</td>';
		echo "<td>".$item->qty."</td>";
		echo '<td>'.$item->size.'</td>';
		echo '<td>'.$item->brand_name.'</td>';
		echo '<td>'.$item->mrp.'</td>';
		echo '<td>'.$item->sale_price.'</td>';
		
		echo "<td>".($item->qty-$item->procured_qty)."</td>";
		
		echo "<td>".$item->supplier_name."</td>";
		
		echo '<td>'.$item->supp_comment.'</td>';
		  
echo '</tr>';
} ?>
</table>
</div>