<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Order Items</title>
<div class="home-title blue-gradient">Order Items</div>
<br>

<div id="box">
<div id="container">
<?php $attributes = array('id' => 'search_items');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td>Order No: <input type="text" id="search_order_no" name="search_order_no" /></td>
	<td>Store name: <input type="text" id="search_store" name="search_store" /></td>
	<td>Sys Status: <input type="text" id="search_sys_status" name="search_sys_status" /></td>
	<td>Internal Status: <input type="text" id="search_internal_status" name="search_internal_status" /></td>
	<td>SKU: <input type="text" id="search_sku" name="search_sku" /></td>
	<td>Name: <input type="text" id="search_name" name="search_name" /></td>
	<td>Brand: <input type="text" id="search_brand" name="search_brand" /></td>
	<td>Status: <input type="text" id="search_status" name="search_status" /></td>
	<td>Procured Date: <input type="text" id="search_pro_date" name="search_pro_date" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
$(document).on('change', '#search_items', function(){
 var search_order_no = $("input#search_order_no").val();
 var search_store = $("input#search_store").val();
 var search_sys_status = $("input#search_sys_status").val();
 var search_internal_status = $("input#search_internal_status").val();
 var search_sku = $("input#search_sku").val();
 var search_name = $("input#search_name").val();
 var search_brand = $("input#search_brand").val();
 var search_status = $("input#search_status").val();
 var search_pro_date = $("input#search_pro_date").val();
 
 var string = 'search_store='+search_store+'&search_order_no='+search_order_no+'&search_sku='+search_sku+'&search_name='+search_name+'&search_brand='+search_brand+'&search_status='+search_status+'&search_pro_date='+search_pro_date+'&search_sys_status='+search_sys_status+'&search_internal_status='+search_internal_status;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_order_process/search_order_item",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.item").remove();
			var col = '';
			for(var i=0;i<data['items'].length;i++){
			
				col = '<td><input type="hidden" name="id" id="id_'+data['items'][i].id+'" value="'+data['items'][i].id+'" /><input type="hidden" id="order_id_'+data['items'][i].id+'" name="order_id" value="'+data['items'][i].order_id+'" />'+data['items'][i].order_no+'</td>';
				
				col += '<td>'+data['items'][i].store_name+'</td>';
				col += '<td>'+data['items'][i].system_status+'</td>';
				
				col += '<td id="internal_status_'+data['items'][i].item_id+'" class="int_st_'+data['items'][i].order_id+'">'+data['items'][i].internal_status+'</td>';
				
				col += '<td><input type="hidden" id="item_id_'+data['items'][i].id+'" name="item_id" value="'+data['items'][i].item_id+'" />'+data['items'][i].sku+'</td>';
				
				col += '<td>'+data['items'][i].item_code+'</td>';
				col += '<td>'+data['items'][i].name+'</td>';
				col += '<td id="qty_'+data['items'][i].id+'">'+(data['items'][i].qty-data['items'][i].qty_cancel-data['items'][i].qty_refund)+'</td>';
				
				col += '<td>'+data['items'][i].size+'</td>';
				col += '<td>'+data['items'][i].brand_name+'</td>';
				col += '<td>'+data['items'][i].mrp+'</td>';
				col += '<td>'+data['items'][i].sale_price+'</td>';
				
				col += '<td id="st_'+data['items'][i].id+'"><input type="hidden" id="status_'+data['items'][i].id+'" value="'+data['items'][i].status+'" name="status" />'+data['items'][i].status+'</td>'
				
				if(data['items'][i].procured_qty) {
					col += '<td id="pro_qty_'+data['items'][i].id+'"><span id="procured_qty_'+data['items'][i].id+'" class="procured_qty editable">'+data['items'][i].procured_qty+'</span></td>';
				} else {
					col += '<td id="pro_qty_'+data['items'][i].id+'"><span id="procured_qty_'+data['items'][i].id+'" class="procured_qty editable">0</span></td>';
				}
				
				col += '<td class="less_purchase_qty less_qty_pur_'+data['items'][i].item_id+'" id="tot_pur_qty_'+data['items'][i].id+'">'+data['inventory_item_qty'][data['items'][i].item_id]+'</td>';
				
				if(data['items'][i].item_comment) {
					col += '<td id="item_cmt_'+data['items'][i].id+'"><span id="item_comment_'+data['items'][i].id+'" class="item_comment editable">'+data['items'][i].item_comment+'</span></td>';
				} else {
					col += '<td id="item_cmt_'+data['items'][i].id+'"><span id="item_comment_'+data['items'][i].id+'" class="item_comment editable">empty</span></td>';
				}
				
				col += '<td>'+data['items'][i].supplier_name+'</td>';
				
				col += '<td id="pro_date_'+data['items'][i].id+'">'+data['items'][i].procured_date+'</td>';
				
				col += '<td>'+data['items'][i].last_updated+'</td>';
				
				$('#all_order_items').append('<tr class="item" id="items_'+data['items'][i].id+'">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>

<script>
$(function() {
$('#search_pro_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy',
	showAnim: "fold"
});
});
</script>

<div id="box">
<div id="container">
<table align="center" class="table table-striped table-bordered" id="all_order_items">
<tr>
	<th>Order No</th>
	<th>Store name</th>
	<th>System Status</th>
	<th>Internal Status</th>
	<th>SKU</th>
	<th>Item Code</th>
	<th>Name</th>
	<th>Qty</th>
	<th>Size</th>
	<th>Brand</th>
	<th>MRP</th>
	<th>Sale Price</th>
	<th>Status</th>
	<th>Procured Qty</th>
	<th>In Inventry</th>
	<th>Comments</th>
	<th>Supplier</th>
	<th>Procured Date</th>
	<th>Last Update</th>
</tr>
<?php foreach($items as $item) {
echo "<tr id='items_$item->id' class='item'>";

		echo "<td><input type='hidden' name='id' id='id_$item->id' value='$item->id' /><input type='hidden' id='order_id_$item->id' name='order_id' value='$item->order_id' />".$item->order_no."</td>";
		
		echo "<td>".$item->store_name."</td>";
		echo "<td>".$item->system_status."</td>";
		echo "<td id='internal_status_$item->item_id' class='int_st_$item->order_id'>".$item->internal_status."</td>";
		
		echo "<td><input type='hidden' id='item_id_$item->id' name='item_id' value='$item->item_id' />".$item->sku."</td>";
		
		echo '<td>'.$item->item_code.'</td>';
		echo '<td>'.$item->name.'</td>';
		echo "<td id='qty_$item->id'>".($item->qty-$item->qty_cancel-$item->qty_refund)."</td>";
		echo '<td>'.$item->size.'</td>';
		echo '<td>'.$item->brand_name.'</td>';
		echo '<td>'.$item->mrp.'</td>';
		echo '<td>'.$item->sale_price.'</td>';
		
		echo "<td id='st_$item->id'><input type='hidden' id='status_$item->id' value='$item->status' name='status' />".$item->status."</td>";
		
		if($item->procured_qty) {
			echo "<td id='pro_qty_$item->id'><span id='procured_qty_$item->id' class='procured_qty editable'>".$item->procured_qty."</span></td>";
		} else {
			echo "<td id='pro_qty_$item->id'><span id='procured_qty_$item->id' class='procured_qty editable'>0</span></td>";
		}
		
		echo "<td class='less_purchase_qty less_qty_pur_$item->item_id' id='tot_pur_qty_$item->id'>".$inventory_item_qty[$item->item_id]."</td>";
		
		if($item->item_comment) {
			echo "<td id='item_cmt_$item->id'><span id='item_comment_$item->id' class='item_comment editable'>".$item->item_comment."</span></td>";
		} else {
			echo "<td id='item_cmt_$item->id'><span id='item_comment_$item->id' class='item_comment editable'>empty</span></td>";
		}
		
		echo '<td>'.$item->supplier_name.'</td>';
		
		echo "<td id='pro_date_$item->id'>".$item->procured_date."</td>";
		
		echo '<td>'.$item->last_updated.'</td>';
		  
echo '</tr>';
} ?>
<tr id="links"><th colspan="17"><?php echo $links; ?></th></tr>
</table>
</div>
</div>

<?php 
$this->load->view('component/footer');
?>

<script type="text/javascript">
$(document).on('click', 'span.item_comment', function(){
	var comment_id = $(this).attr('id');
	var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
	var comment_val = $("#"+comment_id).html();
	$("#item_cmt_"+id).html('<textarea class="text_comment_val" name="text_comment_val" id="comment_val_'+id+'">'+comment_val+'</textarea>');
	
});
$(document).on('keydown', '.text_comment_val', function(e){
    if(e.which == 13){
		var comment_id = $(this).attr('id');
		var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
		var comment_val = $("#"+comment_id).val();
		$("#comment_val_"+id).replaceWith('<span id="item_comment_'+id+'" class="item_comment editable">'+comment_val+'</span>');
		var dataString = 'text_comment_val='+comment_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/item_comment_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
    }
});


$(document).on('click', 'span.procured_qty', function(){
	var procured_qty_id = $(this).attr('id');
	var id = procured_qty_id.match(/[0-9 -()+]+$/,procured_qty_id);
	var procured_qty_val = $("#"+procured_qty_id).html();
	var max_qty  =$("#qty_"+id).html();
	var pur_qty = $("#tot_pur_qty_"+id).html();
	var min_qty = Math.min(max_qty, pur_qty);
	$("#pro_qty_"+id).html('<input type="number" min="-1" max="'+min_qty+'" class="text_procured_qty_val" name="text_procured_qty_val" id="procured_qty_val_'+id+'" value="'+procured_qty_val+'" />');
});

$(document).on('keypress keyup keydown change', '.text_procured_qty_val', function(e){
	var procured_qty_id = $(this).attr('id');
		var id = procured_qty_id.match(/[0-9 -()+]+$/,procured_qty_id);
		var procured_qty_val = $("#"+procured_qty_id).val();
		var max_qty = $("#qty_"+id).html();
		var pur_qty = $("#tot_pur_qty_"+id).html();
		var min_qty = Math.min(max_qty, pur_qty)
	if(procured_qty_val > min_qty || procured_qty_val < -1) {
			
	} else {
		if(procured_qty_val == max_qty) {
			$("#st_"+id).html('<input type="hidden" id="status_'+id+'" value="Procured" name="status" />Procured');
		} else {
			$("#st_"+id).html('<input type="hidden" id="status_'+id+'" value="Partial Procured" name="status" />Partial Procured');
		}
		if(procured_qty_val==0) {
			$("#st_"+id).html('<input type="hidden" id="status_'+id+'" value="Pending" name="status" />Pending');
		}
		if(procured_qty_val==-1){
			$("#st_"+id).html('<input type="hidden" id="status_'+id+'" value="Cancelled" name="status" />Cancelled');
		}
    if((e.which == 13)|| (e.which == 9)){
		$("#procured_qty_val_"+id).replaceWith('<span id="procured_qty_'+id+'" class="procured_qty editable">'+procured_qty_val+'</span>');
		
		var status = $('#status_'+id).val();
		var order_id = $('#order_id_'+id).val();
		var item_id = $('#item_id_'+id).val();
		var dataString = 'text_procured_qty_val='+procured_qty_val+'&status='+status+'&order_id='+order_id+'&item_id='+item_id;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/procured_qty_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(st) {
				$('.int_st_'+st['order_id']).each(function() { 
					$(this).html(st['st']);
				});
			}
		});
		
		$('.less_qty_pur_'+item_id).each(function() {
			var f_id = $(this).attr('id');
			var id = f_id.match(/[0-9 -()+]+$/,f_id);
			var item_id = $('#item_id_'+id).val();
			$.ajax({
				type:"POST",
				url:'<?php echo base_url(); ?>c_order_process/count_saved_inventory/',
				dataType:'json',
				data:'item_id='+item_id,
				success: function(data) {
					$(".less_qty_pur_"+data['item_id']).html(data['inventory']);
				}
			});
		});
    }
	}
});

</script>