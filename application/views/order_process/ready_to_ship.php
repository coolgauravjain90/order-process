<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>

<title>Ready to Ship Orders</title>
<div class="home-title blue-gradient">Ready to Ship Orders</div>
<br>

<div id="box">
<div id="container">
<?php $attributes = array('id' => 'search_orders');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td>Store name: <input type="text" id="search_store" name="search_store" /></td>
	<td>Order No: <input type="text" id="search_order" name="search_order" /></td>
	<td>System Status: <input type="text" id="search_sys_status" name="search_sys_status" /></td>
	<td>Internal Status: <input type="text" id="search_internal_status" name="search_internal_status" /></td>
	<td>Payment Mode: <input type="text" id="search_payment_mode" name="search_payment_mode" /></td>
	<td>Created Date: <input type="text" id="search_create_date" name="search_create_date" /></td>
	<td>Customer Name: <input type="text" id="search_name" name="search_name" /></td>
	<td>Total Ready to Ship: <?php echo $ready_orders; ?></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
$(document).on('change', '#search_orders', function(){
 var search_store = $("input#search_store").val();
 var search_order = $("input#search_order").val();
 var search_sys_status = $("input#search_sys_status").val();
 var search_internal_status = $("input#search_internal_status").val();
 var search_payment_mode = $("input#search_payment_mode").val();
 var search_create_date = $("input#search_create_date").val();
 var search_name = $("input#search_name").val();

 var string = 'search_store='+search_store+'&search_order='+search_order+'&search_create_date='+search_create_date+'&search_name='+search_name+'&search_sys_status='+search_sys_status+'&search_internal_status='+search_internal_status+'&search_payment_mode='+search_payment_mode;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_order_process/search_ready_to_ship_orders",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.order").remove();
			var col = '';
			for(var i=0;i<data['orders'].length;i++){
				if(data['orders'][i].store_name ==+'beautykafe.com') {
					col = '<td></td>';
				} else {
					col = '<td><input type="hidden" name="pid" id="pid_'+data['orders'][i].id+'" value="'+data['orders'][i].id+'" /><input type="checkbox" name="check" id="check_'+data['orders'][i].id+'" value="'+data['orders'][i].id+'" /></td>';
				}
			
				col += '<td>'+data['popup'][data['orders'][i].id]+'</td>';
				
				col += '<td>'+data['orders'][i].store_name+'</td>';
				
				col += '<td><input type="hidden" name="order_id" id="'+data['orders'][i].id+'" value="'+data['orders'][i].order_id+'" />'+data['orders'][i].order_no+'</td>';
				
				col += '<td id="ship_by_'+data['orders'][i].id+'">'+data['partner_name'][data['orders'][i].id]+'</td>';
				
				col += '<td>'+data['orders'][i].status+'</td>';
				
				if(data['orders'][i].store_name ==+'beautykafe.com')
					col += '<td id="internal_status_'+data['orders'][i].id+'">'+data['orders'][i].internal_status+'</td>';
				else {
					col += '<td>'+data['internal'][data['orders'][i].id]+'</td>';
				}
				
				col += '<td><input type="hidden" name="shipment_value" id="shipment_val_'+data['orders'][i].id+'" value="'+data['orders'][i].amount+'" />'+data['orders'][i].amount+'</td>';
				
				if(data['orders'][i].comments) {
					col += '<td id="cmt_'+data['orders'][i].id+'"><span id="comment_'+data['orders'][i].id+'" class="comment editable" style="color:#FF5E00;">'+data['orders'][i].comments+'</span></td>';
				} else {
					col += '<td id="cmt_'+data['orders'][i].id+'"><span id="comment_'+data['orders'][i].id+'" class="comment editable" style="color:#FF5E00;">--</span></td>';
				}
				
				col += '<td class="payment_mode_'+data['orders'][i].id+'"><input type="hidden" name="payement_mode" id="payment_'+data['orders'][i].id+'" value="'+data['orders'][i].payment_mode+'">'+data['orders'][i].payment_mode+'</td>';
				
				col += '<td>'+data['orders'][i].customer_name+'</td>';
				col += '<td>'+data['orders'][i].email+'</td>';
				col += '<td>'+data['orders'][i].street1+'</td>';
				col += '<td>'+data['orders'][i].city+'</td>';
				col += '<td>'+data['orders'][i].state+'</td>';
				
				col += '<td class="pincode_'+data['orders'][i].id+'"><input type="hidden" name="pincode" id="pin_code_'+data['orders'][i].id+'" value="'+data['orders'][i].pincode+'">'+data['orders'][i].pincode+'</td>';
				
				col += '<td>'+data['orders'][i].created_date+'</td>';
				col += '<td>'+data['orders'][i].last_updated+'</td>';
				
				$('#ready_to_ship_orders').append('<tr class="order" id="orders_'+data['orders'][i].id+'">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>
<script>
$(function() {
$('#search_create_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
});
</script>
<div id="container">
<table align="center" class="table table-striped table-bordered" id="ready_to_ship_orders">
<?php 
	$options = array(
			'Ready to Ship' => 'Ready to Ship',
			'Partial Ready to Ship' => 'Partial Ready to Ship',
			'Cancelled' => 'Cancelled',
			'dispatched'    => 'dispatched'
		);
?>

<tr>
	<th>Action</th>
	<th>Order Id</th>
	<th>Store Name</th>
	<th>Order No.</th>
	<th>Ship By</th>
	<th>System Status</th>
	<th>Internal Status</th>
	<th>Total Amount</th>
	<th>Comments</th>
	<th>Payment Mode</th>
	<th>Name</th>
	<th>Address</th>
	<th>City</th>
	<th>State</th>
	<th>Pincode</th>
	<th>Created Date</th>
	<th>Last Updated</th>
</tr>
<tr>
<?php echo "<td colspan='2' align='center'>".form_dropdown('internal_store_status',$options, 1,'class=internal_store_status')."</td>"; ?>
<td align='center'><input type="button" name="submit" class="submit btn btn-primary" value="Submit" /></td>
</tr>
<?php foreach($orders as $order) {
echo "<tr id='orders_$order->id' class='order'>";
		
		if($order->store_name=='beautykafe.com') {
			echo "<td></td>";
		} else {
			echo "<td><input type='hidden' name='pid' id='pid_$order->id' value='$order->id' /><input type='checkbox' name='check' id='check_$order->id' value='$order->id' /></td>";
		}
		
		echo '<td>'.anchor('', $order->order_id, array('class'=>'topopup','id'=>'topopup_'.$order->id)).'</td>';
		
		echo '<td>'.$order->store_name.'</td>';
		
		echo "<td><input type='hidden' name='order_id' id='$order->id' value='$order->order_id' />".$order->order_no."</td>";
		
		echo "<td id='ship_by_$order->id'>".$partner_name[$order->id]."</td>";
		
		echo '<td>'.$order->status.'</td>';
			
		if($order->store_name=='beautykafe.com') {
			echo "<td id='internal_status_$order->id'>".$order->internal_status."</td>";
		} else {
			echo "<td>".form_dropdown('int_st_store_name',$options, $order->internal_status,'class=int_st_store_name id=int_st_store_'.$order->id)."</td>";
		}
		
		echo "<td><input type='hidden' name='shipment_value' id='shipment_val_$order->id' value='$order->amount' />".$order->amount."</td>";
		
		if($order->comments) {
			echo "<td id='cmt_$order->id'><span id='comment_$order->id' class='comment editable' style='color:#FF5E00;'>".$order->comments."</span></td>";
		} else {
			echo "<td id='cmt_$order->id'><span id='comment_$order->id' class='comment editable' style='color:#FF5E00;'>--</span></td>";
		}
		
		echo "<td class='payment_mode_$order->id'><input type='hidden' name='payement_mode' id='payment_$order->id' value='$order->payment_mode' />".$order->payment_mode."</td>";

		echo '<td>'.$order->customer_name.'</td>';
		echo '<td>'.$order->street1.'</td>';
		echo '<td>'.$order->city.'</td>';
		echo '<td>'.$order->state.'</td>';
		echo "<td class='pincode_$order->id'><input type='hidden' name='pincode' id='pin_code_$order->id' value='$order->pincode' />".$order->pincode."</td>";
		echo '<td>'.$order->created_date.'</td>';
		echo '<td>'.$order->last_updated.'</td>';
		
echo '</tr>';
} ?>
<tr id="links"><th colspan="18"><?php echo $links; ?></th></tr>
</table>
</div>

<script type="text/javascript">
$(document).on('click','.submit', function() {
	var internal_status = $('select.internal_store_status').val();
	var checks = $('#ready_to_ship_orders input:checkbox').serializeArray();
if(internal_status=='dispatched' || internal_status=='Cancelled') {
	for(var i=0; i<checks.length;i++) {
		var id = checks[i].value;
		var order_id = $('#'+id).val();
		var dataString = 'internal_store_status='+internal_status+'&pid='+id+'&order_id='+order_id;
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'c_order_process/change_internal_status_bulk/'?>",
			dataType: 'json',
			data: dataString,
			success: function(response){
				$('tr.orders_'+response).remove();
			}, 
			error:function() {
				
			}
		});
	}
	return false;
}
});



$(document).on('change', 'select.int_st_store_name', function() {
	var internal_id = $(this).attr('id');
	var internal_status = $(this).val();
	var id = internal_id.match(/[0-9 -()+]+$/,internal_id);
	var order_id = $('#'+id).val();
	if(internal_status=='dispatched' || internal_status=='Cancelled') {
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/change_internal_status_by_ship/'+id,
			dataType:'json',
			data:'int_st_store_name='+internal_status+'&order_id='+order_id,
			success: function(data) {
				$('tr.orders_'+id).remove();
			}
		});
		return false;
	}
});

$(document).on('click', 'span.comment', function(){
	var comment_id = $(this).attr('id');
	var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
	var comment_val = $("#"+comment_id).html();
	$("#cmt_"+id).html('<textarea style="resize:none" class="text_comment_val" name="text_comment_val" id="comment_val_'+id+'">'+comment_val+'</textarea>');
	
});
$(document).on('keydown', '.text_comment_val', function(e){
    if(e.which == 13){
		var comment_id = $(this).attr('id');
		var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
		var comment_val = $("#"+comment_id).val();
		$("#comment_val_"+id).replaceWith('<span id="comment_'+id+'" class="comment editable">'+comment_val+'</span>');
		var dataString = 'text_comment_val='+comment_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/comment_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
			}
		});
		return false;
    }
});
</script>


<div id="toPopup">
	<div class="close"></div>
	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
<div id="container">
<table id="popup_content" class="table table-bordered">
<tr>
	<th class="blue-gradient">Item Id</th>
	<th class="blue-gradient">Item Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Qty</th>
	<th class="blue-gradient">Size</th>
	<th class="blue-gradient">MRP</th>
	<th class="blue-gradient">Sale Price</th>
	<th class="blue-gradient">Procured Qty</th>
	<th class="blue-gradient">Item Status</th>
	<th class="blue-gradient">Item Comments</th>
</tr>
</table>
</div>
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>

<?php 
$this->load->view('component/footer');
?>

<script type="text/javascript">
$(document).on('click', 'a.topopup', function(){
	
	var a_id = $(this).attr('id');
	var id = a_id.match(/[0-9 -()+]+$/,a_id);
	var order_id = $("#"+id).val();
	$.ajax({  
	  type: "POST",  
	  url: '<?php echo base_url();?>c_order_process/get_order_item_by_order_id/'+order_id,  
	  dataType: 'json',
	  data: 'order_id='+order_id,
	  success: function(data) {
		$(".pops").remove();
		var col = '';
		for(var i=0;i<data.length;i++) {
			col = '<td>'+data[i].item_id+'</td>';
			col += '<td>'+data[i].item_code+'</td>';
			col += '<td>'+data[i].name+'</td>';
			col += '<td>'+data[i].brand_name+'</td>';
			col += '<td>'+data[i].qty+'</td>';
			col += '<td>'+data[i].size+'</td>';
			col += '<td>'+data[i].mrp+'</td>';
			col += '<td>'+data[i].sale_price+'</td>';
			col += '<td>'+data[i].procured_qty+'</td>';
			col += '<td>'+data[i].status+'</td>';
			col += '<td>'+data[i].item_comment+'</td>';
			$('#popup_content').append('<tr style="text-align:center" class="pops">'+col+'</tr>');
		}
	  },
	  error: function(data) {
		$(".pops").remove();
	  }
	});  
	return false;  
});
</script>
<script src="<?php echo site_url('assets/js/popup_loader.js'); ?>"></script>