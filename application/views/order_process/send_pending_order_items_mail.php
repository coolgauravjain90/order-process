<!DOCTYPE html>
<html>
<head>
<style>
#orders
{
font-family:"Trebuchet MS", Arial, Helvetica, sans-serif;
width:100%;
border-collapse:collapse;
}
#orders td, #orders th 
{
font-size:1em;
border:1px solid #98bf21;
padding:1px 1px 1px 1px;
}
#orders th 
{
font-size:1.1em;
text-align:left;
padding-top:2px;
padding-bottom:2px;
background-color:#A7C942;
color:#ffffff;
}
#orders tr:nth-child(odd) td 
{
color:#000000;
background-color:#EAF2D3;
}
</style>
</head>

<body>
Hello <br>
Kindly Process Below order.

<table id="orders">
<tr>
	<th >Name</th>
	<th>SKU</th>
	<th>Brand</th>
	<th>Size</th>
	<th>MRP</th>
	<th>Qty</th>
</tr>
<?php foreach($items as $item) {
echo "<tr>";

		echo '<td>'.$item->name.'</td>';
		echo "<td>".$item->sku."</td>";
		echo '<td>'.$item->brand_name.'</td>';
		echo '<td>'.$item->size.'</td>';
		echo '<td>'.$item->mrp.'</td>';
		echo "<td>".($item->qty-$item->procured_qty-$item->qty_refund-$item->qty_cancel)."</td>";
		if($type=='none'){
			echo "<td>".$item->supp_comment."</td>";
		}
		
echo '</tr>';
} ?>
</table>

<br><br>
Regards<br>
------<br>
Suraj<br>
Beautykafe.com<br>


</body>
</html>