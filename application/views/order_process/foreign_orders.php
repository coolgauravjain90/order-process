<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>

<title>Dispatched Orders</title>
<div class="home-title blue-gradient">Dispatched Orders</div>
<br>

<div id="box">
<div id="container">
<?php $attributes = array('id' => 'search_orders');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td>Order No: <input type="text" id="search_order" name="search_order" /></td>
	<td>Store Name: <input type="text" id="search_store_name" name="search_store_name" /></td>
	<td>Internal Status: <input type="text" id="search_internal_status" name="search_internal_status" /></td>
	<td>Created Date: <input type="text" id="search_create_date" name="search_create_date" /></td>
	<td>Customer Name: <input type="text" id="search_name" name="search_name" /></td>
	<td>Total Internal Dispatched: <?php echo $count_foreign_orders; ?></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
$(document).on('change', '#search_orders', function(){
 var search_order = $("input#search_order").val();
 var search_store_name = $("input#search_store_name").val();
 var search_internal_status = $("input#search_internal_status").val();
 var search_create_date = $("input#search_create_date").val();
 var search_name = $("input#search_name").val();

 var string = 'search_order='+search_order+'&search_create_date='+search_create_date+'&search_name='+search_name+'&search_store_name='+search_store_name+'&search_internal_status='+search_internal_status;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_order_process/search_foreign_orders",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.order").remove();
			var col = '';
			for(var i=0;i<data['orders'].length;i++){
				col = '<td>'+data['popup'][data['orders'][i].id]+'</td>';
				
				col += '<td>'+data['orders'][i].store_name+'</td>';
				
				col += '<td><input type="hidden" name="order_id" id="'+data['orders'][i].id+'" value="'+data['orders'][i].order_id+'" />'+data['orders'][i].order_no+'</td>';
				
				if(data['orders'][i].store_name ==+'beautykafe.com')
					col += '<td id="internal_status_'+data['orders'][i].id+'">'+data['orders'][i].internal_status+'</td>';
				else {
					col += '<td>'+data['internal'][data['orders'][i].id]+'</td>';
				}
				
				col += '<td>'+data['orders'][i].amount+'</td>';
				
				if(data['orders'][i].comments) {
					col += '<td id="cmt_'+data['orders'][i].id+'"><span id="comment_'+data['orders'][i].id+'" class="comment editable">'+data['orders'][i].comments+'</span></td>';
				} else {
					col += '<td id="cmt_'+data['orders'][i].id+'"><span id="comment_'+data['orders'][i].id+'" class="comment editable">--</span></td>';
				}
				
				col += '<td>'+data['orders'][i].customer_name+'</td>';
				col += '<td>'+data['orders'][i].street1+'</td>';
				col += '<td>'+data['orders'][i].city+'</td>';
				col += '<td>'+data['orders'][i].state+'</td>';
				
				col += '<td>'+data['orders'][i].pincode+'</td>';
				
				col += '<td>'+data['orders'][i].created_date+'</td>';
				col += '<td>'+data['orders'][i].last_updated+'</td>';
				
				$('#foriegn_orders').append('<tr class="order" id="orders_'+data['orders'][i].id+'">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>
<script>
$(function() {
$('#search_create_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
});
</script>
<div id="container">
<table align="center" class="table table-striped table-bordered" id="foriegn_orders">
<tr>
	<th>Order Id</th>
	<th>Store Name</th>
	<th>Order No.</th>
	<th>Internal Status</th>
	<th>Total Amount</th>
	<th>Comments</th>
	<th>Name</th>
	<th>Address</th>
	<th>City</th>
	<th>State</th>
	<th>Pincode</th>
	<th>Created Date</th>
	<th>Last Updated</th>
</tr>
<?php foreach($orders as $order) {
echo "<tr id='orders_$order->id' class='order'>";
		echo '<td>'.anchor('', $order->order_id, array('class'=>'topopup','id'=>'topopup_'.$order->id)).'</td>';
		
		echo '<td>'.$order->store_name.'</td>';
		
		echo "<td><input type='hidden' name='order_id' id='$order->id' value='$order->order_id' />".$order->order_no."</td>";
		
		$options = array(
			'Cancelled' => 'Cancelled',
			'dispatched'    => 'dispatched'
		);
			
		if($order->store_name=='beautykafe.com') {
			echo "<td id='internal_status_$order->id'>".$order->internal_status."</td>";
		} else {
			echo "<td>".form_dropdown('int_st_store_name',$options, $order->internal_status,'class=int_st_store_name id=int_st_store_'.$order->id)."</td>";
		}
		
		echo "<td>".$order->amount."</td>";
		
		if($order->comments) {
			echo "<td id='cmt_$order->id'><span id='comment_$order->id' class='comment editable'>".$order->comments."</span></td>";
		} else {
			echo "<td id='cmt_$order->id'><span id='comment_$order->id' class='comment editable'>--</span></td>";
		}
		
		echo '<td>'.$order->customer_name.'</td>';
		echo '<td>'.$order->street1.'</td>';
		echo '<td>'.$order->city.'</td>';
		echo '<td>'.$order->state.'</td>';
		echo "<td>".$order->pincode."</td>";
		echo '<td>'.$order->created_date.'</td>';
		echo '<td>'.$order->last_updated.'</td>';
		
echo '</tr>';
} ?>
<tr id="links"><th colspan="18"><?php echo $links; ?></th></tr>
</table>
</div>

<script type="text/javascript">
$(document).on('change', 'select.int_st_store_name', function() {
	var internal_id = $(this).attr('id');
	var internal_status = $(this).val();
	var id = internal_id.match(/[0-9 -()+]+$/,internal_id);
	var order_id = $('#'+id).val();
	$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/change_internal_status_by_ship/'+id,
			dataType:'json',
			data:'int_st_store_name='+internal_status+'&order_id='+order_id,
			success: function(data) {
				if(internal_status=='Cancelled') {
					$('tr.orders_'+id).remove();
				}
			}
		});
		return false;
});

$(document).on('click', 'span.comment', function(){
	var comment_id = $(this).attr('id');
	var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
	var comment_val = $("#"+comment_id).html();
	$("#cmt_"+id).html('<textarea style="resize:none" class="text_comment_val" name="text_comment_val" id="comment_val_'+id+'">'+comment_val+'</textarea>');
	
});
$(document).on('keydown', '.text_comment_val', function(e){
    if(e.which == 13){
		var comment_id = $(this).attr('id');
		var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
		var comment_val = $("#"+comment_id).val();
		$("#comment_val_"+id).replaceWith('<span id="comment_'+id+'" class="comment editable">'+comment_val+'</span>');
		var dataString = 'text_comment_val='+comment_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_order_process/comment_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
			}
		});
		return false;
    }
});
</script>


<div id="toPopup">
	<div class="close"></div>
	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
<div id="container">
<table id="popup_content" class="table table-bordered">
<tr>
	<th class="blue-gradient">Item Id</th>
	<th class="blue-gradient">Item Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Qty</th>
	<th class="blue-gradient">Size</th>
	<th class="blue-gradient">MRP</th>
	<th class="blue-gradient">Sale Price</th>
	<th class="blue-gradient">Procured Qty</th>
	<th class="blue-gradient">Item Status</th>
	<th class="blue-gradient">Item Comments</th>
</tr>
</table>
</div>
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>

<?php 
$this->load->view('component/footer');
?>

<script type="text/javascript">
$(document).on('click', 'a.topopup', function(){
	
	var a_id = $(this).attr('id');
	var id = a_id.match(/[0-9 -()+]+$/,a_id);
	var order_id = $("#"+id).val();
	$.ajax({  
	  type: "POST",  
	  url: '<?php echo base_url();?>c_order_process/get_order_item_by_order_id/'+order_id,  
	  dataType: 'json',
	  data: 'order_id='+order_id,
	  success: function(data) {
		$(".pops").remove();
		var col = '';
		for(var i=0;i<data.length;i++) {
			col = '<td>'+data[i].item_id+'</td>';
			col += '<td>'+data[i].item_code+'</td>';
			col += '<td>'+data[i].name+'</td>';
			col += '<td>'+data[i].brand_name+'</td>';
			col += '<td>'+data[i].qty+'</td>';
			col += '<td>'+data[i].size+'</td>';
			col += '<td>'+data[i].mrp+'</td>';
			col += '<td>'+data[i].sale_price+'</td>';
			col += '<td>'+data[i].procured_qty+'</td>';
			col += '<td>'+data[i].status+'</td>';
			col += '<td>'+data[i].item_comment+'</td>';
			$('#popup_content').append('<tr style="text-align:center" class="pops">'+col+'</tr>');
		}
	  },
	  error: function(data) {
		$(".pops").remove();
	  }
	});  
	return false;  
});
</script>
<script src="<?php echo site_url('assets/js/popup_loader.js'); ?>"></script>