<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Manage Inventory</title>
<div class="home-title blue-gradient">Manage Inventory</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_inventory_products');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Sku: <input type="text" id="search_sku" name="search_sku" /></td>
	<td class="blue-gradient">Bar Code: <input type="text" id="search_bar_code" name="search_bar_code" /></td>
	<td class="blue-gradient">Name: <input type="text" id="search_name" name="search_name" /></td>
	<td class="blue-gradient">Brand: <input type="text" id="search_brand" name="search_brand" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$(document).on('change', '#search_inventory_products', function(){
	var search_sku = $("input#search_sku").val();
	var search_bar_code = $("input#search_bar_code").val();
	var search_name = $("input#search_name").val();
	var search_brand = $("input#search_brand").val();
	
	var string = 'search_sku='+search_sku+'&search_bar_code='+search_bar_code+'&search_name='+search_name+'&search_brand='+search_brand;
	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_manage_products/search_manage_inventory",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.inventories").remove();
			var col = '';
			for(var i=0;i<data['inventories'].length;i++){
			if((data['inventories'][i].pur_qty - data['inventories'][i].memo_qty - data['procured_qty'][data['inventories'][i].item_id]) > 0) {
				col = '<td>'+data['inventories'][i].sku+'</td>';
				col += '<td>'+data['inventories'][i].product_code+'</td>';
				col += '<td>'+data['inventories'][i].name+'</td>';
				col += '<td>'+data['inventories'][i].brand_name+'</td>';
				col += '<td>'+data['inventories'][i].mrp+'</td>';
				col += '<td>'+(data['inventories'][i].pur_qty - data['inventories'][i].memo_qty - data['procured_qty'][data['inventories'][i].item_id])+'</td>';
				
				$('#manage_inventory').append('<tr class="inventories">'+col+'</tr>');
			}
			}
		}
	});
	return false;
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="manage_inventory">
<tr>
	<th class="blue-gradient">Sku</th>
	<th class="blue-gradient">Bar Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Mrp</th>
	<th class="blue-gradient">Qty</th>
</tr>
<?php foreach($inventories as $inventory) {
if(($inventory->pur_qty-$inventory->memo_qty-$procured_qty[$inventory->item_id]) > 0) {
	echo "<tr class=inventories>";
		echo '<td>'.$inventory->sku.'</td>';
		echo '<td>'.$inventory->product_code.'</td>';
		echo '<td>'.$inventory->name.'</td>';
		echo '<td>'.$inventory->brand_name.'</td>';
		echo '<td>'.$inventory->mrp.'</td>';
		echo '<td>'.($inventory->pur_qty-$inventory->memo_qty-$procured_qty[$inventory->item_id]).'</td>';
	echo '</tr>';
}
} ?>
<tr id="links"><th colspan="6"><?php echo $links; ?></th></tr>
</table>
</div>