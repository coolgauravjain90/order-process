<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Manage Brands</title>
<div class="home-title blue-gradient">Manage Brands</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_brands');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Brand: <input type="text" id="search_brand" name="search_brand" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$(document).on('change', '#search_brands', function(){
	var search_brand = $("input#search_brand").val();
	
	var string = 'search_brand='+search_brand;
	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_manage_products/search_manage_brands",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.brands").remove();
			var col = '';
			for(var i=0;i<data.length;i++){
			
				col = '<td>'+data[i].brand_id+'</td>';
				col += '<td>'+data[i].brand_name+'</td>';
				
				$('#manage_brands').append('<tr class="brands">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="manage_brands">
<tr>
	<th class="blue-gradient">Id</th>
	<th class="blue-gradient">Brand</th>
</tr>
<?php foreach($brands as $brand) {
echo "<tr class=brands>";
	echo '<td>'.$brand->brand_id.'</td>';
	echo '<td>'.$brand->brand_name.'</td>';
echo '</tr>';
} ?>
<tr id="links"><th colspan="2"><?php echo $links; ?></th></tr>
</table>
</div>