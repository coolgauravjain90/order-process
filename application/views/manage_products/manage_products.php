<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Manage Products</title>
<div class="home-title blue-gradient">Manage Products</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_products');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Sku: <input type="text" id="search_sku" name="search_sku" /></td>
	<td class="blue-gradient">Bar Code: <input type="text" id="search_bar_code" name="search_bar_code" /></td>
	<td class="blue-gradient">Name: <input type="text" id="search_name" name="search_name" /></td>
	<td class="blue-gradient">Brand: <input type="text" id="search_brand" name="search_brand" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>

<script type="text/javascript">
$(document).on('change', '#search_products', function(){
	var search_sku = $("input#search_sku").val();
	var search_bar_code = $("input#search_bar_code").val();
	var search_name = $("input#search_name").val();
	var search_brand = $("input#search_brand").val();
	
	var string = 'search_sku='+search_sku+'&search_bar_code='+search_bar_code+'&search_name='+search_name+'&search_brand='+search_brand;
	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_manage_products/search_manage_products",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.products").remove();
			var col = '';
			for(var i=0;i<data.length;i++){
			
				col = '<td>'+data[i].id+'</td>';
				col += '<td>'+data[i].sku+'</td>';
				col += '<td>'+data[i].product_code+'</td>';
				col += '<td>'+data[i].name+'</td>';
				col += '<td>'+data[i].brand_name+'</td>';
				col += '<td>'+data[i].size+'</td>';
				col += '<td>'+data[i].weight+'</td>';
				
				$('#manage_products').append('<tr class="products">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="manage_products">
<tr>
	<th class="blue-gradient">Id</th>
	<th class="blue-gradient">Sku</th>
	<th class="blue-gradient">Bar Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Size</th>
	<th class="blue-gradient">Weight</th>
</tr>
<?php foreach($products as $product) {
echo "<tr class=products>";
	echo '<td>'.$product->id.'</td>';
	echo '<td>'.$product->sku.'</td>';
	echo '<td>'.$product->product_code.'</td>';
	echo '<td>'.$product->name.'</td>';
	echo '<td>'.$product->brand_name.'</td>';
	echo '<td>'.$product->size.'</td>';
	echo '<td>'.$product->weight.'</td>';
echo '</tr>';
} ?>
<tr id="links"><th colspan="7"><?php echo $links; ?></th></tr>
</table>
</div>