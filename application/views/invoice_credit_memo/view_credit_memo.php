<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Credit Memo Products</title>
<div class="home-title blue-gradient">Credit Memo Products</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_memos');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Supplier: <input type="text" id="search_supplier" name="search_supplier" /></td>
	<td class="blue-gradient">Invoice No: <input type="text" id="search_inovice_no" name="search_inovice_no" /></td>
	<td class="blue-gradient">Credit Memo Date: <input type="text" id="search_credit_date" name="search_credit_date" /></td>
	<td class="blue-gradient">Sku: <input type="text" id="search_sku" name="search_sku" /></td>
	<td class="blue-gradient">Brand: <input type="text" id="search_brand" name="search_brand" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
<script>
 $(function() {
    $( "#search_credit_date" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true, pickerClass: 'my-picker'});
  });
 </script>
<script type="text/javascript">
$(document).on('change', '#search_memos', function(){
	var search_supplier = $("input#search_supplier").val();
	var search_inovice_no = $("input#search_inovice_no").val();
	var search_credit_date = $("input#search_credit_date").val();
	var search_sku = $("input#search_sku").val();
	var search_brand = $("input#search_brand").val();
	
	var string = 'search_supplier='+search_supplier+'&search_inovice_no='+search_inovice_no+'&search_credit_date='+search_credit_date+'&search_sku='+search_sku+'&search_brand='+search_brand;
	
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>credit_memo/search_credit_memo_items",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.memos").remove();
			var col = '';
			for(var i=0;i<data.length;i++){
			
				col = '<td>'+data[i].id+'</td>';
				col += '<td>'+data[i].supplier_name+'</td>';
				col += '<td>'+data[i].invoice_no+'</td>';
				col += '<td>'+data[i].credit_memo_date+'</td>';
				col += '<td>'+data[i].product_name+'</td>';
				col += '<td>'+data[i].sku+'</td>';
				col += '<td>'+data[i].brand_name+'</td>';
				col += '<td>'+data[i].qty+'</td>';
				
				$('#manage_credit_memos').append('<tr class="memos">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>


<div id="container">
<table align="center" class="table table-striped table-bordered" id="manage_credit_memos">
<tr>
	<th class="blue-gradient">Id</th>
	<th class="blue-gradient">Supplier</th>
	<th class="blue-gradient">Invoice No.</th>
	<th class="blue-gradient">Credit Memo Date</th>
	<th class="blue-gradient">Product Name</th>
	<th class="blue-gradient">Sku</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Qty</th>
</tr>
<?php foreach($memos as $memo) {
echo "<tr class=memos>";
	echo '<td>'.$memo->id.'</td>';
	echo '<td>'.$memo->supplier_name.'</td>';
	echo '<td>'.$memo->invoice_no.'</td>';
	echo '<td>'.$memo->credit_memo_date.'</td>';
	echo '<td>'.$memo->product_name.'</td>';
	echo '<td>'.$memo->sku.'</td>';
	echo '<td>'.$memo->brand_name.'</td>';
	echo '<td>'.$memo->qty.'</td>';
echo '</tr>';
} ?>
<tr id="links"><th colspan="8"><?php echo $links; ?></th></tr>
</table>
</div>