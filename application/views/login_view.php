
<?php
$this->load->view('component/header_login'); 	
$this->load->helper('form'); ?>
<title>OrderProcess | Login</title>
<div class="home-title blue-gradient">Login Page</div>
<div id="login_a">
<div><?php if($msg) echo $msg; ?></div>
<?php echo form_open('loginValidate.php'); ?>
 <table>
	<tr>
		<td>
			<label for="email">Email</label>
			<input type="email" id="email" name="email" value="<?php echo $this->input->post('email'); ?>" required/>
		</td>
		<td><?php echo form_error('email')?></td>
	</tr>
	<tr>
		<td>
			<label for="password">Password</label>
			<input type="password" id="password" name="password" value="<?php echo $this->input->post('password'); ?>" required/>
		</td>
		<td><?php echo form_error('password')?></td>
	</tr>
	<tr>
		<td colspan="2"><?php echo form_submit('login', 'Login', 'class="btn-blue"'); ?></td>
	</tr>
</table>
 <?php echo form_close(); ?>
 </div>
<?php $this->load->view('component/footer'); ?>