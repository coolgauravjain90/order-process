<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Purchase Invoice & Inventory Entry</title>

<div class="home-title blue-gradient">Purchase Product Entry</div>
<br>
<div id="container">
<?php $attributes = array('class' => 'invoice_table');?>
<div class="alert-error">
	<?php echo validation_errors(); ?>
</div>
<?php echo form_open_multipart('', $attributes); ?>
 <table>
	<tr>
		<td>
			<label>Supplier</label>
			<?php echo form_dropdown('supplier', $dropdown, 0, 'id="supplier"'); ?>
		</td>
		<td>
			<label>Invoice Date</label>
			<input type="text" id="invoice_date" name="invoice_date" required" />
			<input type="hidden" id="update_date" name="create_date" />
		</td>
		<td>
			<label>Invoice No.</label>
			<input type="text" id="invoice_no" name="invoice_no" required/>
		</td>
		<td>
			<label>Total Invoice Value</label>
			<input type="text" id="invoice_value" name="invoice_value" value="0" />
		</td>
		<td>
			<label>Correction Value</label>
			<input type="text" id="correction_value" name="correction_value" value="0" disabled />
		</td>
		
	</tr>
</table>
 <?php echo form_close(); ?>
 </div>

<script>
$(function() {
  
$('#invoice_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy',
	pickerClass: 'my-picker'
});


$('.list_exp_date_1').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
 
$('.list_invent_date_1').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});

});
</script>
 
 <script type="text/javascript"> 
 	
$(document).on('focusout', '#invoice_no', function(){
	var invoice_no = $("input#invoice_no").val();
	var supplier = $("select#supplier").val();
	var inovice_date = $("input#invoice_date").val();
	$('.purchase_table #invent_date_1').val(inovice_date);
	var dataString = 'supplier='+supplier+'&invoice_no='+invoice_no;
if(invoice_no=="") {
		$("#stock_product").hide();
		$('.save').attr("disabled", false);
		return false;
} else if(invoice_no) {
	$.ajax({
		type:"POST",
		url:'<?php echo base_url();?>purchase_invoice_entry/validate_supplier_invoice',
		dataType:'json',
		data:dataString,
		success: function(data) {
			alert('This Invoice no  '+invoice_no+' ,against supplier '+supplier+ ' is already exist.');
		}, 
		error:function(data) {
			$("#stock_product").show();
		}
	});
	return false;
}
});
 
</script>

<div id="container" style="position:relative; float:right">
<table>
	<tr>
		<td><label>Invoiced Amount Status<label></td>
	</tr>
	<tr>
		<td id="balance"></td>
	</tr>
	<tr>
		<td>Total Qty:</td><td id="total_qty"></td>
	</tr>
</table>
 </div>
<script type="text/javascript">
 $(document).on('mouseover change keyup', '.purchase_table', function() {
	
	var ids = $(".purchase_table input[name=id]").serializeArray();
	var total_invoice_value = 0;
	
	var invoice_value = parseFloat($('.invoice_table input#invoice_value').val());
	var correction_value = parseFloat($('.invoice_table input#correction_value').val());
	
	for(var i=0;i<ids.length;i++){
		var id = ids[i].value;
		var price = parseFloat($(".purchase_table input#list_price_"+id).val()).toFixed(2);
		var qty = parseInt($(".purchase_table input#list_qty_"+id).val());
		total_invoice_value += price*qty;
	}
	total_invoice_value = (total_invoice_value).toFixed(2);
	var balance = (invoice_value-total_invoice_value);
	
	if(total_invoice_value==(invoice_value+correction_value)) {
		$("td#balance").html('Now You can Update All Information.');
		$('.save').attr("disabled", false);
	} else if(total_invoice_value > (invoice_value+correction_value)) {
		$("td#balance").html('Invoiced Amount Exceeded : '+(-balance));
		$('.save').attr("disabled", true);
		$('.invoice_table input#correction_value').attr("disabled", false);
	} else if(total_invoice_value < (invoice_value+correction_value)) {
		$("td#balance").html('Invoiced Amount Left : '+balance);
		$('.invoice_table input#correction_value').attr("disabled", false);
	}

});

$(document).on('change keyup', '.list_qty', function() {
	
	var ids = $(".purchase_table input[name=id]").serializeArray();
	var total_qty = 0;
	
	for(var i=0;i<ids.length;i++){
		var id = ids[i].value;
		var qty = parseInt($(".purchase_table input#list_qty_"+id).val());
		if( isNaN( qty ) ) qty = 0;
		total_qty += qty;
	}
	$("td#total_qty").html(total_qty);
});

</script>

 
<div id="box">
<div id="container">
<?php $attributes = array('class' => 'purchase_table');?>
<div class="alert-error">
	<?php echo validation_errors(); ?>
</div>
<?php echo form_open_multipart('', $attributes); ?>
 <table class="table table-striped table-bordered" style="display:none" id="stock_product">
 <tbody>
	<tr>
		<td colspan="12"><input type="button" name="Save" class="save btn btn-primary" value="Save All Information" disabled /></td>
		<td><?php echo anchor('purchaseform.php', 'Back', array('class'=>'btn back btn-primary')); ?></td>
	</tr>
	<tr>
		<th class="blue-gradient">Product Image</th>
		<th class="blue-gradient">Bar Code</th>
		<th class="blue-gradient">SKU</th>
		<th class="blue-gradient">Item Id</th>
		<th class="blue-gradient">Name</th>
		<th class="blue-gradient">Brand</th>
		<th class="blue-gradient">Weight</th>
		<th class="blue-gradient">MRP</th>
		<th class="blue-gradient">Discount</th>
		<th class="blue-gradient">Discounted Price</th>
		<th class="blue-gradient">Tax</th>
		<th class="blue-gradient">After Tax</th>
		<th class="blue-gradient">Purchase Date</th>
		<th class="blue-gradient">Expire Date</th>
		<th class="blue-gradient">Qty</th>
		<th class="blue-gradient">Action</th>
	</tr>
	<tr id="pro_list_1">
		<td class="list_product_image_1"><a class="fancybox-effects-a" rel="gallery" href="" id="list_ref_image_1"></a></td>
		<td><input type="hidden" id="id_1" class="id" name="id" value="1" /><input type="text" id="list_bar_code_1" class="bar_code" name="bar_code" required/><input type="hidden" id="list_item_id_1" name="item_id" /></td>
		<td>
		<input type="text" id="list_sku_1" class="sku" name="sku" required/>
		</td>
		<td class="list_item_id_1"></td>
		<td class="list_name_1"></td>
		<td class="list_brand_1"></td>
		<td><input type="text" id="list_weight_1" name="weight" required/></td>
		<td><input type="text" class="list_mrp_1" id="mrp" name="mrp" /></td>
		<td><input type="text" id="list_discount_1" name="discount" class="discount" required/></td>
		
		<td><input type="text" class="price" id="list_price_1" name="price" required/></td>
		
		<td><input type="text" class="tax" id="list_tax_1" name="tax" value="12.5" required/></td>
		<td><input type="text" class="after_tax" id="list_after_tax_1" name="after_tax" value="0" required/></td>
		
		<td><input type="text" class="list_invent_date_1" id="invent_date_1" name="purchase_date" required/></td>
		<td><input type="text" class="list_exp_date_1" id="exp_date_1" name="exp_date" /></td>	
		<td><input type="number" min="1" class="list_qty" id="list_qty_1" name="qty" required/></td>
		<td><input type="button" class="remove btn-primary" id="list_remove_1" value="Remove" name="remove" /></td>
	</tr>
</tbody>
</table>
 <?php echo form_close(); ?>
 </div>
 <div>
 <input type="button" name="add" class="add" value="Add New Row" />
 </div>
 </div>
 <script src="<?php echo site_url('assets/js/inventory_date.js'); ?>"></script>
 <?php
$this->load->view('component/footer'); 	
?>

<script type="text/javascript">
$(document).on('keyup', '.after_tax', function() {
	var after_tax_id = $(this).attr('id');
	var id = after_tax_id.match(/[0-9 -()+]+$/,after_tax_id);
	var after_tax = parseFloat($(this).val());
	var mrp  = parseFloat($("input.list_mrp_"+id).val());
	var tax = parseFloat((mrp-after_tax)*100/mrp).toFixed(2);
	$("#list_tax_"+id).val(tax);
	
});

$(document).on('keyup change', '.tax', function() {
	var tax_id = $(this).attr('id');
	var id = tax_id.match(/[0-9 -()+]+$/,tax_id);
	var tax = parseFloat($(this).val());
	var mrp  = parseFloat($("input.list_mrp_"+id).val());
	var after_tax = parseFloat(mrp-(mrp*tax/100)).toFixed(2);
	$("#list_after_tax_"+id).val(after_tax);
	
});

$(document).on('keyup', '.discount', function() {
	var dis_id = $(this).attr('id');
	var id = dis_id.match(/[0-9 -()+]+$/,dis_id);
	var discount = parseFloat($(this).val());
	var mrp  = parseFloat($("input.list_mrp_"+id).val());
	var price = parseFloat(mrp-(mrp*discount/100)).toFixed(2);
	$("#list_price_"+id).val(price);
	
});

$(document).on('keyup', '.price', function() {
	var dis_id = $(this).attr('id');
	var id = dis_id.match(/[0-9 -()+]+$/,dis_id);
	var price = parseFloat($(this).val());
	var mrp  = parseFloat($("input.list_mrp_"+id).val());
	var discount = parseFloat((mrp-price)*100/mrp).toFixed(2);
	$("#list_discount_"+id).val(discount);
});

$(document).on('change', '.sku', function () {
	var fid = $(this).attr('id');
	var id = fid.match(/[0-9 -()+]+$/,fid);
	var sku = $("#"+fid).val();
	if($('input#list_item_id_'+id).val()!='') return;
	var supplier = $(".invoice_table select#supplier").val();
	$.ajax({
		type: "POST",
		url: '<?php echo base_url();?>purchase_invoice_entry/get_product_info_sku/'+sku,
		dataType:'json',
		data:'supplier='+supplier,
		success: function(data){
			if(!data['pro']) return;
			$('a#list_ref_image_'+id).html('<img src="http://www.beautykafe.com/media/catalog/product/'+data['pro'][0].product_image+'">');
			$('a#list_ref_image_'+id).attr('href','http://www.beautykafe.com/media/catalog/product/'+data['pro'][0].product_image);
			if($('input#list_bar_code_'+id).val()!='' && data['pro'][0].product_code==''){
			}else{
				$('input#list_bar_code_'+id).val(data['pro'][0].product_code);
			}
			$('input#list_item_id_'+id).val(data['pro'][0].id);
			$('td.list_item_id_'+id).html(data['pro'][0].id);
			$('td.list_name_'+id).html(data['pro'][0].name);
			$('td.list_brand_'+id).html(data['pro'][0].brand_name);
			
			$('input#list_weight_'+id).val(data['pro'][0].weight);
			$('input.list_mrp_'+id).val(data['pro'][0].mrp);
			
			$('input#list_discount_'+id).val(data['discount'][0].discount);
			var discounts = parseFloat(data['discount'][0].discount);
			var mrps = parseFloat(data['pro'][0].mrp);
			var prices = parseFloat((mrps-(mrps*discounts)/100)).toFixed(2);
			$(".purchase_table input#list_price_"+id).val(prices);
			
		},
		error: function(data) {
			
		}
	});
	return false;
 });
$(document).on('change', '.bar_code', function () {
	var fid = $(this).attr('id');
	var id = fid.match(/[0-9 -()+]+$/,fid);
	var bar_code = $("#"+fid).val();
	if($('input#list_item_id_'+id).val()!='') return;
	var supplier = $(".invoice_table select#supplier").val();
	$.ajax({
	   type: "POST",
		url: '<?php echo base_url();?>purchase_invoice_entry/get_product_info_bar_code',
		dataType:'json',
		data:'supplier='+supplier+'&bar_code='+bar_code,
		success: function(data){
			if(!data['pro']) return;
			$('a#list_ref_image_'+id).html('<img src="http://www.beautykafe.com/media/catalog/product/'+data['pro'][0].product_image+'">');
			$('a#list_ref_image_'+id).attr('href','http://www.beautykafe.com/media/catalog/product/'+data['pro'][0].product_image);
			if($('input#list_sku_'+id).val()!='' && data['pro'][0].sku==''){
			}else{
				$('input#list_sku_'+id).val(data['pro'][0].sku);
			}
			$('input#list_sku_'+id).val(data['pro'][0].sku);
			$('input#list_item_id_'+id).val(data['pro'][0].id);
			$('td.list_item_id_'+id).html(data['pro'][0].id);
			$('td.list_name_'+id).html(data['pro'][0].name);
			$('td.list_brand_'+id).html(data['pro'][0].brand_name);
			$('input#list_weight_'+id).val(data['pro'][0].weight);
			$('input.list_mrp_'+id).val(data['pro'][0].mrp);
			$('input#list_discount_'+id).val(data['discount'][0].discount);
			
			var discounts = parseFloat(data['discount'][0].discount);
			var mrps = parseFloat(data['pro'][0].mrp);
			var prices = parseFloat((mrps-(mrps*discounts)/100)).toFixed(2);
			if( isNaN( prices ) ) prices = 0.00;
			
			$(".purchase_table input#list_price_"+id).val(prices);
		},
		error: function(data) {
			
		}
	});
	return false;
 });
 

$(document).ready(function(){
    $('.fancybox-effects-a').fancybox({
		helpers:  {
            overlay : {
                css : {
                    'background' : 'rgba(255,255,255,0.5)'
                }
            }
        }
	});
});

 $(document).on('click', '.remove', function() {
	
	var r_id = $(this).attr('id');
	var id = r_id.match(/[0-9 -()+]+$/,r_id);
	var rowCount = $('table#stock_product tr:last').index() + 1;
	if(rowCount>3) {
		$("tr#pro_list_"+id).remove();
	} else {
		return false;
	}
 });
 
 

var counter = 2;
$(document).on('click', '.add', function(){
	var col='';
	var invent_date = $('.invoice_table #invoice_date').val();
	col = '<td class="list_product_image_'+counter+'"><a class="fancybox-effects-a" rel="gallery" href="" id="list_ref_image_'+counter+'"></a></td>';
	col += '<td><input type="hidden" id="id_'+counter+'" name="id" class="id" value="'+counter+'" /><input type="text" id="list_bar_code_'+counter+'" name="bar_code" class="bar_code" required/><input type="hidden" id="list_item_id_'+counter+'" name="item_id" /></td>';
	
	col += '<td><input type="text" id="list_sku_'+counter+'" name="sku" class="sku" required/></td>';
	
	col += '<td class="list_item_id_'+counter+'"></td>';
	col += '<td class="list_name_'+counter+'"></td>';
	col += '<td class="list_brand_'+counter+'"></td>';
	
	col += '<td><input type="text" id="list_weight_'+counter+'" name="weight" required/></td>';
	
	col += '<td><input type="text" class="list_mrp_'+counter+'" id="mrp" name="mrp" /></td>';
	
	col += '<td><input type="text" class="discount" id="list_discount_'+counter+'" name="discount" required/></td>';
	
	col += '<td><input type="text" class="price" id="list_price_'+counter+'" name="price" required/></td>';
	
	col += '<td><input type="text" class="tax" id="list_tax_'+counter+'" name="tax" value="12.5" required/></td>';
	
	col += '<td><input type="text" class="after_tax" id="list_after_tax_'+counter+'" name="after_tax" value="0" required/></td>';
	
	col += '<td><input type="text" id="invent_date_'+counter+'" class="list_invent_date_'+counter+'" name="purchase_date" value="'+invent_date+'" required/></td>';
	
	col += '<td><input type="text" class="list_exp_date_'+counter+'" id="exp_date_'+counter+'" name="exp_date" required/></td>';
	
	col += '<td><input type="number" min="1" class="list_qty" id="list_qty_'+counter+'" name="qty" required/></td>';
	
	col += '<td><input type="button" class="remove btn-primary" id="list_remove_'+counter+'" value="Remove" name="remove" /></td>';
	
	$('#stock_product > tbody:last').append('<tr id="pro_list_'+counter+'">'+col+'</tr>');
	
	$('#exp_date_'+counter).datepicker('destroy');
	$('#exp_date_'+counter).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy'
	});
	$('#invent_date_'+counter).datepicker('destroy');
	$('#invent_date_'+counter).datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd/mm/yy'
	});
	
	counter++;
	return false;
});

$(document).on('click', '.save', function(){
	$('.save').attr("disabled", true);
	$('.back').attr("disabled", true);
	$('.add').attr("disabled", true);
	$('.remove').attr("disabled", true);

	var invoice_no = $(".invoice_table input#invoice_no").val();
	var supplier = $(".invoice_table select#supplier").val();
	var invoice_date = $(".invoice_table input#invoice_date").val();
	var create_date = $(".invoice_table input#update_date").val();
	var correction_value = $(".invoice_table input#correction_value").val();
	
	var dataString = 'supplier='+supplier+'&invoice_no='+invoice_no+'&invoice_date='+invoice_date+'&create_date='+create_date+'&correction_value='+correction_value;
	
	if(invoice_no=="") {
		alert("Fill the Invoice No Box");
		$("#stock_product").hide();
		$('.save').attr("disabled", false);
		$('.back').attr("disabled", false);
		$('.add').attr("disabled", false);
		$('.remove').attr("disabled", false);
		return false;
	}
	
	$(".invoice_table input#invoice_no").attr("disabled", true);
	$(".invoice_table select#supplier").attr("disabled", true);
	$(".invoice_table input#invoice_date").attr("disabled", true);
	$(".invoice_table input#correction_value").attr("disabled", true);
	
	var val_bar = $(".purchase_table input[name=bar_code]").serializeArray();
	for(var i=0;i<val_bar.length;i++) {
		var id = val_bar[i].value;
		if(id=="") {
			alert("fill bar code column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_sku = $(".purchase_table input[name=sku]").serializeArray();
	for(var i=0;i<val_sku.length;i++) {
		var id = val_sku[i].value;
		if(id=="") {
			alert("fill the sku column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_weight = $(".purchase_table input[name=weight]").serializeArray();
	for(var i=0;i<val_weight.length;i++) {
		var id = val_weight[i].value;
		if(id=="") {
			alert("fill weight column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_price = $(".purchase_table input[name=price]").serializeArray();
	for(var i=0;i<val_price.length;i++) {
		var id = val_price[i].value;
		if(id=="0.00") {
			alert("fill the price column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_purchase_date = $(".purchase_table input[name=purchase_date]").serializeArray();
	for(var i=0;i<val_purchase_date.length;i++) {
		var id = val_purchase_date[i].value;
		if(id=="") {
			alert("fill the purchase date column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_exp_date = $(".purchase_table input[name=exp_date]").serializeArray();
	for(var i=0;i<val_exp_date.length;i++) {
		var id = val_exp_date[i].value;
		if(id=="") {
			alert("fill the expire date column, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	var val_qty = $(".purchase_table input[name=qty]").serializeArray();
	for(var i=0;i<val_qty.length;i++) {
		var id = val_qty[i].value;
		if(id=="" || id <0) {
			alert("fill the qty, row:"+(i+1));
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			return false;
		}
	}
	
if(confirm("Are you sure? All the information is correct.")) {
	
	var post_data = {'invoice':{'supplier':supplier, 'invoice_no':invoice_no, 'invoice_date':invoice_date,'create_date':create_date,'correction_value':correction_value}};
	
	var ids = $(".purchase_table input[name=id]").serializeArray();
	post_data['invoice']['items'] = new Array();	
	for(var i=0;i<ids.length;i++) {
		var id = ids[i].value;
		post_data['invoice']['items'][i] = {'item_id':$(".purchase_table input#list_item_id_"+id).val(),'sku':$(".purchase_table input#list_sku_"+id).val(),'bar_code':$(".purchase_table input#list_bar_code_"+id).val(),'weight':$(".purchase_table input#list_weight_"+id).val(),'price':$(".purchase_table input#list_price_"+id).val(),'purchase_date':$(".purchase_table input.list_invent_date_"+id).val(),'exp_date':$(".purchase_table input.list_exp_date_"+id).val(),'qty':$(".purchase_table input#list_qty_"+id).val(),'mrp':$(".purchase_table input.list_mrp_"+id).val()}
	}
	$(".purchase_table input").attr("disabled", true);
	$.ajax({
		type:"POST",
		url:'<?php echo base_url();?>purchaseform_one.php',
		dataType:'json',
		data:post_data,
		success: function(data) {
			
		}, 
		error:function(data) {
			$(".purchase_table input").attr("disabled", false);
			$('.save').attr("disabled", false);
			$('.back').attr("disabled", false);
			$('.add').attr("disabled", false);
			$('.remove').attr("disabled", false);
			alert('invoice error');
		}
	});
	return false;
}
$(".purchase_table input").attr("disabled", false);
$('.save').attr("disabled", false);
$('.back').attr("disabled", false);
$('.remove').attr("disabled", false);
$('.add').attr("disabled", false);
});

</script>

		
	