<!DOCTYPE html>
<?php $this->load->helper('url');?>
<html>
<head>
	<meta charset="UTF-8">
	<script src="<?php echo site_url('assets/js/jquery.min.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/currentTime.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/randomNumber.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/jquery-ui.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/jquery.datePicker.js'); ?>"></script>
	<script src="<?php echo site_url('assets/js/jquery.validate.js'); ?>"></script>
	<script src="<?php echo site_url('assets/source/jquery.fancybox.pack.js'); ?>"></script>
	<link href="<?php echo site_url('assets/source/jquery.fancybox.css'); ?>" rel="stylesheet" media="screen" type="text/css">
	<link href="<?php echo site_url('assets/css/jquery-ui.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/datePicker.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
	<link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Pragma" content="no-cache" /> 
	<meta http-equiv="Expires" content="-1" />
</head>
<body class=container-fluid>
<div id="container">
<ul  id="menu">
<li><?php echo anchor('orders.php', 'Home'); ?></li>
<li><a>Products</a>
<ul>
	<li><?php echo anchor('manage_products.php', 'Manage Products'); ?></li>
	<li><?php echo anchor('manage_inventory.php', 'Manage Inventory'); ?></li>
	<li><?php echo anchor('manage_brands.php', 'Manage Brands'); ?></li>
</ul>
</li>
<li><a>Suppliers</a>
<ul>
	<li><?php echo anchor('manage_suppliers.php', 'Manage Suppliers'); ?></li>
	<li><?php echo anchor('manage_discount_rule.php', 'Manage Discounts'); ?></li>
	<li><?php echo anchor('discount_rule.php', 'Discount Rule'); ?></li>
</ul>
</li>
<li><a>Purchase</a>
<ul>
	<li><?php echo anchor('purchaseform.php', 'Add Invoice'); ?></li>
	<li><?php echo anchor('allinvoice.php', 'View Invoices'); ?></li>
	<li><?php echo anchor('memo_purchase_product.php', 'Create Credit Memo'); ?> </li>
	<li><?php echo anchor('view_credit_memo.php', 'View Credit Memo'); ?> </li>
	<li><?php echo anchor('manage_inventory.php', 'View Inventory'); ?></li>
</ul>
</li>

<li><a>Orders</a><ul>
	<li><?php echo anchor('orders.php', 'All Orders'); ?></li>
	<li><?php echo anchor('pending_orders.php', 'Pending Orders'); ?></li>
	<li><?php echo anchor('ready_to_ship_orders.php', 'Ready to Ship Orders'); ?></li>
	<li><?php echo anchor('foreign_orders.php', 'Dispatched Orders'); ?></li>
	<li><?php echo anchor('holded_orders.php', 'Holded Orders'); ?></li>
	<li><?php echo anchor('export_ready_to_ship_orders.php', 'Export Ready to Ship'); ?></li>
</ul></li>

<li><a>Order Items</a><ul>
	<li><?php echo anchor('order_items.php', 'Order Items'); ?></li>
	<li><?php echo anchor('procured_order_items.php', 'Procured Order Items'); ?></li>
	<li><?php echo anchor('pending_order_items.php', 'Pending Order Items'); ?></li>
	<li><?php echo anchor('pending_export.php', 'Export Pending Items'); ?></li>
</ul></li>

<li><a>Shipments</a><ul>
	<li><?php echo anchor('pending_shipment.php', 'Dispatched'); ?></li>
	<li><?php echo anchor('delivered_shipment.php', 'Delivered'); ?></li>
	<li><?php echo anchor('returned_shipment.php', 'Returned'); ?></li>
</ul></li>
<li><a>Logistics</a><ul>
	<li>View Partners</li>
	<li>Add Partner</li>
	<li><a href="http://dev.beautykafe.com/ci_sync/upload_pincodes.php" target="#">Add Pincodes</a></li>
</ul></li>

<li><a>Marketplaces</a><ul>
	<li><a href="http://dev.beautykafe.com/ci_sync/insert_new_vendor_orders.php" title="Insert Orders" target="#">Another Stores Orders</a></li>
</ul></li>
<li style="float:right"><?php echo anchor('logout.php', 'Logout'); ?></li>
</ul>
</div>

