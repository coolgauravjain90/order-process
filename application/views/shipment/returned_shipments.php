<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Returned Shipment</title>
<div class="home-title blue-gradient">Returned Shipment</div>
<br>

<div id="box">
<div id="container">
<?php $attributes = array('id' => 'search_shipments');
$options = array(
			'' => '==Select==',
			'delivered'  => 'delivered',
			'dispatched'    => 'dispatched',
			'processing'   => 'processing',
			'return' => 'return',
			'returned' => 'returned',
			'stuck' => 'stuck',
			);
?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td>Order Id: <input type="text" id="search_order_id" name="search_order_id" /></td>
	<td>Ship by: <input type="text" id="search_ship_by" name="search_ship_by" /></td>
	<td>Order Status: <input type="text" id="search_order_status" name="search_order_status" /></td>
	<td>Payment Mode: <input type="text" id="search_payment_mode" name="search_payment_mode" /></td>
	<td>Shipment Status:<?php echo form_dropdown('search_shipment_status', $options, 1, 'id="search_shipment_status"'); ?></td>
	<td>Returned Shipment: <?php echo $returned_shipment; ?></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
</div>

<script type="text/javascript">
$(document).on('change', '#search_shipments', function(){
 var search_order_id = $("input#search_order_id").val();
 var search_ship_by = $("input#search_ship_by").val();
 var search_order_status = $("input#search_order_status").val();
 var search_payment_mode = $("input#search_payment_mode").val();
 var search_shipment_status = $("select#search_shipment_status").val();

 var string = 'search_order_id='+search_order_id+'&search_ship_by='+search_ship_by+'&search_order_status='+search_order_status+'&search_payment_mode='+search_payment_mode+'&search_shipment_status='+search_shipment_status;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>c_shipment/search_returned_shipments/",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.shipment").remove();
			var col = '';
			for(var i=0;i<data['shipments'].length;i++){
				
				col = '<td><input type="hidden" name="id" value="'+data['shipments'][i].id+'" id="'+data['shipments'][i].id+'" />'+data['shipments'][i].shipment_id+'</td>';
				
				col += '<td>'+data['shipments'][i].order_no+'</td>';
				col += '<td>'+data['shipments'][i].shipped_amt+'</td>';
				col += '<td>'+data['shipments'][i].status+'</td>';
				col += '<td>'+data['shipments'][i].created_date+'</td>';
				col += '<td>'+data['shipments'][i].shipped_on+'</td>';
				col += '<td>'+data['shipments'][i].payment_mode+'</td>';
				col += '<td>'+data['shipments'][i].shipped_by+'</td>';
				if(data['shipments'][i].shipped_by+'==ECOM') {
					col += '<td><a href="http://www.ecomexpress.in/track-me/?awb='+data['shipments'][i].awb+'" target="_blank">'+data['shipments'][i].awb+'</a></td>';
				} else if(data['shipments'][i].shipped_by+'==Delhivery') {
					col += '<td><a href="http://track.delhivery.com/p/'+data['shipments'][i].awb+'" target="_blank">'+data['shipments'][i].awb+'</a></td>';
				}
				else if(data['shipments'][i].shipped_by+'==Firstflight') {
					col += '<td><a href="www.firstflight.net/n_contrac_new.asp?tracking1='+data['shipments'][i].awb+'" target="_blank">'+data['shipments'][i].awb+'</a></td>';
				} else {
					col += '<td>'+data['shipments'][i].awb+'</td>';
				}
				
				// if(data['shipments'][i].shipment_status) {
					// col += '<td id="ship_st_'+data['shipments'][i].id+'"><span id="shipment_status_'+data['shipments'][i].id+'" class="shipment_status editable">'+data['shipments'][i].shipment_status+'</span></td>';
				// } else {
					// col += '<td id="ship_st_'+data['shipments'][i].id+'"><span id="shipment_status_'+data['shipments'][i].id+'" class="shipment_status editable">--</span></td>';
				// }
				
				col += '<td>'+data['status'][data['shipments'][i].id]+'</td>';
				
				if(data['shipments'][i].current_status) {
					col += '<td id="curr_st_'+data['shipments'][i].id+'"><span id="current_status_'+data['shipments'][i].id+'" class="current_status editable">'+data['shipments'][i].current_status+'</span></td>';
				} else {
					col += '<td id="curr_st_'+data['shipments'][i].id+'"><span id="current_status_'+data['shipments'][i].id+'" class="current_status editable">--</span></td>';
				}
				
				col += '<td>'+data['shipments'][i].city+'</td>';
				col += '<td>'+data['shipments'][i].pincode+'</td>';
				
				if(data['shipments'][i].comments) {
					col += '<td id="cmt_'+data['shipments'][i].id+'"><span id="comments_'+data['shipments'][i].id+'" class="comments editable">'+data['shipments'][i].comments+'</span></td>';
				} else {
					col += '<td id="cmt_'+data['shipments'][i].id+'"><span id="comments_'+data['shipments'][i].id+'" class="comments editable">--</span></td>';
				}
				
				$('#pending_shipments').append('<tr class="shipment" id="shipment_'+data['shipments'][i].id+'">'+col+'</tr>');
				
			}
		}
	});
	return false;
});
</script>

<div id="box">
<div id="container">
<table align="center" class="table table-striped table-bordered" id="pending_shipments">
<tr>
	<th>Shipment Id</th>
	<th>Order Id</th>
	<th>Item Total</th>
	<th>Order Status</th>
	<th>Created on</th>
	<th>Shipped on</th>
	<th>Payment Mode</th>
	<th>Ship by</th>
	<th>Tracking</th>
	<th>Shipment Status</th>
	<th>Current Status</th>
	<th>City</th>
	<th>Pincode</th>
	<th>Comments</th>
</tr>
<?php 
$options1 = array(
			'delivered'  => 'delivered',
			'dispatched'    => 'dispatched',
			'processing'   => 'processing',
			'return' => 'return',
			'returned' => 'returned',
			'stuck' => 'stuck',
			);
foreach($shipments as $shipment) {
echo "<tr id='shipment_$shipment->id' class='shipment'>";

		echo "<td><input type='hidden' name='id' value='$shipment->id' id='$shipment->id' />".$shipment->shipment_id."</td>";
		echo '<td>'.$shipment->order_no.'</td>';
		echo '<td>'.$shipment->shipped_amt.'</td>';
		echo "<td>".$shipment->status."</td>";
		echo '<td>'.$shipment->created_date.'</td>';
		echo '<td>'.$shipment->shipped_on.'</td>';
		echo '<td>'.$shipment->payment_mode.'</td>';
		echo '<td>'.$shipment->shipped_by.'</td>';
		
		if($shipment->shipped_by=="ECOM") {
			echo "<td><a href='http://www.ecomexpress.in/track-me/?awb=$shipment->awb' target='_blank'>".$shipment->awb."</td>";
		} elseif($shipment->shipped_by=="Delhivery") {
			echo "<td><a href='http://track.delhivery.com/p/$shipment->awb' target='_blank'>".$shipment->awb."</td>";
		} elseif($shipment->shipped_by=="First Flig") {
			echo "<td><a href='www.firstflight.net/n_contrac_new.asp?tracking1=$shipment->awb'>".$shipment->awb."</td>";
		} else {
			echo '<td>'.$shipment->awb.'</td>';
		}
		
		// if($shipment->shipment_status) {
			// echo "<td id='ship_st_$shipment->id'><span id='shipment_status_$shipment->id' class='shipment_status editable'>".$shipment->shipment_status."</span></td>";
		// } else {
			// echo "<td id='ship_st_$shipment->id'><span id='shipment_status_$shipment->id' class='shipment_status editable'>--</span></td>";
		// }
		
		echo "<td>".form_dropdown('shipment_status', $options1, $shipment->shipment_status, 'class=shipment_status id=shipment_status_'.$shipment->id)."</td>";
		
		if($shipment->current_status) {
			echo "<td id='curr_st_$shipment->id'><span id='current_status_$shipment->id' class='current_status editable'>".$shipment->current_status."</span></td>";
		} else {
			echo "<td id='curr_st_$shipment->id'><span id='current_status_$shipment->id' class='current_status editable'>--</span></td>";
		}
		
		echo '<td>'.$shipment->city.'</td>';
		echo '<td>'.$shipment->pincode.'</td>';
		
		if($shipment->comments) {
			echo "<td id='cmt_$shipment->id'><span id='comments_$shipment->id' class='comments editable'>".$shipment->comments."</span></td>";
		} else {
			echo "<td id='cmt_$shipment->id'><span id='comments_$shipment->id' class='comments editable'>--</span></td>";
		}
		
echo '</tr>';
} ?>
<tr id="links"><th colspan="14"><?php echo $links; ?></th></tr>
</table>
</div>
</div>

<script type="text/javascript">
$(document).on('change', 'select.shipment_status', function(){
	var shipment_st_id = $(this).attr('id');
	var id = shipment_st_id.match(/[0-9 -()+]+$/,shipment_st_id);
	var st_val = $(this).val();
	var dataString = 'shipment_status='+st_val;
	$.ajax({
		type:"POST",
		url:'<?php echo base_url(); ?>c_shipment/shipment_status_submit/'+id,
		dataType:'json',
		data:dataString,
		success: function(data) {
				
		}
	});
	return false;
});

// $(document).on('click', 'span.shipment_status', function(){
	// var ship_st_id = $(this).attr('id');
	// var id = ship_st_id.match(/[0-9 -()+]+$/,ship_st_id);
	// var st_val = $("#"+ship_st_id).html();
	// $("#ship_st_"+id).html('<input class="text_shipment_status_val" name="text_shipment_status_val" id="ship_st_val_'+id+'" value="'+st_val+'" />');
	
// });
// $(document).on('keydown', '.text_shipment_status_val', function(e){
    // if(e.which == 13){
		// var shipment_st_id = $(this).attr('id');
		// var id = shipment_st_id.match(/[0-9 -()+]+$/,shipment_st_id);
		// var st_val = $("#"+shipment_st_id).val();
		// $("#ship_st_val_"+id).replaceWith('<span id="shipment_status_'+id+'" class="shipment_status editable">'+st_val+'</span>');
		// var dataString = 'text_shipment_status_val='+st_val;
		// $.ajax({
			// type:"POST",
			// url:'<?php echo base_url(); ?>c_shipment/shipment_status_submit/'+id,
			// dataType:'json',
			// data:dataString,
			// success: function(data) {
				
			// }
		// });
		// return false;
    // }
// });

$(document).on('click', 'span.current_status', function(){
	var curr_st_id = $(this).attr('id');
	var id = curr_st_id.match(/[0-9 -()+]+$/,curr_st_id);
	var st_val = $("#"+curr_st_id).html();
	$("#curr_st_"+id).html('<textarea class="text_current_status_val" name="text_current_status_val" id="curr_st_val_'+id+'">'+st_val+'</textarea>');
	
});
$(document).on('keydown', '.text_current_status_val', function(e){
    if(e.which == 13){
		var curr_st_id = $(this).attr('id');
		var id = curr_st_id.match(/[0-9 -()+]+$/,curr_st_id);
		var st_val = $("#"+curr_st_id).val();
		$("#curr_st_val_"+id).replaceWith('<span id="current_status_'+id+'" class="current_status editable">'+st_val+'</span>');
		var dataString = 'text_current_status_val='+st_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_shipment/current_status_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
    }
});

$(document).on('click', 'span.comments', function(){
	var cmt_id = $(this).attr('id');
	var id = cmt_id.match(/[0-9 -()+]+$/,cmt_id);
	var cmt_val = $("#"+cmt_id).html();
	$("#cmt_"+id).html('<textarea class="text_comment_val" name="text_comment_val" id="cmt_val_'+id+'">'+cmt_val+'</textarea>');
	
});
$(document).on('keydown', '.text_comment_val', function(e){
    if(e.which == 13){
		var cmt_id = $(this).attr('id');
		var id = cmt_id.match(/[0-9 -()+]+$/,cmt_id);
		var cmt_val = $("#"+cmt_id).val();
		$("#cmt_val_"+id).replaceWith('<span id="comments_'+id+'" class="comments editable">'+cmt_val+'</span>');
		var dataString = 'text_comment_val='+cmt_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>c_shipment/comment_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
				
			}
		});
		return false;
    }
});


</script>



<?php 
$this->load->view('component/footer');
?>