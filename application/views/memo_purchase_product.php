<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>

<title>Credit Memo</title>
<div class="home-title blue-gradient">Credit Memo</div>
<br>

<div id="container">
<?php $attributes = array('class' => 'credit_memo');?>
<?php echo form_open_multipart('', $attributes); ?>
 <table>
	<tr>
		<td>
			<label>Supplier</label>
			<?php echo form_dropdown('supplier', $dropdown, 0, 'id="supplier"'); ?>
			<input type="hidden" id="invoice_id" name="invoice_id" />
		</td>
		<td>
			<label>Invoice Date</label>
			<input type="text" id="invoice_date" name="invoice_date" required/>
			<input type="hidden" id="credit_date" name="credit_date" required/>
		</td>
		<td>
			<label>Invoice No.</label>
			<input type="text" id="invoice_no" name="invoice_no" value="<?php echo $this->input->post('invoice_no'); ?>" required/>
		</td>
	</tr>
	<tr>
		<td colspan="1" align="center"><input type="button" name="search" value="Search" class="search btn btn-primary" disabled /></td>
		
		<td colspan="2" align="center"><?php echo anchor('memo_purchase_product.php','Back', array('class' => 'btn btn-primary back', 'name' => 'back')); ?></td>
	</tr>
</table>
 <?php echo form_close(); ?>
 </div>
<script>
 $(function() {
    $( "#invoice_date" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth: true, changeYear: true, pickerClass: 'my-picker'});
  });
 </script>
 <script src="<?php echo site_url('assets/js/credit_memo_date.js'); ?>"></script>
 
 
 <script type="text/javascript">
 /*Save invoice value, supplier & get purchase product list for invoice date is 0 */
 
 $(document).on('focusout', '#invoice_no', function(){
	
	var invoice_no = $(".credit_memo input#invoice_no").val();
	var supplier = $(".credit_memo select#supplier").val();
	var invoice_date = $(".credit_memo input#invoice_date").val();
	var dataString = 'supplier='+supplier+'&invoice_no='+invoice_no+'&invoice_date='+invoice_date;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url();?>credit_memo/validate_supplier_invoice",
		dataType:'json',
		data:dataString,
		success: function(data) {
			if(data) {
				$('.search').attr("disabled", false);
				$('.credit_memo input#invoice_id').val(data);
			}
		},
		error: function(){
			alert('This Invoice no  '+invoice_no+' ,against supplier '+supplier+ ' on this date '+invoice_date+' is not exist.');
		}
	});
	return false;
 });
 
 
 $(document).on('click', '.search', function(){
	$('.search').attr("disabled", true);
	
	var invoice_id = $(".credit_memo input#invoice_id").val();  

	$.ajax({  
	  type: "POST",  
	  url: '<?php echo base_url();?>credit_memo/get_invoiced_purchase_product/'+invoice_id,  
	  dataType: 'json',
	  success: function(data) {
			var col = '';
			for(var i=0;i<data.length;i++){
				$('#product_list').show();
			
				col = '<td><input class="in" type="checkbox" name="in" id="in_'+data[i].id+'" required/><input type="hidden" name="id" id="id_'+data[i].id+'" value="'+data[i].id+'" required/></td>';
				
				col += '<td>'+data[i].id+'</td>';
				
				col += '<td class="list_sku" id="list_sku_'+data[i].id+'">'+data[i].sku+'</td>';
				
				col += '<td class="list_name" id="list_name_'+data[i].id+'">'+data[i].name+'</td>';
				
				col += '<td class="list_brand" id="list_brand_'+data[i].id+'">'+data[i].brand_name+'</td>';
				
				col += '<td class="list_item_id" id="list_item_id_'+data[i].id+'">'+data[i].item_id+'</td>';
				
				col += '<td class="list_after_tax" id="list_after_tax_'+data[i].id+'">'+data[i].after_tax+'</td>';
				
				col += '<td class="list_qty" id="list_qty_'+data[i].id+'">'+data[i].qty+'</td>';
				
				$('#product_list').append('<tr id="list_'+data[i].id+'">'+col+'</tr>');
				
			}
			
	  },
	  error: function(data) {
		$('.search').attr("disabled", false);
	  }
	});  
	return false;  
});
</script>


<div id="container">
<table align="center" class="table table-condensed table-striped table-bordered" id="product_list" style="display:none">
		
		<td class="blue-gradient">Action</td>
		<td class="blue-gradient">Id</td>
		<td class="blue-gradient">SKU</td>
		<td class="blue-gradient">Name</td>
		<td class="blue-gradient">Brand</td>
		<td class="blue-gradient">Item Id</td>
		<td class="blue-gradient">Inc. Tax Price</td>
		<td class="blue-gradient">Qty</td>
</table>
</div>

<script type="text/javascript">

/* Create form for purchase products by id for this particular invoice date and id */
$(document).on('click', '.in', function(){
	var fid = $(this).attr('id');
	var f_id = fid.match(/[0-9 -()+]+$/,fid);
	var id = $("input#id_"+f_id).val();
	var sku = $("#list_sku_"+f_id).html();
	var name = $("#list_name_"+f_id).html();
	var brand = $("#list_brand_"+f_id).html();
	var item_id = $("#list_item_id_"+f_id).html();
	var after_tax = $("#list_after_tax_"+f_id).html();
	var qty = $("#list_qty_"+f_id).html();
	
	$("#list_"+f_id).fadeOut('fast', function() {$(this).remove();});
	$('.memo').attr("disabled", false);
	var col = '';
	$('#product_inventory').show();
	col = '<td><input type="hidden" name="id" id="id_'+id+'" value="'+id+'" required/><input type="hidden" name="credit_memo_id" id="credit_memo_id_'+id+'" value="" required/><input type="hidden" name="item_id" id="item_id_'+id+'" value="'+item_id+'" required/>'+id+'</td>';
	
	col += '<td class="sku" id="product_sku_'+id+'">'+sku+'</td>';
	
	col += '<td class="product_name" id="product_name_'+id+'">'+name+'</td>';
	
	col += '<td class="brand_name" id="product_brand_'+id+'">'+brand+'</td>';
	
	col += '<td>'+item_id+'</td>';
	
	col += '<td class="after_tax" id="after_tax_'+id+'">'+after_tax+'</td>';
	
	col += '<td><input type="number" min="1" max="'+qty+'" class="qty_change" name="qty" id="qty_'+id+'" value="'+qty+'" required/></td>';
	
	col += '<td><input type=button class="remove" id="remove_'+id+'" title="Remove This Row" value="Remove" /></td>';
	
	$('#product_inventory').append('<tr class="inventory" id="product_'+id+'">'+col+'</tr>');
				
});
</script>


<div id="container">
<form action="" class="inventory_product">
<table align="center" class="table table-striped table-bordered" id="product_inventory" style="display:none">
<tr><td colspan="9"><input type="button" name="memo" value="Credit Memo" class="memo btn btn-primary" /></td></tr>
	<tr>
		<th class="blue-gradient">Id</th>
		<th class="blue-gradient">SKU</th>
		<th class="blue-gradient">Name</th>
		<th class="blue-gradient">Brand</th>
		<th class="blue-gradient">Item Id</th>
		<th class="blue-gradient">Inc. Tax Price</th>
		<th class="blue-gradient">Qty</th>
		<th class="blue-gradient">Action</th>
	</tr>
</table>
</form>
</div>

<script type="text/javascript">

/*Removing Row product_inventory table*/
$(document).on('click', '.remove', function(){
	var fid = $(this).attr('id');
	var f_id = fid.match(/[0-9 -()+]+$/,fid);
	var sku = $('#product_sku_'+f_id).html();
	var name = $('#product_name_'+f_id).html();
	var brand = $('#product_brand_'+f_id).html();	
	var item_id = $('#item_id_'+f_id).val();	
	var after_tax = $('#after_tax_'+f_id).html();	
	var qty = $('#qty_'+f_id).attr("max");	
	var col = '';
	
	col = '<td><input class="in" type="checkbox" name="in" id="in_'+f_id+'" required/><input type="hidden" name="id" id="id_'+f_id+'" value="'+f_id+'" required/></td>';
	
	col += '<td>'+f_id+'</td>';
	
	col += '<td class="list_sku" id="list_sku_'+f_id+'">'+sku+'</td>';
	
	col += '<td class="list_name" id="list_name_'+f_id+'">'+name+'</td>';
	
	col += '<td class="list_brand" id="list_brand_'+f_id+'">'+brand+'</td>';
	
	col += '<td class="list_item_id" id="list_item_id_'+f_id+'">'+item_id+'</td>';
				
	col += '<td class="list_after_tax" id="list_after_tax_'+f_id+'">'+after_tax+'</td>';
				
	col += '<td class="list_qty" id="list_qty_'+f_id+'">'+qty+'</td>';
	
	$('#product_list').append('<tr id="list_'+f_id+'">'+col+'</tr>');
	$("#product_"+f_id).remove();
	
});


/* Update Invoice value, unit price, tax, qty, price after tax for form#inventory_product */
$(document).on('click', '.memo', function(){
	$('.memo').attr("disabled", true);
	
	var invoice_id = $(".credit_memo input#invoice_id").val();  
	var credit_date = $(".credit_memo input#credit_date").val();
	
if(confirm("Are you sure? All the information is correct.")) {
	$.ajax({  
	  type: "POST",  
	  url: '<?php echo base_url();?>credit_memo/create_credit_memo_invoice/'+invoice_id,  
	  dataType: 'json',
	  data:'credit_date='+credit_date,
	  success: function(data) {
			
			var ids = $(".inventory_product input[name=id]").serializeArray();
			var credit_memo_id = data;
			for(var i=0;i<ids.length;i++) {
				var iddss = ids[i].value;
				$(".inventory_product input#credit_memo_id_"+iddss).val(data);
			}
			for(var i=0;i<ids.length;i++) {
				$('.memo').attr("disabled", true);
				var id = ids[i].value;
				var item_id = $(".inventory_product input#item_id_"+id).val();
				var credit_id = $(".inventory_product input#credit_memo_id_"+id).val();
				var qty = $(".inventory_product input#qty_"+id).val();
				
				var dataString = 'item_id=' + item_id + '&credit_memo_id=' + credit_id + '&qty=' + qty;
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url().'credit_memo/create_credit_memo_product/'?>",
					dataType: 'json',
					data: dataString,
					success: function(response){
						alert('Successful');
					}, 
					error:function() {
						alert('Update Error');
					}
				});
			}
			return false;
	    },	
	   error: function(data) {
			alert('Credit Memo Error');
		}
	});
	return false;
}
});
</script>


<?php $this->load->view('component/footer'); ?>