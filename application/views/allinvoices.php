<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat,26 Jul 1991 05:00:00 GMT");
?>

<?php
$this->load->view('component/header'); 	
?>
<title>Invoices</title>
<div class="home-title blue-gradient">Invoices</div>
<br>

<div id="container">
<?php $attributes = array('id' => 'search_invoices');?>
<?php echo form_open_multipart('', $attributes); ?>
<table align="center" class="table table-striped table-bordered">
<tr>
	<td class="blue-gradient">Invoice Id: <input type="text" id="search_invoice_id" name="search_invoice_id" /></td>
	<td class="blue-gradient">Supplier Name: <input type="text" id="search_supplier" name="search_supplier" /></td>
	<td class="blue-gradient">Invoice No.: <input type="text" id="search_invoice_no" name="search_invoice_no" /></td>
	<td class="blue-gradient">Invoice Date: <input type="text" id="search_invoice_date" name="search_invoice_date" /></td>
</tr>
</table>
<?php echo form_close(); ?>
</div>
<script type="text/javascript">
$(document).on('keyup change', '#search_invoices', function(){
 var search_invoice_id = $("input#search_invoice_id").val();
 var search_supplier = $("input#search_supplier").val();
 var search_invoice_no = $("input#search_invoice_no").val();
 var search_invoice_date = $("input#search_invoice_date").val();
 
 var string = 'search_invoice_id='+search_invoice_id+'&search_supplier='+search_supplier+'&search_invoice_no='+search_invoice_no+'&search_invoice_date='+search_invoice_date;
	$.ajax({
		type:"POST",
		url:"<?php echo base_url(); ?>invoice_detail/search_invoice",
		dataType:'json',
		data:string,
		success: function(data) {
			$("tr.invoices").remove();
			var col = '';
			for(var i=0;i<data['invoices'].length;i++){
				col = '<td><a href="<?php echo base_url(); ?>invoice_detail/get_purchase_product_invoice/'+data['invoices'][i].id+'" class="topopup">'+data['invoices'][i].id+'</a></td>';
				col += '<td>'+data['invoices'][i].name+'</td>';
				
				col += '<td id="i_no_'+data['invoices'][i].id+'"><span id="invoice_no_'+data['invoices'][i].id+'" class="invoice">'+data['invoices'][i].invoice_no+'</span></td>';
				
				col += '<td>'+data['invoices'][i].date+'</td>';
				col += '<td>'+(data['invoices'][i].total_invoice_val-data['invoices'][i].credit_invoice_val)+'</td>';
				col += '<td>'+data['invoices'][i].correction_amt+'</td>';
				if(data['invoices'][i].payment_status) {
					col += '<td id="st_'+data['invoices'][i].id+'"><span id="status_'+data['invoices'][i].id+'" class="status editable">'+data['invoices'][i].payment_status+'</span></td>';
				} else {
					col += '<td id="st_'+data['invoices'][i].id+'"><span id="status_'+data['invoices'][i].id+'" class="status editable">empty</span></td>';
				}
				if(data['invoices'][i].comment) {
					col += '<td id="cmt_'+data['invoices'][i].id+'"><span id="comment_'+data['invoices'][i].id+'" class="comment editable">'+data['invoices'][i].comment+'</span></td>';
				} else {
					col += '<td id="cmt_'+data['invoices'][i].id+'"><span id="comment_'+data['invoices'][i].id+'" class="comment editable">empty</span></td>';
				}
				col+= '<td>'+data['invoices'][i].created_at+'</td>';
				col+= '<td>'+data['invoices'][i].updated_at+'</td>';
				
				$('#all_invoices').append('<tr class="invoices" id="invoice_'+data['invoices'][i].id+'">'+col+'</tr>');
			}
		}
	});
	return false;
});
</script>
<script>
$(function() {
$('#search_invoice_date').datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'dd/mm/yy'
});
});
</script>

<div id="container">
<table align="center" class="table table-striped table-bordered" id="all_invoices">
<tr>
	<th class="blue-gradient">Invoice Id</th>
	<th class="blue-gradient">Supplier</th>
	<th class="blue-gradient">Invoice No.</th>
	<th class="blue-gradient">Invoice Date</th>
	<th class="blue-gradient">Invoice Value(Rs.)</th>
	<th class="blue-gradient">Correction Amount(Rs.)</th>
	<th class="blue-gradient">Payment Status</th>
	<th class="blue-gradient">Comment</th>
	<th class="blue-gradient">Create At</th>
	<th class="blue-gradient">Update At</th>
</tr>
<?php foreach($invoices as $invoice) {
echo "<tr id=invoice_$invoice->id class=invoices>";
		echo '<td>'.anchor('invoice_detail/get_purchase_product_invoice/'.$invoice->id, $invoice->id, array('class'=>'topopup')).'</td>';
		echo '<td>'.$invoice->name.'</td>';
		
		echo "<td id='i_no_$invoice->id'><span id='invoice_no_$invoice->id' class='invoice'>".$invoice->invoice_no."</span></td>";
		
		echo '<td>'.$invoice->date.'</td>';
		echo '<td>'.($invoice->total_invoice_val-$invoice->credit_invoice_val).'</td>';
		echo '<td>'.$invoice->correction_amt.'</td>';
		
		if($invoice->payment_status) {
			echo "<td id='st_$invoice->id'><span id='status_$invoice->id' class='status editable'>".$invoice->payment_status."</span></td>";
		} else {
			echo "<td id='st_$invoice->id'><span id='status_$invoice->id' class='status editable'>--</span></td>";
		}
		
		if($invoice->comment) {
			echo "<td id='cmt_$invoice->id'><span id='comment_$invoice->id' class='comment editable'>".$invoice->comment."</span></td>";
		} else {
			echo "<td id='cmt_$invoice->id'><span id='comment_$invoice->id' class='comment editable'>--</span></td>";
		}
		
		echo '<td>'.$invoice->created_at.'</td>';
		echo '<td>'.$invoice->updated_at.'</td>';
echo '</tr>';
} ?>
<tr id="links"><th colspan="10"><?php echo $links; ?></th></tr>
</table>
</div>
<script type="text/javascript">
$(document).on('click', 'span.invoice', function(){
	var invoice_no_id = $(this).attr('id');
	var id = invoice_no_id.match(/[0-9 -()+]+$/,invoice_no_id);
	var invoice_no = $("#"+invoice_no_id).html();
	$("#i_no_"+id).html('<input type="text" class="text_invoice_no" name="text_invoice_no" id="invoice_val_'+id+'" value="'+invoice_no+'" />');
});
$(document).on('keydown', '.text_invoice_no', function(e){
    if((e.which == 13) || (e.which == 9)){
		var invoice_no_id = $(this).attr('id');
		var id = invoice_no_id.match(/[0-9 -()+]+$/,invoice_no_id);
		var invoice_no = $("#"+invoice_no_id).val();
	if(invoice_no.length > 0) {
		$("#invoice_val_"+id).replaceWith('<span id="invoice_no_'+id+'" class="invoice">'+invoice_no+'</span>');
		var dataString = 'text_invoice_no='+invoice_no;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>invoice_detail/change_invoice_no/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
			}
		});
		return false;
    }
	}
});


$(document).on('click', 'span.comment', function(){
	var comment_id = $(this).attr('id');
	var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
	var comment_val = $("#"+comment_id).html();
	$("#cmt_"+id).html('<textarea style="resize:none" class="text_comment_val" name="text_comment_val" id="comment_val_'+id+'">'+comment_val+'</textarea>');
	
});
$(document).on('keydown focusout', '.text_comment_val', function(e){
    if((e.which == 13) || (e.which == 9)){
		var comment_id = $(this).attr('id');
		var id = comment_id.match(/[0-9 -()+]+$/,comment_id);
		var comment_val = $("#"+comment_id).val();
		$("#comment_val_"+id).replaceWith('<span id="comment_'+id+'" class="comment editable">'+comment_val+'</span>');
		var dataString = 'text_comment_val='+comment_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>invoice_detail/comment_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
			}
		});
		return false;
    }
});

$(document).on('click', 'span.status', function(){
	var status_id = $(this).attr('id');
	var id = status_id.match(/[0-9 -()+]+$/,status_id);
	var status_val = $("#"+status_id).html();
	$("#st_"+id).html('<input type="text" class="text_status_val" name="text_status_val" id="status_val_'+id+'" value="'+status_val+'" />');
});
$(document).on('keydown', '.text_status_val', function(e){
    if((e.which == 13) || (e.which == 9)){
		var status_id = $(this).attr('id');
		var id = status_id.match(/[0-9 -()+]+$/,status_id);
		var status_val = $("#"+status_id).val();
		$("#status_val_"+id).replaceWith('<span id="status_'+id+'" class="status editable">'+status_val+'</span>');
		var dataString = 'text_status_val='+status_val;
		$.ajax({
			type:"POST",
			url:'<?php echo base_url(); ?>invoice_detail/status_submit/'+id,
			dataType:'json',
			data:dataString,
			success: function(data) {
			}
		});
		return false;
    }
});
</script>

<div id="toPopup">
	<div class="close"></div>
	<span class="ecs_tooltip">Press Esc to close <span class="arrow"></span></span>
<div id="container">
<table id="popup_content" class="table table-bordered">
<tr>
	<th class="blue-gradient">SKU</th>
	<th class="blue-gradient">Item Id</th>
	<th class="blue-gradient">Item Code</th>
	<th class="blue-gradient">Name</th>
	<th class="blue-gradient">Brand</th>
	<th class="blue-gradient">Weight</th>
	<th class="blue-gradient">Qty</th>
	<th class="blue-gradient">Purchase Date</th>
	<th class="blue-gradient">Exp Date</th>
	<th class="blue-gradient">Inc. Tax Price</th>
</tr>
</table>
</div>
</div>
<div class="loader"></div>
<div id="backgroundPopup"></div>


<script src="<?php echo site_url('assets/js/popup_loader.js'); ?>"></script>

<script type="text/javascript">
$(document).on('click', 'a.topopup', function(){
	
	var invoice_id = $(this).html();  
	$.ajax({  
	  type: "POST",  
	  url: '<?php echo base_url();?>invoice_detail/get_purchase_product_invoice/'+invoice_id,  
	  dataType: 'json',
	  success: function(data) {
		$(".pops").remove();
		var col = '';
		for(var i=0;i<data.length;i++) {
			col = '<td>'+data[i].sku+'</td>';
			col += '<td>'+data[i].item_id+'</td>';
			col += '<td>'+data[i].product_code+'</td>';
			col += '<td>'+data[i].name+'</td>';
			col += '<td>'+data[i].brand_name+'</td>';
			col += '<td>'+data[i].weight+'</td>';
			col += '<td>'+data[i].qty+'</td>';
			col += '<td>'+data[i].date+'</td>';
			col += '<td>'+data[i].exp_date+'</td>';
			col += '<td>'+data[i].after_tax+'</td>';
			$('#popup_content').append('<tr style="text-align:center" class="pops">'+col+'</tr>');
		}
	  },
	  error: function(data) {
		$(".pops").remove();
	  }
	});  
	return false;  
});
</script>
<?php 
$this->load->view('component/footer');
?>