<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_Suppliers_M extends CI_Model {
	
	public function __construct () {
		parent::__construct();
		$this->load->database();
	}
	
/*Discount Rules */
	public function get_all_discount_rules($per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("discount_rule.id, discount_rule.name, discount_rule.segment, discount_rule.discount, discount_rule.rule_priority, discount_rule.supplier_priority, discount_rule.status, (suppliers.name) as supplier_name, brand.brand_name");
		
		$this->db->from('discount_rule');
		$this->db->join('suppliers', 'suppliers.id=discount_rule.supplier', 'left');
		$this->db->join('brand', 'brand.brand_id=discount_rule.brand', 'left');
		
		$this->db->group_by('discount_rule.name');
		$this->db->order_by('discount_rule.supplier_priority');
		
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_discount_rules($search_rule_name,$search_supplier,$search_brand,$search_status) {
		
		$this->db->select("discount_rule.id, discount_rule.name, discount_rule.segment, discount_rule.discount, discount_rule.rule_priority, discount_rule.supplier_priority, discount_rule.status, (suppliers.name) as supplier_name, brand.brand_name");
		
		$this->db->from('discount_rule');
		$this->db->join('suppliers', 'suppliers.id=discount_rule.supplier', 'left');
		$this->db->join('brand', 'brand.brand_id=discount_rule.brand', 'left');
		
		$array = array();
		if($search_rule_name !=null){
			$array['discount_rule.name'] = $search_rule_name;
		}
		if($search_supplier !=null){
			$array['suppliers.name'] = $search_supplier;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		if($search_status !=null){
			$array['discount_rule.status'] = $search_status;
		}
		$this->db->like ($array);
		
		$this->db->group_by('discount_rule.name');
		$this->db->order_by('discount_rule.supplier_priority');
		
		$query = $this->db->get();
		return $query->result();
	}
	
	public function submit_discounts_discount_rule($id, $discount) {
		$array = array('discount'=>$discount);
		$this->db->where('id', $id);
		$this->db->update('discount_rule', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_rules_priority_discount_rule($id, $priority) {
		$array = array('rule_priority'=>$priority);
		$this->db->where('id', $id);
		$this->db->update('discount_rule', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_suppliers_priority_discount_rule($id, $priority) {
		$array = array('supplier_priority'=>$priority);
		$this->db->where('id', $id);
		$this->db->update('discount_rule', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_status_discounts_rule($id, $status) {
		$array = array('status'=>$status);
		$this->db->where('id', $id);
		$this->db->update('discount_rule', $array);
		return $this->db->affected_rows();	
	}
	
/* Manage Suppliers */

	public function get_all_suppliers($per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("id, name, about, pocontact, mobile, phone, email, email2, street1, city, state, pincode, vat_no, cst_no, status");
		$this->db->from('suppliers');
		$this->db->group_by('name');
		$this->db->order_by('name');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function submit_abouts_suppliers($id, $about) {
		$array = array('about'=>$about);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_pocontact_suppliers($id, $po) {
		$array = array('pocontact'=>$po);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_mobile_suppliers($id, $mobile) {
		$array = array('mobile'=>$mobile);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_phone_supplier($id, $phone) {
		$array = array('phone'=>$phone);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	public function submit_email_supplier($id, $email) {
		$array = array('email'=>$email);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	public function submit_email2_supplier($id, $email) {
		$array = array('email2'=>$email);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_street_supplier($id, $street) {
		$array = array('street1'=>$street);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_city_supplier($id, $city) {
		$array = array('city'=>$city);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_state_supplier($id, $state) {
		$array = array('state'=>$state);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_pincode_supplier($id, $pin) {
		$array = array('pincode'=>$pin);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_cst_supplier($id, $cst) {
		$array = array('cst_no'=>$cst);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_vat_supplier($id, $vat) {
		$array = array('vat_no'=>$vat);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_status_supplier($id, $status) {
		$array = array('status'=>$status);
		$this->db->where('id', $id);
		$this->db->update('suppliers', $array);
		return $this->db->affected_rows();	
	}
	
	public function add_new_supplier($name, $contact_person, $address, $city, $state, $pincode, $mobile, $phone, $email, $email2, $status) {
		
		$data = array(
		   'name' => $name ,
		   'pocontact' => $contact_person,
		   'street1' => $address,
		   'city' => $city,
		   'state' => $state,
		   'pincode' => $pincode,
		   'mobile' => $mobile,
		   'phone' => $phone,
		   'email' => $email,
		   'email2' => $email2,
		   'status' => $status
		);
		$this->db->insert('suppliers', $data); 
		return $this->db->affected_rows();	
	}
	
	public function remove_supplier($id) {
		$this->db->where('id', $id);
		$this->db->delete('suppliers'); 
		return $this->db->affected_rows();	
	}
	
}