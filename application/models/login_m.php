<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_M extends CI_Model {

	function __construct ()
	{
		parent::__construct();
		$this->load->database();
		if ( ! $this->migration->current()) {
			show_error($this->migration->error_string());
		}
	}
	
	public function is_user_exist($email){
		$this->db->select('*');
		$this->db->where('email',$email);
		$query=$this->db->get('user');
		if($query -> num_rows()> 0){
			return true;
		}else{
			return false;
		}
	}
	public function is_account_active($email){
		$this->db->select('accountactive');
		$this->db->where('email',$email);
		$query=$this->db->get('user');
		$query=$query->row_array(); 
		return $query['accountactive'];
	}
	public function is_account_blocked($email){
		$this->db->select('accountblocked');
		$this->db->where('email',$email);
		$query=$this->db->get('user');
		$query=$query->row_array(); 
		if($query['accountblocked']=='0'){
			return true;
		}else{
			return false;
		}
	}
	public function get_password_by_email($email){
		$this->db->select('password');
		$this->db->where('email',$email);
		$query=$this->db->get('user');
		$query=$query->row_array(); 
		return $query['password'];
	}
	
	public function update_lastlogin_date($email){
		$this->db->where('email',$email);
		return $this->db->update('user', array('lastloggenindate'=>time()));
	}	
}