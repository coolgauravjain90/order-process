<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage_Products_M extends CI_Model {
	
	public function __construct () {
		parent::__construct();
		$this->load->database();
	}
	
/* Manage Products view */
	public function get_all_products($per_page,$start) {
	
		$this->db->limit($per_page,$start);
		$this->db->select("products.id, products.sku, products.product_code, products.name, products.brand_id, products.size, products.mrp, products.weight, brand.brand_name");
		$this->db->from('products');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		$this->db->group_by('products.id');
		$this->db->order_by('products.name','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_all_products($search_sku, $search_bar_code, $search_name, $search_brand) {
	
		$this->db->select("products.id, products.sku, products.product_code, products.name, products.brand_id, products.size, products.mrp, products.weight, brand.brand_name");
		$this->db->from('products');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$array = array();
		if($search_sku !=null){
			$array['products.sku'] = $search_sku;
		}
		if($search_bar_code !=null){
			$array['products.product_code'] = $search_bar_code;
		}
		if($search_name !=null){
			$array['products.name'] = $search_name;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		$this->db->like ($array);
		$this->db->group_by('products.id');
		$this->db->order_by('products.name','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
/* Manage Brands */
	
	public function get_all_brands($per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("brand_id, brand_name");
		$this->db->from('brand');
		$this->db->order_by('brand_name', 'asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_all_brands($search_brand) {
		$this->db->select("brand_id, brand_name");
		$this->db->from('brand');
		$array = array();
		if($search_brand !=null){
			$array['brand_name'] = $search_brand;
		}
		$this->db->like ($array);
		$this->db->order_by('brand_name', 'asc');
		$query = $this->db->get();
		return $query->result();
	}
	
/* Manage Inventory */
	
	public function get_all_inventory($per_page,$start) {
	
		$this->db->limit($per_page,$start);
		
		$this->db->select("purchase_table.item_id, purchase_table.sku, products.product_code, products.name, products.brand_id, products.mrp, brand.brand_name, SUM(purchase_table.qty) as pur_qty, SUM(credit_memo_purchase.qty) as memo_qty");
		
		$this->db->from('purchase_table');
		$this->db->join('products', 'products.id=purchase_table.item_id', 'left');
		$this->db->join('brand', 'brand.brand_id=products.brand_id', 'left');
		$this->db->join('credit_memo_purchase', 'credit_memo_purchase.item_id=purchase_table.item_id', 'left');
		
		$this->db->group_by('purchase_table.item_id');
		$this->db->order_by('purchase_table.item_id','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_all_inventory($search_sku, $search_bar_code, $search_name, $search_brand) {
	
		$this->db->select("purchase_table.item_id, purchase_table.sku, products.product_code, products.name, products.brand_id, products.mrp, brand.brand_name, SUM(purchase_table.qty) as pur_qty, , SUM(credit_memo_purchase.qty) as memo_qty");
		
		$this->db->from('purchase_table');
		$this->db->join('products', 'products.id=purchase_table.item_id', 'left');
		$this->db->join('brand', 'brand.brand_id=products.brand_id', 'left');
		$this->db->join('credit_memo_purchase', 'credit_memo_purchase.item_id=purchase_table.item_id', 'left');
		
		$array = array();
		if($search_sku !=null){
			$array['purchase_table.sku'] = $search_sku;
		}
		if($search_bar_code !=null){
			$array['products.product_code'] = $search_bar_code;
		}
		if($search_name !=null){
			$array['products.name'] = $search_name;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		$this->db->like ($array);
		$this->db->group_by('purchase_table.item_id');
		$this->db->order_by('purchase_table.item_id','asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_procured_item_qty($item_id) {
		$this->db->select("SUM(order_item_table.procured_qty) as pro_qty");
		$this->db->from('order_item_table');
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		$this->db->where('order_item_table.item_id',$item_id);
		$this->db->where_not_in('order_table.status', array('canceled', 'pending', 'holded', 'unconfirmed', 'paymentfail'));
		
		$this->db->where("(order_item_table.procured_qty>0)");
		$query = $this->db->get();
		if($query->num_rows >= 1) {
			$results = $query->result();
			$pro_sum;
			foreach($results as $key) {
				$pro_sum = $key->pro_qty;
			}
			return $pro_sum;
		} else {
			return 0;
		}
	}
}