<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_Shipment extends CI_Model {
	
	public function __construct () {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_pending_shipments($per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");
		
		$this->db->from('shipment');
		
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$this->db->where("(shipment.shipment_status='dispatched')");
		
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
	
	public function count_all_pending_shipments() {
		$this->db->select('shipment.id');
		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		$this->db->where("(shipment.shipment_status='dispatched')");
		$this->db->group_by('shipment.order_no');

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function submit_shipment_status($id, $shipment_status) {
		$array = array('shipment_status'=>$shipment_status);
		$this->db->where('id', $id);
		$this->db->update('shipment', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_current_status($id, $current_status) {
		$array = array('current_status'=>$current_status);
		$this->db->where('id', $id);
		$this->db->update('shipment', $array);
		return $this->db->affected_rows();	
	}
	
	public function submit_comments($id, $comment) {
		$array = array('comments'=>$comment);
		$this->db->where('id', $id);
		$this->db->update('shipment', $array);
		return $this->db->affected_rows();	
	}
	
	public function search_pending_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status,$per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");

		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$array = array();
		if($search_order_id !=null){
			$array['shipment.order_no'] = $search_order_id;
		}
		if($search_ship_by !=null){
			$array['shipment.shipped_by'] = $search_ship_by;
		}
		if($search_order_status !=null){
			$array['order_table.status'] = $search_order_status;
		}
		if($search_payment_mode !=null){
			$array['order_table.payment_mode'] = $search_payment_mode;
		}
		if($search_shipment_status !=null){
			$array['shipment.shipment_status'] = $search_shipment_status;
		}
		
		$this->db->like ($array);
		$this->db->where("(shipment.shipment_status='dispatched')");
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
	
/* Delivered Shipments */

	public function get_all_delivered_shipments($per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");
		
		$this->db->from('shipment');
		
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$this->db->where("(shipment.shipment_status='delivered')");
		
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
	
	public function count_all_delivered_shipments() {
		$this->db->select('shipment.id');
		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		$this->db->where("(shipment.shipment_status='delivered')");
		$this->db->group_by('shipment.order_no');

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function search_delivered_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status) {
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");

		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$array = array();
		if($search_order_id !=null){
			$array['shipment.order_no'] = $search_order_id;
		}
		if($search_ship_by !=null){
			$array['shipment.shipped_by'] = $search_ship_by;
		}
		if($search_order_status !=null){
			$array['order_table.status'] = $search_order_status;
		}
		if($search_payment_mode !=null){
			$array['order_table.payment_mode'] = $search_payment_mode;
		}
		if($search_shipment_status !=null){
			$array['shipment.shipment_status'] = $search_shipment_status;
		}
		
		$this->db->like ($array);
		$this->db->where("(shipment.shipment_status='delivered')");
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
	
/* Returned Shipments */

	public function get_all_returned_shipments($per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");
		
		$this->db->from('shipment');
		
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$this->db->where("(shipment.shipment_status='returned')");
		
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
	
	public function count_all_returned_shipments() {
		$this->db->select('shipment.id');
		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		$this->db->where("(shipment.shipment_status='returned')");
		$this->db->group_by('shipment.order_no');

		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function search_returned_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status) {
		$this->db->select("shipment.id, shipment.shipment_id, shipment.order_no, shipment.shipped_on, shipment.shipped_amt, shipment.shipped_by, shipment.awb, shipment.shipment_status, shipment.current_status, shipment.comments, shipment.last_updated, order_table.created_date, order_table.city, order_table.pincode, order_table.status, order_table.payment_mode");

		$this->db->from('shipment');
		$this->db->join('order_table', 'order_table.order_no=shipment.order_no', 'left');
		
		$array = array();
		if($search_order_id !=null){
			$array['shipment.order_no'] = $search_order_id;
		}
		if($search_ship_by !=null){
			$array['shipment.shipped_by'] = $search_ship_by;
		}
		if($search_order_status !=null){
			$array['order_table.status'] = $search_order_status;
		}
		if($search_payment_mode !=null){
			$array['order_table.payment_mode'] = $search_payment_mode;
		}
		if($search_shipment_status !=null){
			$array['shipment.shipment_status'] = $search_shipment_status;
		}
		
		$this->db->like ($array);
		$this->db->where("(shipment.shipment_status='returned')");
		$this->db->group_by('shipment.order_no');
		$this->db->order_by('shipment.shipment_id', 'desc');

		$query = $this->db->get();
		return $query->result();
	}
}