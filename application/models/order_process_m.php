<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_Process_M extends CI_Model {
	
	public function __construct () {
		parent::__construct();
		$this->load->database();
	}
	
	public function get_all_orders($per_page, $start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_table.id,order_table.order_id, order_table.store_name, order_table.order_no, order_table.created_date, order_table.status, order_table.last_updated, order_table.amount, order_table.internal_status, order_table.comments, order_table.payment_mode, order_table.customer_id, order_table.customer_name, order_table.email, order_table.email2, order_table.street1, order_table.street2, order_table.city, order_table.state, order_table.pincode");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_order_item_by_order_id($order_id) {
		$this->db->select("order_item_table.id, order_item_table.order_id, order_item_table.item_id, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, order_item_table.item_comment");
		$this->db->from('order_item_table');
		$this->db->join('products', 'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		$this->db->where('order_item_table.order_id', $order_id);
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_all_order_item_by_order_id($order_id) {
	
		$this->db->where('order_id',$order_id);
		$query = $this->db->get('order_item_table');
		$results =  $query->result();
		$status = array();
		foreach($results as $result) {
				array_push($status, $result->status);
		}
		$unique_items=array_merge(array_unique($status));
	
		$set_status = 'Processing';
		if((sizeof($unique_items) == 1) && ($unique_items[0]=='Procured')) {
			$set_status = 'Ready to Ship';
		}
		if((sizeof($unique_items) == 1) && ($unique_items[0] == 'Cancelled')) {
			$set_status = 'Cancelled';
		}
		if((sizeof($unique_items) == 2) && (($unique_items[0] == 'Cancelled') && ($unique_items[1] == 'Procured'))) {
			$set_status = 'Partial Ready to Ship';
		}
		if((sizeof($unique_items) == 2) && (($unique_items[0] == 'Procured') && ($unique_items[1] == 'Cancelled'))) {
			$set_status = 'Partial Ready to Ship';
		}
		
		$update= array('internal_status'=>$set_status);
		$this->db->where('order_id', $order_id);
		$this->db->update('order_table',$update);
		return $set_status;
	}
	
	public function search_orders($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode, $per_page,$start) {
		$this->db->limit($per_page,$start);
		$array = array();
		if($search_store !=null){
			$array['store_name'] = $search_store;
		}
		if($search_order !=null){
			$array['order_no'] = $search_order;
		}
		if($search_sys_status !=null){
			$array['status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['internal_status'] = $search_internal_status;
		}
		if($search_payment_mode !=null){
			$array['payment_mode'] = $search_payment_mode;
		}
		if($search_create_date !=null){
			$array['created_date'] = $search_create_date;
		}
		if($search_name !=null){
			$array['customer_name'] = $search_name;
		}
		$this->db->like ($array);
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	public function order_comment_submit($comment, $id) {
		$array = array('comments'=>$comment);
		$this->db->where('id', $id);
		$this->db->update('order_table', $array);
		return $this->db->affected_rows();
	}

/*ready to ship orders*/	
	public function export_all_ready_to_ship_orders() {
		
		$this->db->select("order_table.id, order_table.store_name, order_table.order_no, order_table.payment_mode, order_table.pincode");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Ready to Ship' OR order_table.internal_status='Partial Ready to Ship')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'processing','procurement', 'inprocurement', 'holded'));
		$this->db->where('order_table.store_name','beautykafe.com');
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		
		return $query->result();
	}

	public function get_all_ready_to_ship_orders($per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_table.id, order_table.order_id, order_table.store_name, order_table.order_no, order_table.created_date, order_table.status, order_table.last_updated, order_table.amount, order_table.internal_status, order_table.comments, order_table.payment_mode, order_table.customer_id, order_table.customer_name, order_table.email, order_table.email2, order_table.street1, order_table.street2, order_table.city, order_table.state, order_table.pincode");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Ready to Ship' OR order_table.internal_status='Partial Ready to Ship')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'processing','procurement', 'inprocurement', 'holded'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_ready_to_ship_order($search_store,$search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode, $per_page,$start) {
		$this->db->limit($per_page,$start);
		$array = array();
		if($search_store !=null){
			$array['store_name'] = $search_store;
		}
		if($search_order !=null){
			$array['order_no'] = $search_order;
		}
		if($search_sys_status !=null){
			$array['status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['internal_status'] = $search_internal_status;
		}
		if($search_payment_mode !=null){
			$array['payment_mode'] = $search_payment_mode;
		}
		if($search_create_date !=null){
			$array['created_date'] = $search_create_date;
		}
		if($search_name !=null){
			$array['customer_name'] = $search_name;
		}
		$this->db->like ($array);
		$this->db->where("(internal_status='Ready to Ship' OR internal_status='Partial Ready to Ship')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'processing','procurement', 'inprocurement', 'holded'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	public function count_all_ready_to_ship_orders() {
		$this->db->select("order_table.id");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Ready to Ship' OR order_table.internal_status='Partial Ready to Ship')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'processing','procurement', 'inprocurement', 'holded'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function submit_internal_status_by_ship($id, $internal_status,$order_id) {
		if($internal_status=='Cancelled') {
			$array2 = array('status'=>'Cancelled','procured_qty'=>-1);
			$this->db->where('order_id', $order_id);
			$this->db->update('order_item_table', $array2);
		}
		$array = array('internal_status'=>$internal_status);
		$this->db->where('id', $id);
		$this->db->update('order_table', $array);
		return $this->db->affected_rows();	
	}

/* Others Stores Orders */
	public function get_all_foreign_dispatched_orders($per_page,$start) {
		$this->db->limit($per_page,$start);

		$this->db->where_in('internal_status',array('dispatched'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	public function search_foreign_order($search_order, $search_create_date, $search_name, $search_store_name, $search_internal_status) {
		
		$array = array();
		if($search_order !=null){
			$array['order_no'] = $search_order;
		}
		if($search_internal_status !=null){
			$array['internal_status'] = $search_internal_status;
		}
		if($search_store_name !=null){
			$array['store_name'] = $search_store_name;
		}
		if($search_create_date !=null){
			$array['created_date'] = $search_create_date;
		}
		if($search_name !=null){
			$array['customer_name'] = $search_name;
		}
		$this->db->like ($array);
		$this->db->where_in('internal_status',array('dispatched','Cancelled'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	public function count_foreign_dispatched_orders() {
		$this->db->select('id');
		$this->db->from('order_table');
		$this->db->where_in('internal_status',array('dispatched'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
/* pending Orders */
	public function get_all_pending_orders($per_page,$start) {
	
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_table.id, order_table.order_id, order_table.store_name, order_table.order_no, order_table.created_date, order_table.status, order_table.last_updated, order_table.amount, order_table.internal_status, order_table.comments, order_table.payment_mode, order_table.customer_id, order_table.customer_name, order_table.email, order_table.email2, order_table.street1, order_table.street2, order_table.city, order_table.state, order_table.pincode");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Processing' OR order_table.internal_status='Pending')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'procurement','processing'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_pending_order($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode, $per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$array = array();
		if($search_store !=null){
			$array['store_name'] = $search_store;
		}
		if($search_order !=null){
			$array['order_no'] = $search_order;
		}
		if($search_sys_status !=null){
			$array['status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['internal_status'] = $search_internal_status;
		}
		if($search_payment_mode !=null){
			$array['payment_mode'] = $search_payment_mode;
		}
		if($search_create_date !=null){
			$array['created_date'] = $search_create_date;
		}
		if($search_name !=null){
			$array['customer_name'] = $search_name;
		}
		$this->db->like ($array);
		$this->db->where("(order_table.internal_status='Processing' OR order_table.internal_status='Pending')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'procurement','processing'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	public function count_all_pending_orders() {
		$this->db->select("order_table.id");
		$this->db->from('order_table');
		$this->db->join('order_item_table', 'order_item_table.order_id=order_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Processing' OR order_table.internal_status='Pending')");
		$this->db->where_in('order_table.status', array('paid', 'confirmed', 'procurement','processing'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
/* holded orders */
	public function get_all_holded_orders($per_page,$start) {
		$this->db->limit($per_page,$start);
		$this->db->select("order_table.id, order_table.order_id, order_table.store_name, order_table.order_no, order_table.created_date, order_table.status, order_table.last_updated, order_table.amount, order_table.internal_status, order_table.comments, order_table.payment_mode, order_table.customer_id, order_table.customer_name, order_table.email, order_table.email2, order_table.street1, order_table.street2, order_table.city, order_table.state, order_table.pincode");
		$this->db->from('order_table');
		$this->db->where_in('order_table.status', array('holded'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
		
	public function count_all_holded_orders() {
		$this->db->select("order_table.id");
		$this->db->from('order_table');
		$this->db->where_in('order_table.status', array('holded'));
		$this->db->group_by('order_table.id');
		$this->db->order_by('order_table.id', 'desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function search_holded_order($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode) {
		$array = array();
		if($search_store !=null){
			$array['store_name'] = $search_store;
		}
		if($search_order !=null){
			$array['order_no'] = $search_order;
		}
		if($search_sys_status !=null){
			$array['status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['internal_status'] = $search_internal_status;
		}
		if($search_payment_mode !=null){
			$array['payment_mode'] = $search_payment_mode;
		}
		if($search_create_date !=null){
			$array['created_date'] = $search_create_date;
		}
		if($search_name !=null){
			$array['customer_name'] = $search_name;
		}
		$this->db->like ($array);
		$this->db->where_in('order_table.status', array('holded'));
		$this->db->group_by('id');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get('order_table');
		return $query->result();
	}
	
	
/*order_items*/
	public function get_all_order_items($per_page,$start) {
		
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function order_item_status_submit($status, $id) {
		$array = array('status'=>$status);
		$this->db->where('id', $id);
		$this->db->update('order_item_table', $array);
		return $this->db->affected_rows();	
	}
	
	public function order_item_procured_qty_submit($procured_qty, $status, $id) {
		$array = array('procured_qty'=>$procured_qty, 'status'=>$status, 'procured_date'=>Date('d/m/Y',time()));
		$this->db->where('id', $id);
		$this->db->update('order_item_table', $array);
		
		return $this->db->affected_rows();	
	}
	
	public function order_item_procured_date_submit($procured_date, $id) {
		$array = array('procured_date'=>$procured_date);
		$this->db->where('id', $id);
		$this->db->update('order_item_table', $array);
		return $this->db->affected_rows();	
	}
	
	public function search_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status, $per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		$array = array();
		if($search_order_no !=null){
			$array['order_table.order_no'] = $search_order_no;
		}
		if($search_store !=null){
			$array['order_table.store_name'] = $search_store;
		}
		if($search_sys_status !=null){
			$array['order_table.status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['order_table.internal_status'] = $search_internal_status;
		}
		if($search_sku !=null){
			$array['products.sku'] = $search_sku;
		}
		if($search_name !=null){
			$array['order_item_table.name'] = $search_name;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		if($search_status !=null){
			$array['order_item_table.status'] = $search_status;
		}
		if($search_pro_date !=null){
			$array['order_item_table.procured_date'] = $search_pro_date;
		}
		$this->db->like ($array);
		
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function order_item_comment_submit($comment, $id) {
		$array = array('item_comment'=>$comment);
		$this->db->where('id', $id);
		$this->db->update('order_item_table', $array);
		return $this->db->affected_rows();	
	}
	
	
	public function get_procured_item_by_item_id($item_id) {
		$this->db->select("SUM(order_item_table.procured_qty) as sum");
		$this->db->from('order_item_table');
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		$this->db->where('order_item_table.item_id',$item_id);
		$this->db->where_not_in('order_table.status', array('canceled', 'pending', 'holded', 'unconfirmed', 'paymentfail'));
		//$this->db->where_in('order_table.status', array('processing', 'dispatched','paid', 'delivered', 'procurement', 'inprocurement', 'holded', 'confirmed', 'pending'));
		$this->db->where("(order_item_table.procured_qty>0)");
		$query = $this->db->get();
		if($query->num_rows >= 1) {
			$results = $query->result();
			$pro_sum;
			foreach($results as $key) {
				$pro_sum = $key->sum;
			}
			return $pro_sum;
		} else {
			return 0;
		}
	}
	
	public function get_purchase_item_by_item_id($item_id) {
		$this->db->select("SUM(qty) as total_pur_qty");
		$this->db->where('item_id', $item_id);
		$query = $this->db->get('purchase_table');
		if($query->num_rows >= 1) {
			$results = $query->result();
			$pur_sum;
			foreach($results as $key) {
				$pur_sum = $key->total_pur_qty;
			}
			return $pur_sum;
		} else {
			return 0;
		}
	}
	public function get_credit_memo_item_by_item_id($item_id) {
		$this->db->select("SUM(qty) as credit_qty");
		$this->db->where('item_id', $item_id);
		$query = $this->db->get('credit_memo_purchase');
		if($query->num_rows >= 1) {
			$results = $query->result();
			$memo_sum;
			foreach($results as $key) {
				$memo_sum = $key->credit_qty;
			}
			return $memo_sum;
		} else {
			return 0;
		}
	}
	
	public function get_all_partners($payement_mode, $pincode) {
		if($payement_mode == 'cod'){
			$array = array('pincode'=>$pincode, 'type'=>$payement_mode);
		}else{
			$array = array('pincode'=>$pincode);
		}
		$this->db->where($array);
		$query = $this->db->get('pincodes');
		if($query->num_rows >= 1) {
			return $query->result();
		} else {
			return false;
		}
	}
	
	public function get_freight_charges($partners) {
		$where;
		foreach($partners as $partner){
			if(empty($where)){
				$where = "(partner ='". $partner['name'] ."' AND zone = '". $partner['zone'] . "')";
			}else{
				$where .= " OR (partner = '". $partner['name'] ."' AND zone ='". $partner['zone'] . "')";
			}
		}
		$this->db->where($where);
		$query = $this->db->get('freight_charges');
		if($query->num_rows >= 1) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}
	
	public function get_cod_charges($partners) {
		$where;
		foreach($partners as $partner){
			if(empty($where)){
				$where = "partner ='". $partner['name'] ."'";
			}else{
				$where .= " OR partner ='". $partner['name'] ."'";
			}
		}
		$this->db->where($where);
		$query = $this->db->get('cod_charges');
		if($query->num_rows >= 1) {
			$data = $query->result();
			return $data;
		} else {
			return false;
		}
	}
	
	public function get_order_item_weight($order_id) {
		$this->db->select("order_item_table.item_id, order_item_table.procured_qty, purchase_table.weight");
		$this->db->from('order_item_table');
		$this->db->join('purchase_table', 'purchase_table.item_id=order_item_table.item_id', 'inner');
		$this->db->where('order_item_table.order_id', $order_id);
		$this->db->where_in('order_item_table.status', array('Procured', 'Partial Procured'));
		$query = $this->db->get();
		$result = array();
		if($query->num_rows >= 1) {
			$data = $query->result();
			foreach($data as $key){
				$result[$key->item_id] = ($key->procured_qty*$key->weight);
			}
			return array_sum($result);
		} else {
			return false;
		}
	}
	
/* pending order items */
	public function get_all_pending_order_items($per_page,$start) {
		
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.supp_comment,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		$this->db->where_not_in('order_table.status', array('pending', 'holded', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function search_pending_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status, $per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.supp_comment,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		
		$array = array();
		if($search_order_no !=null){
			$array['order_table.order_no'] = $search_order_no;
		}
		if($search_store !=null){
			$array['order_table.store_name'] = $search_store;
		}
		if($search_sys_status !=null){
			$array['order_table.status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['order_table.internal_status'] = $search_internal_status;
		}
		if($search_sku !=null){
			$array['products.sku'] = $search_sku;
		}
		if($search_name !=null){
			$array['order_item_table.name'] = $search_name;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		if($search_status !=null){
			$array['order_item_table.status'] = $search_status;
		}
		if($search_pro_date !=null){
			$array['order_item_table.procured_date'] = $search_pro_date;
		}
		$this->db->like ($array);
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		
		$this->db->where_not_in('order_table.status', array('pending', 'holded', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'asc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_total_pending_item_for_pending_orders() {
		$this->db->select("SUM(order_item_table.procured_qty) as pro_sum, SUM(order_item_table.qty) as total_item_qty");
		$this->db->from('order_item_table');
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where_not_in('order_table.status', array('pending', 'holded', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$query = $this->db->get();
		if($query->num_rows >= 1) {
			$results = $query->result();
			$pro_sum;
			$total_item_qty;
			foreach($results as $key) {
				$pro_sum = $key->pro_sum;
				$total_item_qty = $key->total_item_qty;
			}
			return ($total_item_qty-$pro_sum);
		} else {
			return 0;
		}
	}
	public function count_total_pending_order_items() {
	
		$this->db->select("order_item_table.id");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where_not_in('order_table.status', array('pending', 'holded', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function get_supplier_by_item_brand_id($brand_id) {
		
		$this->db->select('suppliers.name');
		$this->db->from('discount_rule');
		$this->db->join('suppliers', 'suppliers.id=discount_rule.supplier', 'left');
		$this->db->where('brand', $brand_id);
		$this->db->group_by('supplier_priority');
		$this->db->order_by('supplier_priority','asc');
		$query = $this->db->get();
		$results = $query->result();
		if($results) {
			$supplier;
			foreach($results as $name) {
				$supplier[$name->name] = $name->name;
			}
			$supplier['none'] = 'none';
			return $supplier;
		} else {
			return array('none'=>'none');
		}
	}
	
	public function save_supplier_name_order_item_table_by_item_id($id,$supplier,$item_id) {
		
		$data = array('oit.supplier_name'=>$supplier);
		
		$this->db->where('oit.item_id', $item_id);
		$this->db->where('((oit.qty_invoice)+(oit.qty_cancel)) < oit.qty');
		$this->db->where_in('ot.internal_status', array('Processing','Pending'));
		$this->db->where_not_in('ot.status', array('pending', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(oit.procured_qty!=-1)");
		$this->db->update('order_item_table oit join order_table ot on ot.order_id=oit.order_id', $data);
		return $this->db->affected_rows();	
	}
	
	public function save_supplier_name_order_item_table_by_id($id,$supplier) {
		$array = array('supplier_name'=>$supplier);
		$this->db->where('id', $id);
		$this->db->update('order_item_table', $array);
		return $this->db->affected_rows();	
	}
	
	
/* Procured items */
	public function get_all_procured_items($per_page,$start) {
		$this->db->limit($per_page,$start);
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.supp_comment,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) = order_item_table.procured_qty');
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) > 0');
		
		$this->db->where_not_in('order_table.status', array('pending', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(order_item_table.procured_qty > 0)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function count_total_procured_order_items() {
	
		$this->db->select("order_item_table.id");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) = order_item_table.procured_qty');
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) > 0');
		
		$this->db->where_not_in('order_table.status', array('pending', 'unconfirmed', 'paymentfail','canceled'));
		
		$this->db->where("(order_item_table.procured_qty > 0)");
		
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'desc');
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function search_procured_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status) {
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.item_comment,order_item_table.qty_cancel,order_item_table.qty_refund");
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table',	'purchase_table.item_id=order_item_table.item_id', 'left');
		$array = array();
		if($search_order_no !=null){
			$array['order_table.order_no'] = $search_order_no;
		}
		if($search_store !=null){
			$array['order_table.store_name'] = $search_store;
		}
		if($search_sys_status !=null){
			$array['order_table.status'] = $search_sys_status;
		}
		if($search_internal_status !=null){
			$array['order_table.internal_status'] = $search_internal_status;
		}
		if($search_sku !=null){
			$array['products.sku'] = $search_sku;
		}
		if($search_name !=null){
			$array['order_item_table.name'] = $search_name;
		}
		if($search_brand !=null){
			$array['brand.brand_name'] = $search_brand;
		}
		if($search_status !=null){
			$array['order_item_table.status'] = $search_status;
		}
		if($search_pro_date !=null){
			$array['order_item_table.procured_date'] = $search_pro_date;
		}
		$this->db->like ($array);
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) = order_item_table.procured_qty');
		$this->db->where('((order_item_table.qty)-(order_item_table.qty_refund)-(order_item_table.qty_cancel)) > 0');
		
		$this->db->where_not_in('order_table.status', array('pending', 'unconfirmed', 'paymentfail','canceled'));
		
		$this->db->where("(order_item_table.procured_qty > 0)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	/*export_excel_pending_order_items*/
	
	public function export_excel_pending_order_items() {
		
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.supp_comment, suppliers.email");
		
		$this->db->from('order_item_table');
		
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table','purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->join('suppliers', 'suppliers.name=order_item_table.supplier_name', 'left');
		
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		$this->db->where_not_in('order_table.status', array('pending', 'holded','unconfirmed', 'paymentfail','canceled'));
		$this->db->where('((order_item_table.qty)-(order_item_table.procured_qty)) > 0');
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$this->db->group_by('order_item_table.id');
		$this->db->order_by('order_item_table.id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function email_pending_order_items_to_send_by_supplier($e_name) {
		$this->db->select("order_item_table.id, order_item_table.order_id, order_table.order_no, order_table.store_name, order_table.internal_status, order_table.status as system_status, order_item_table.item_id, products.sku, products.product_code as item_code, products.brand_id, brand.brand_name, order_item_table.name, order_item_table.qty, order_item_table.size, order_item_table.mrp, order_item_table.sale_price, order_item_table.status, order_item_table.procured_qty,order_item_table.qty_cancel,order_item_table.qty_refund, order_item_table.procured_date, order_item_table.last_updated, SUM(purchase_table.qty) as total_purchase_qty,order_item_table.supplier_name,order_item_table.supp_comment, suppliers.email");
		$this->db->from('order_item_table');
		$this->db->join('order_table',	'order_table.order_id=order_item_table.order_id', 'left');
		
		$this->db->join('products',	'products.id=order_item_table.item_id', 'left');
		$this->db->join('brand', 'products.brand_id=brand.brand_id', 'left');
		
		$this->db->join('purchase_table','purchase_table.item_id=order_item_table.item_id', 'left');
		
		$this->db->join('suppliers', 'suppliers.name=order_item_table.supplier_name', 'left');
		if(isset($e_name)){
			$this->db->where('order_item_table.supplier_name',$e_name);
		}else{
			$this->db->where("(order_item_table.supplier_name ='none' or order_item_table.supplier_name='' or order_item_table.supplier_name='0') ");
		}
		$this->db->where('((order_item_table.qty_invoice)+(order_item_table.qty_cancel)) < order_item_table.qty');
		$this->db->where("(order_table.internal_status='Processing'  OR order_table.internal_status='Pending')");
		$this->db->where_not_in('order_table.status', array('pending','holded', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where('((order_item_table.qty)-(order_item_table.procured_qty)-order_item_table.qty_refund-order_item_table.qty_cancel) > 0');
		$this->db->where("(order_item_table.procured_qty!=-1)");
		$this->db->group_by('order_item_table.id');
		
		$query = $this->db->get();
		
		return $query->result();
	}
	
	public function update_supplier_name_order_item_comment($id,$supplier_name,$supp_comment) {
		$data = array('oit.supp_comment'=>$supp_comment.'/'.$supplier_name);
		$this->db->where('oit.id',$id);
		$this->db->where('((oit.qty_invoice)+(oit.qty_cancel)) < oit.qty');
		$this->db->where_in('ot.internal_status', array('Processing','Pending'));
		$this->db->where_not_in('ot.status', array('pending', 'unconfirmed', 'paymentfail','canceled'));
		$this->db->where("(oit.procured_qty!=-1)");
		$this->db->update('order_item_table oit join order_table ot on ot.order_id=oit.order_id', $data);
		return $this->db->affected_rows();	
	}
	
}