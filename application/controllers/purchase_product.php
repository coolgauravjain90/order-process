<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_Product extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$config = array();
		$config['base_url'] = base_url().'purchase_product/index';
		$config['total_rows'] = $this->db->count_all('purchase_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['products'] = $this->order_m->purchase_product_table($config['per_page'],$offset);
		$this->load->view('purchase_view.php', $data);
	}
	
	public function get_invoice_detail($invoice_id){
		$data = $this->order_m->get_supplier_invoice_detail($invoice_id);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}	
	}
	
	public function search_purchase() {
		
		$config = array();
		$config['base_url'] = base_url().'purchase_product/search_purchase';
		$config['total_rows'] = $this->db->count_all('purchase_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_sku = $this->input->post('search_sku');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		$search_invoice_id = $this->input->post('search_invoice_id');
		$search_date = $this->input->post('search_date');
		$data['products'] = $this->order_m->search_purchase_product($search_sku,$search_name,$search_brand,$search_invoice_id,$search_date,$config['per_page'],$offset);
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
}