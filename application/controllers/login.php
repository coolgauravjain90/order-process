<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
	public function __construct() {
            parent::__construct();
            $this->load->model('login_m');
    }
	
	public function index() {
			$data['msg'] = 'Please Login';
			$this->load->view('login_view.php', $data);
	}
	
	//For validate the username and password 
	public function validate() {
	
	if($this->session->userdata('logged_in')){
				redirect('orders.php');
	} else {
		$this->form_validation->set_rules('email','email','required|trim|valid_email');
		$this->form_validation->set_rules('password','password','required|trim|min_length[6]');
		
		if($this->form_validation->run()) {
			
			$data['email'] = $email = $this->input->post('email');
			if($this->login_m->is_user_exist($email)) {
				if($this->login_m->is_account_active($email)){
					
						$dbpass = $this->login_m->get_password_by_email($email);
						$password = md5($this->input->post('password'));
						if($dbpass==$password) {
							$newdata = array(
								'email'    => $email,
								'password'    => $password,
								'logged_in' 	=> TRUE
							);
							$this->session->set_userdata($newdata);
							$this->login_m->update_lastlogin_date($email);
							redirect('orders.php', 'refresh');
						} else {
							$data['msg']=$this->lang->line('um_login_invalid_pass');
							$this->load->view('login_view',$data);	
						}
					
				}
			} else {
				$data['msg']=$this->lang->line('um_login_user_not_found');
				$this->load->view('login_view',$data);	
			}
		} else {
			$data['msg']=$this->lang->line('um_login_msg');
			$this->load->view('login_view',$data);
		}
	}
	}
	
	//for logout the users
	public function logout() {
		
		$newdata = array(
		'email'     => '',
		'password'     => '',
		'logged_in' => FALSE
		);
		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		redirect('login.php', 'Logout Successfully');
	}
}