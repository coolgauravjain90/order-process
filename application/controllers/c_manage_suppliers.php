<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Manage_Suppliers extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('manage_suppliers_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
/* manage Discount Rules */
	public function manage_discount_rules() {
		$config = array();
		$config['base_url'] = base_url().'c_manage_suppliers/manage_discount_rules';
		$config['total_rows'] = $this->db->count_all('discount_rule');
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$data['rules'] = $this->manage_suppliers_m->get_all_discount_rules($config['per_page'],$offset);
		$this->load->view('manage_suppliers/manage_discount_rule.php', $data);
	}
	
	public function search_discount_rule() {
		$search_rule_name = $this->input->post('search_rule_name');
		$search_supplier = $this->input->post('search_supplier');
		$search_brand = $this->input->post('search_brand');
		$search_status = $this->input->post('search_status');
		
		$data = $this->manage_suppliers_m->search_discount_rules($search_rule_name,$search_supplier,$search_brand,$search_status);

		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function submit_discount_discount_rule($id) {
		$discount = $this->input->post('text_discount_val');
		$data = $this->manage_suppliers_m->submit_discounts_discount_rule($id, $discount);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_rule_priority_discount_rule($id) {
		$priority = $this->input->post('text_rp_val');
		$data = $this->manage_suppliers_m->submit_rules_priority_discount_rule($id, $priority);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_supplier_priority_discount_rule($id) {
		$priority = $this->input->post('text_sp_val');
		$data = $this->manage_suppliers_m->submit_suppliers_priority_discount_rule($id, $priority);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_status_discount_rule($id) {
		$status = $this->input->post('text_st_val');
		$data = $this->manage_suppliers_m->submit_status_discounts_rule($id, $status);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
/* Manage Suppliers */
	
	public function manage_suppliers() {
		$config = array();
		$config['base_url'] = base_url().'c_manage_suppliers/manage_suppliers';
		$config['total_rows'] = $this->db->count_all('suppliers');
		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$data['suppliers'] = $this->manage_suppliers_m->get_all_suppliers($config['per_page'],$offset);
		$this->load->view('manage_suppliers/manage_suppliers.php', $data);
	}
	
	public function submit_about_suppliers($id) {
		$about = $this->input->post('text_about_val');
		$data = $this->manage_suppliers_m->submit_abouts_suppliers($id, $about);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_pocontact_suppliers($id) {
		$po = $this->input->post('text_po_val');
		$data = $this->manage_suppliers_m->submit_pocontact_suppliers($id, $po);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_mobile_suppliers($id) {
		$mobile = $this->input->post('text_mob_val');
		$data = $this->manage_suppliers_m->submit_mobile_suppliers($id, $mobile);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_phone_suppliers($id) {
		$phone = $this->input->post('text_ph_val');
		$data = $this->manage_suppliers_m->submit_phone_supplier($id, $phone);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_email_suppliers($id) {
		$email = $this->input->post('text_email_val');
		$data = $this->manage_suppliers_m->submit_email_supplier($id, $email);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_email2_suppliers($id) {
		$email = $this->input->post('text_sec_email_val');
		$data = $this->manage_suppliers_m->submit_email2_supplier($id, $email);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_street_suppliers($id) {
		$street = $this->input->post('text_strt_val');
		$data = $this->manage_suppliers_m->submit_street_supplier($id, $street);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_city_suppliers($id) {
		$city = $this->input->post('text_city_val');
		$data = $this->manage_suppliers_m->submit_city_supplier($id, $city);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_state_suppliers($id) {
		$state = $this->input->post('text_state_val');
		$data = $this->manage_suppliers_m->submit_state_supplier($id, $state);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_pincode_suppliers($id) {
		$pin = $this->input->post('text_pincode_val');
		$data = $this->manage_suppliers_m->submit_pincode_supplier($id, $pin);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_cst_suppliers($id) {
		$cst = $this->input->post('text_cst_val');
		$data = $this->manage_suppliers_m->submit_cst_supplier($id, $cst);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_vat_suppliers($id) {
		$vat = $this->input->post('text_vat_val');
		$data = $this->manage_suppliers_m->submit_vat_supplier($id, $vat);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function submit_status_suppliers($id) {
		$status = $this->input->post('text_sta_val');
		$data = $this->manage_suppliers_m->submit_status_supplier($id, $status);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function add_new_supplier() {
		$name = $this->input->post('supplier_name');
		$contact_person = $this->input->post('supplier_pocontact');
		$address = $this->input->post('supplier_adress');
		$city = $this->input->post('supplier_city');
		$state = $this->input->post('supplier_state');
		$pincode = $this->input->post('supplier_pincode');
		$mobile = $this->input->post('supplier_mobile');
		$phone = $this->input->post('supplier_ph_no');
		$email = $this->input->post('supplier_email');
		$email2 = $this->input->post('supplier_sec_email');
		$status = $this->input->post('supplier_status');
		
		$data = $this->manage_suppliers_m->add_new_supplier($name, $contact_person, $address, $city, $state, $pincode, $mobile, $phone, $email, $email2, $status);
		if($data==1) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function remove_suppliers($id) {
		$data = $this->manage_suppliers_m->remove_supplier($id);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
}