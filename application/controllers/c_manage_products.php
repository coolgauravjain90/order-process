<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Manage_Products extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('manage_products_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }

/* Manage Products View */
		
	public function manage_products() {
	
		$config = array();
		$config['base_url'] = base_url().'c_manage_products/manage_products';
		$config['total_rows'] = $this->db->count_all('products');
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$data['products'] = $this->manage_products_m->get_all_products($config['per_page'],$offset);
		$this->load->view('manage_products/manage_products.php', $data);
	}
	
	public function search_manage_products() {
		
		$search_sku = $this->input->post('search_sku');
		$search_bar_code = $this->input->post('search_bar_code');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		
		$data = $this->manage_products_m->search_all_products($search_sku, $search_bar_code, $search_name, $search_brand);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}

/* Manage Brands view */
	public function manage_brands() {
		$config = array();
		$config['base_url'] = base_url().'c_manage_products/manage_brands';
		$config['total_rows'] = $this->db->count_all('brand');
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$data['brands'] = $this->manage_products_m->get_all_brands($config['per_page'],$offset);
		$this->load->view('manage_products/manage_brands.php', $data);
	}
	
	public function search_manage_brands() {
		
		$search_brand = $this->input->post('search_brand');
		
		$data = $this->manage_products_m->search_all_brands($search_brand);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
/* Manage Inventory */
	public function manage_all_inventory() {
		$config = array();
		$config['base_url'] = base_url().'c_manage_products/manage_all_inventory';
		$config['total_rows'] = $this->db->count_all('purchase_table');
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$inventories = $this->manage_products_m->get_all_inventory($config['per_page'],$offset);
		
		$procured_item_qty  = array();
		foreach($inventories as $inventory) {
			$procured_item_qty[$inventory->item_id] = $this->manage_products_m->get_procured_item_qty($inventory->item_id);
		}
		$data['procured_qty'] = $procured_item_qty;
		$data['inventories'] = $inventories;
		$this->load->view('manage_products/manage_inventory.php', $data);
	}
	
	public function search_manage_inventory() {
		$search_sku = $this->input->post('search_sku');
		$search_bar_code = $this->input->post('search_bar_code');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		
		$inventories = $this->manage_products_m->search_all_inventory($search_sku, $search_bar_code, $search_name, $search_brand);
		
		$procured_item_qty  = array();
		foreach($inventories as $inventory) {
			$procured_item_qty[$inventory->item_id] = $this->manage_products_m->get_procured_item_qty($inventory->item_id);
		}
		$data['procured_qty'] = $procured_item_qty;
		$data['inventories'] = $inventories;
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
}