<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Shipment extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('m_shipment');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		
	}
	
	public function get_pending_shipment() {
	
		$config = array();
		$config['base_url'] = base_url().'c_shipment/get_pending_shipment';
		$config['total_rows'] = $this->m_shipment->count_all_pending_shipments();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['shipments'] = $this->m_shipment->get_all_pending_shipments($config['per_page'],$offset);
		
		$data['pending_shipment'] = $this->m_shipment->count_all_pending_shipments();
		$this->load->view('shipment/pending_shipments.php', $data);
	}
	
	public function shipment_status_submit($id) {
		$shipment_status = $this->input->post('shipment_status');
		$data = $this->m_shipment->submit_shipment_status($id, $shipment_status);
		if($data == 1) {
			echo true;
		} else {
			echo false;
		}	
	}
	
	public function current_status_submit($id) {
		$current_status = $this->input->post('text_current_status_val');
		$data = $this->m_shipment->submit_current_status($id, $current_status);
		if($data == 1) {
			echo true;
		} else {
			echo false;
		}	
	}
	
	public function comment_submit($id) {
		$comment = $this->input->post('text_comment_val');
		$data = $this->m_shipment->submit_comments($id, $comment);
		if($data == 1) {
			echo true;
		} else {
			echo false;
		}	
	}
	
	public function search_shipments() {
		$config = array();
		$config['base_url'] = base_url().'c_shipment/search_shipments';
		$config['total_rows'] = $this->m_shipment->count_all_pending_shipments();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_order_id = $this->input->post('search_order_id');
		$search_ship_by = $this->input->post('search_ship_by');
		$search_order_status = $this->input->post('search_order_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_shipment_status = $this->input->post('search_shipment_status');
		
		$shipments = $this->m_shipment->search_pending_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status,$config['per_page'],$offset);
		
		$options1 = array(
			'delivered'  => 'delivered',
			'dispatched'    => 'dispatched',
			'processing'   => 'processing',
			'return' => 'return',
			'returned' => 'returned',
			'stuck' => 'stuck',
			);
		$status = array();
		foreach($shipments as $shipment) {
			$status[$shipment->id] = form_dropdown('shipment_status', $options1, $shipment->shipment_status, 'class=shipment_status id=shipment_status_'.$shipment->id);
		}
		$data['status'] = $status;
		$data['shipments'] = $shipments;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
/* Delivered shipment */
	
	public function get_delivered_shipment() {
	
		$config = array();
		$config['base_url'] = base_url().'c_shipment/get_delivered_shipment';
		$config['total_rows'] = $this->m_shipment->count_all_delivered_shipments();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['shipments'] = $this->m_shipment->get_all_delivered_shipments($config['per_page'],$offset);
		
		$data['delivered_shipment'] = $this->m_shipment->count_all_delivered_shipments();
		$this->load->view('shipment/delivered_shipments.php', $data);
	}
	
	public function search_delivered_shipments() {
		
		$search_order_id = $this->input->post('search_order_id');
		$search_ship_by = $this->input->post('search_ship_by');
		$search_order_status = $this->input->post('search_order_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_shipment_status = $this->input->post('search_shipment_status');
		
		$shipments = $this->m_shipment->search_delivered_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status);
		
		$options1 = array(
			'delivered'  => 'delivered',
			'dispatched'    => 'dispatched',
			'processing'   => 'processing',
			'return' => 'return',
			'returned' => 'returned',
			'stuck' => 'stuck',
			);
		$status = array();
		foreach($shipments as $shipment) {
			$status[$shipment->id] = form_dropdown('shipment_status', $options1, $shipment->shipment_status, 'class=shipment_status id=shipment_status_'.$shipment->id);
		}
		$data['status'] = $status;
		$data['shipments'] = $shipments;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
/* Returned shipment */
	
	public function get_returned_shipment() {
	
		$config = array();
		$config['base_url'] = base_url().'c_shipment/get_returned_shipment';
		$config['total_rows'] = $this->m_shipment->count_all_returned_shipments();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['shipments'] = $this->m_shipment->get_all_returned_shipments($config['per_page'],$offset);
		
		$data['returned_shipment'] = $this->m_shipment->count_all_returned_shipments();
		$this->load->view('shipment/returned_shipments.php', $data);
	}
	
	public function search_returned_shipments() {
		
		$search_order_id = $this->input->post('search_order_id');
		$search_ship_by = $this->input->post('search_ship_by');
		$search_order_status = $this->input->post('search_order_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_shipment_status = $this->input->post('search_shipment_status');
		
		$shipments = $this->m_shipment->search_returned_shipment($search_order_id, $search_ship_by, $search_order_status, $search_payment_mode, $search_shipment_status);
		
		$options1 = array(
			'delivered'  => 'delivered',
			'dispatched'    => 'dispatched',
			'processing'   => 'processing',
			'return' => 'return',
			'returned' => 'returned',
			'stuck' => 'stuck',
			);
		$status = array();
		foreach($shipments as $shipment) {
			$status[$shipment->id] = form_dropdown('shipment_status', $options1, $shipment->shipment_status, 'class=shipment_status id=shipment_status_'.$shipment->id);
		}
		$data['status'] = $status;
		$data['shipments'] = $shipments;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	
	
}