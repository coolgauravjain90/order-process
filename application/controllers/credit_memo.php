<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Credit_Memo extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$data['dropdown'] = $this->order_m->get_supplier_id_dropdown();
		$this->load->view('invoice_credit_memo/memo_purchase_product.php', $data);
	}
	
	public function validate_supplier_invoice() {
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('invoice_no','Invoice No.', 'required|min_lenght[3]');
		$this->form_validation->set_rules('invoice_date','Invoice Date', 'required|min_lenght[3]');
		if ($this->form_validation->run()){
			$supplier = $this->input->post('supplier');
			$invoice_no = $this->input->post('invoice_no');
			$invoice_date = $this->input->post('invoice_date');
			
			$invoice_id = $this->order_m->check_search_supplier_invoice_id($supplier, $invoice_no, $invoice_date);
			if($invoice_id) {
				echo json_encode($invoice_id);
			}
		} else {
			echo false;
		}
	}
	
	public function get_invoiced_purchase_product($invoice_id) {
		$data = $this->order_m->get_purchase_product_memo_invoice_id($invoice_id);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function create_credit_memo_invoice($invoice_id) {
		$this->form_validation->set_rules('credit_date','Credit Date', 'required');
		if ($this->form_validation->run()){
			$credit_date = $this->input->post('credit_date');
			$memo_id = $this->order_m->create_credit_memo_id_invoice($invoice_id, $credit_date);
			if($memo_id) {
				echo json_encode($memo_id);
			}
		} else {
			echo false;
		}
	}
	
	public function create_credit_memo_product() {
		$this->form_validation->set_rules('item_id','Item Id', 'required');
		$this->form_validation->set_rules('credit_memo_id','Credit Memo Id', 'required');
		$this->form_validation->set_rules('qty','Qty', 'required');
		if ($this->form_validation->run()){
			$item_id = $this->input->post('item_id');
			$credit_memo_id = $this->input->post('credit_memo_id');
			$qty = $this->input->post('qty');
			
			$data = $this->order_m->create_credit_memo_id_product($item_id, $credit_memo_id, $qty);
			if($data==1) {
					echo true;
			}
		} else {
				echo false;
		}
	}
	
	public function view_credit_memo_invoice_items() {
		$config = array();
		$config['base_url'] = base_url().'credit_memo/view_credit_memo_invoice_items';
		$config['total_rows'] = $this->db->count_all('credit_memo_invoice');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 5;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$data['memos'] = $this->order_m->view_credit_memo_invoice_item($config['per_page'],$offset);
		$this->load->view('invoice_credit_memo/view_credit_memo.php', $data);
	}
	
	public function search_credit_memo_items() {
		$search_supplier = $this->input->post('search_supplier');
		$search_inovice_no = $this->input->post('search_inovice_no');
		$search_credit_date = $this->input->post('search_credit_date');
		$search_sku = $this->input->post('search_sku');
		$search_brand = $this->input->post('search_brand');
		
		$data = $this->order_m->search_credit_memo_item($search_supplier, $search_inovice_no, $search_credit_date, $search_sku, $search_brand);
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
}
	