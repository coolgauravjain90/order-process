<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edit_invoice extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$data['dropdown'] = $this->order_m->get_supplier_id_dropdown();
		$this->load->view('invoice_edit.php', $data);
	}
	
	public function validate_supplier_invoice() {
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('invoice_no','Invoice No.', 'required|min_lenght[3]');
		$this->form_validation->set_rules('invoice_date','Invoice Date', 'required|min_lenght[3]');
		if ($this->form_validation->run()){
			$supplier = $this->input->post('supplier');
			$invoice_no = $this->input->post('invoice_no');
			$invoice_date = $this->input->post('invoice_date');
			
			$data = $this->order_m->check_search_supplier_invoice_id($supplier, $invoice_no, $invoice_date);
			if($data) {
				$value = $this->order_m->get_purchase_products_invoice_value($supplier, $invoice_no, $invoice_date);
				echo json_encode($value);
			}
		} else {
			echo false;
		}
	}
	
	public function search_product_invoiced() {
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('invoice_date','Invoice Date', 'required|date');
		$this->form_validation->set_rules('invoice_no','Invoice No.', 'required|min_lenght[3]');
		
		if ($this->form_validation->run()){
			$supplier = $this->input->post('supplier');
			$invoice_date = $this->input->post('invoice_date');
			$invoice_no = $this->input->post('invoice_no');
			
			$data = $this->order_m->search_purchse_products($invoice_date, $supplier, $invoice_no);
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function get_purchase_products() {

		$this->form_validation->set_rules('id','ID', 'required');
		if ($this->form_validation->run()){
			$id = $this->input->post('id');
			$data = $this->order_m->get_purchase_products_by_id_on_check($id);
			echo json_encode($data);
		} else {
			echo false;
		}	
	}
	
	public function update_invoice_entry() {
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('invoice_date','Invoice Date', 'required|date');
		$this->form_validation->set_rules('invoice_no','Invoice No.', 'required|min_lenght[3]');
		$this->form_validation->set_rules('update_date','Update date');
		
		if ($this->form_validation->run()){
			$invoice = array(
				'supplier' => $this->input->post('supplier'),
				'invoice_date' => $this->input->post('invoice_date'),
				'update_date' => $this->input->post('update_date'),
				'invoice_no' => $this->input->post('invoice_no')
				);
			$this->session->set_userdata($invoice);
			$data = $this->order_m->update_invoice();
			$array_items = array('supplier'=>'', 'invoice_date'=>'', 'invoice_no'=>'', 'update_date'=>'');
			$this->session->unset_userdata($array_items);
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	
	public function update_purchase_product() {
		$this->form_validation->set_rules('id','ID', 'required');
		$this->form_validation->set_rules('invoice_id','Inovice No.', 'required|min_lenght[2]');
		$this->form_validation->set_rules('after_tax','After Tax','required');
		$this->form_validation->set_rules('qty','QTY', 'required|numeric');
		if ($this->form_validation->run()){
			$id = $this->input->post('id');
			$invoice_id = $this->input->post('invoice_id');
			$after_tax = $this->input->post('after_tax');
			$qty = $this->input->post('qty');
			
			$this->get_purchase_product_id_clone($id, $qty);
			
			$data = $this->order_m->update_purchase_stock($id, $invoice_id, $after_tax, $qty);
			if($data) {
				echo json_encode($data);
			}
		} else {
			echo false;
		}
	}
	
	public function get_purchase_product_id_clone($id, $qty) {
		$products = $this->order_m->get_purchase_products_by_id($id);
		foreach($products as $product) {
			$product_info = array(
						'sku' => $product->sku,
						'item_id' => $product->item_id,
						'date' => $product->date,
						'exp_date' => $product->exp_date,
						'weight' => $product->weight,
						'qty' => ($product->qty - $qty)
						);
			if(($product->qty - $qty) <= 0) {
				return false;
			} else {
				$this->session->set_userdata($product_info);
				$this->order_m->create_product_clone();
				$array_items = array('sku'=>'', 'item_id'=>'', 'date'=>'', 'exp_date'=>'', 'weight'=>'', 'qty'=>'');
				$this->session->unset_userdata($array_items);
			}
		} 
	}
	
	
	
}