<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_Invoice_Entry extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$data['dropdown'] = $this->order_m->get_supplier_id_dropdown();
		$this->load->view('purchase/purchaseform.php', $data);
	}
	
	public function validate_supplier_invoice() {
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('invoice_no','Invoice No.', 'required|min_lenght[3]');
		if ($this->form_validation->run()){
			$supplier = $this->input->post('supplier');
			$invoice_no = $this->input->post('invoice_no');
			
			$data = $this->order_m->check_supplier_invoice_id($supplier, $invoice_no);
			if($data) {
				echo json_encode($data);
			}
		} else {
			echo false;
		}
	}
	
	public function purchaseform_one() {
		if($this->input->post()){
			$data = $this->input->post();
			$supplier = $data['invoice']['supplier'];
			$invoice_no = $data['invoice']['invoice_no'];
			$invoice_date = $data['invoice']['invoice_date'];
			$create_date = $data['invoice']['create_date'];
			$correction_value = $data['invoice']['correction_value'];
			$items = $data['invoice']['items'];
			$insert_id = $this->order_m->add_invoice_to_system($supplier, $invoice_no, $invoice_date, $create_date, $correction_value);
			if($insert_id > 0) {
				foreach($items as $item){
					$item_id = $item['item_id'];
					$invoice_id = $insert_id;
					$sku = $item['sku'];
					$bar_code = $item['bar_code'];
					$weight = $item['weight'];
					$price = $item['price'];
					$purchase_date = $item['purchase_date'];
					$exp_date = $item['exp_date'];
					$qty = $item['qty'];
					$item_mrp = $item['mrp'];
					
					$this->update_weight_mrp_if_change($item_id, $weight,$item_mrp, $bar_code);
	
					$result = $this->order_m->add_product_to_inventory_to_system($item_id, $invoice_id, $sku, $weight, $price, $purchase_date, $exp_date, $qty);
					if($result > 0) {
						echo true;
					}
				}
			} else {
				echo false;
			}
		}
		echo false;
	}
	
	public function get_product_info_sku($sku) {
		$supplier = $this->input->post('supplier');
		$data['discount'] = $this->order_m->get_discount_discount_rule_sku($sku, $supplier);
		$data['pro'] = $this->order_m->get_product_detail_by_sku($sku);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function get_product_info_bar_code() {
		$supplier = $this->input->post('supplier');
		$bar_code = $this->input->post('bar_code');
		
		$data['discount'] = $this->order_m->get_discount_discount_rule_bar_code($bar_code, $supplier);
		$data['pro'] = $this->order_m->get_product_detail_by_bar_code($bar_code);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function purchaseform_two() {
		$this->form_validation->set_rules('item_id', 'Item Id', 'required');
		$this->form_validation->set_rules('sku', 'SKU', 'required');
		$this->form_validation->set_rules('bar_code', 'Bar Code', 'required');
		$this->form_validation->set_rules('weight', 'Weight', 'required');
		$this->form_validation->set_rules('price', 'Price', 'required');
		$this->form_validation->set_rules('purchase_date', 'Purchase Date', 'required');
		$this->form_validation->set_rules('exp_date', 'Exp. Date', 'required');
		$this->form_validation->set_rules('qty', 'Qty', 'required');
		$this->form_validation->set_rules('invoice_id', 'invoice_id', 'required');
		if($this->form_validation->run()) {
			$productinfo = array(
					'item_id' => $this->input->post('item_id'),
					'invoice_id' => $this->input->post('invoice_id'),
					'sku' => $this->input->post('sku'),
					'bar_code' => $this->input->post('bar_code'),
					'weight' => $this->input->post('weight'),
					'price' => $this->input->post('price'),
					'purchase_date' => $this->input->post('purchase_date'),
					'exp_date' => $this->input->post('exp_date'),
					'qty' => $this->input->post('qty'),
					'mrp' => $this->input->post('mrp')
				);
			$this->session->set_userdata($productinfo);
			
			$this->update_weight_mrp_if_change($productinfo['item_id'], $productinfo['weight'],$productinfo['mrp'], $productinfo['bar_code']);
	
			$data = $this->order_m->add_product_to_inventory();
			$array_items = array('item_id'=>'', 'invoice_id'=>'', 'sku'=>'', 'bar_code'=>'', 'weight'=>'', 'price'=>'', 'purchase_date'=>'', 'exp_date'=>'', 'qty'=>'', 'mrp'=>'');
			$this->session->unset_userdata($array_items);
			if($data>=1) {
				echo true;
			}
		} else {
			echo false;
		}
	}
	
	public function update_weight_mrp_if_change($item_id, $weight, $mrp, $bar_code) {
		$products = $this->order_m->get_product_info_by_item_id($item_id);
		
		foreach($products as $product) {
			if($product->weight != $weight) {
				$this->order_m->update_weight_product_table($item_id, $weight);
			}
			if($product->mrp != $mrp) {
				$this->order_m->update_mrp_product_table($item_id, $mrp);
			}
			if($product->product_code != $bar_code) {
				$this->order_m->update_bar_code_product_table($item_id, $bar_code);
			}
			
		}
	}
	
}