<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_Detail extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$config = array();
		$config['base_url'] = base_url().'invoice_detail/index';
		$config['total_rows'] = $this->db->count_all('invoice_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 4;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['invoices'] = $this->order_m->invoice_details($config['per_page'],$offset);
		$this->load->view('allinvoices.php', $data);
	}
	
	public function search_invoice() {
		$config = array();
		$config['base_url'] = base_url().'invoice_detail/search_invoice';
		$config['total_rows'] = $this->db->count_all('invoice_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 4;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_invoice_id = $this->input->post('search_invoice_id');
		$search_supplier = $this->input->post('search_supplier');
		$search_invoice_no = $this->input->post('search_invoice_no');
		$search_invoice_date = $this->input->post('search_invoice_date');
		
		$data['invoices'] = $this->order_m->search_invoices($search_invoice_id, $search_supplier, $search_invoice_no, $search_invoice_date, $config['per_page'],$offset);
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function comment_submit($id) {
		$comment = $this->input->post('text_comment_val');
		$data = $this->order_m->invoice_comment_submit($comment, $id);
		if($data==1) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function status_submit($id) {
		$status = $this->input->post('text_status_val');
		$data = $this->order_m->invoice_status_submit($status, $id);
		if($data==1) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function get_purchase_product_invoice($invoice_id) {
	
		$data = $this->order_m->get_purchase_product_invoice_id($invoice_id);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function change_invoice_no($id) {
		$invoice_no = $this->input->post('text_invoice_no');
		$data = $this->order_m->change_invoice_no_for_supplier($id,$invoice_no);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
}