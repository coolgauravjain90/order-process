<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Discount_Rule_Table extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_m');
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		$data['supplier'] = $this->order_m->get_supplier_id_dropdown();
		$data['brand'] = $this->order_m->get_brand_dropdown();
		$this->load->view('discount_rule.php', $data);
	}
	
	public function create_discount_rule() {
		$this->form_validation->set_rules('name','Name', 'required');
		$this->form_validation->set_rules('supplier','Supplier', 'required');
		$this->form_validation->set_rules('brand','Brand', 'required');
		$this->form_validation->set_rules('brand_segment','Brand Segment');
		$this->form_validation->set_rules('discount','Discount', 'required');
		$this->form_validation->set_rules('status','Status', 'required');
		$this->form_validation->set_rules('rule_priority','Rule Priority', 'required|numeric');
		$this->form_validation->set_rules('supplier_priority','Supplier Priority', 'required|numeric');
		
		if ($this->form_validation->run()){
			$discountinfo = array(
					'name' => $this->input->post('name'),
					'supplier' => $this->input->post('supplier'),
					'brand' => $this->input->post('brand'),
					'brand_segment' => $this->input->post('brand_segment'),
					'discount' => $this->input->post('discount'),
					'status' => $this->input->post('status'),
					'rule_priority' => $this->input->post('rule_priority'),
					'supplier_priority' => $this->input->post('supplier_priority'),
					);
			$this->session->set_userdata($discountinfo);
			$this->order_m->insert_discount_rule();
			$array_items = array('name'=>'', 'supplier'=>'', 'brand'=>'', 'brand_segment'=>'', 'discount'=>'', 'status'=>'', 'rule_priority'=>'', 'supplier_priority'=>'');
			$this->session->unset_userdata($array_items);
			redirect('discount_rule.php');
		}
	}
	
}