<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class C_Order_Process extends CI_Controller {
	public function __construct() {
            parent::__construct();
            $this->load->model('order_process_m');
			//$this->output->enable_profiler(TRUE);
			$this->load->library( 'email' );
			$this->load->database();
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
    }
	
	public function index() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/index/';
		$config['total_rows'] = $this->db->count_all('order_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config['num_links'] = 4;
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];
		
		$data['links'] = $this->pagination->create_links();
		
		$orders = $this->order_process_m->get_all_orders($config['per_page'],$offset);
		$partner_name = array();
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
		}
		
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		$this->load->view('order_process/orders.php', $data);
	}
	
	public function change_order_internal_status($order_id) {
		$data = $this->order_process_m->get_all_order_item_by_order_id($order_id);
		if($data) {
			echo json_encode($data);
		} else {
			false;
		}
	}
	
	public function get_order_item_by_order_id($id) {
		$order_id = $this->input->post('order_id');
		$data = $this->order_process_m->get_order_item_by_order_id($order_id);
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function search_orders() {
	
		$config = array();
		$config['base_url'] = base_url().'c_order_process/search_orders';
		$config['total_rows'] = $this->db->count_all('order_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_store = $this->input->post('search_store');
		$search_order = $this->input->post('search_order');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_create_date = $this->input->post('search_create_date');
		$search_name = $this->input->post('search_name');
		
		$orders = $this->order_process_m->search_orders($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode, $config['per_page'],$offset);
		
		$partner_name = array();
		$popup = array();
		$int_drop = array();
		$options = array(
			'Ready to Ship' => 'Ready to Ship',
			'Partial Ready to Ship' => 'Partial Ready to Ship',
			'Cancelled' => 'Cancelled',
			'dispatched'    => 'dispatched',
			'Pending'=>'Pending'
		);
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
			$popup[$order->id] = anchor('',$order->order_id,array('class'=>'topopup', 'id'=>'topopup_'.$order->id));
			
			$int_drop[$order->id] = form_dropdown('int_st_store_name',$options, $order->internal_status,'class=int_st_store_name id=int_st_store_'.$order->id);
		}
		$data['internal'] = $int_drop;
		$data['popup'] = $popup;
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function comment_submit($id) {
		$comment = $this->input->post('text_comment_val');
		$data = $this->order_process_m->order_comment_submit($comment, $id);
		if($data==1) {
			echo true;
		} else {
			echo false;
		}
	}

/*Ready to ship*/	

	public function export_ready_to_ship_orders() {
		
		$orders = $this->order_process_m->export_all_ready_to_ship_orders();
		$partner_name = array();
		
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		$this->load->view('order_process/export_ready_to_ship.php', $data);
	}

	public function get_ready_to_ship_orders() {
		$config = array();
		$config['base_url'] = base_url().'c_order_process/get_ready_to_ship_orders';
		$config['total_rows'] = $this->order_process_m->count_all_ready_to_ship_orders();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$orders = $this->order_process_m->get_all_ready_to_ship_orders($config['per_page'],$offset);
		$partner_name = array();
		
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
		}
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		$data['ready_orders'] = $this->order_process_m->count_all_ready_to_ship_orders();
		$this->load->view('order_process/ready_to_ship.php', $data);
	}
	
	public function search_ready_to_ship_orders() {
	
		$config = array();
		$config['base_url'] = base_url().'c_order_process/search_ready_to_ship_orders';
		$config['total_rows'] = $this->order_process_m->count_all_ready_to_ship_orders();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_store = $this->input->post('search_store');
		$search_order = $this->input->post('search_order');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_create_date = $this->input->post('search_create_date');
		$search_name = $this->input->post('search_name');
		
		$orders = $this->order_process_m->search_ready_to_ship_order($search_store,$search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode, $config['per_page'],$offset);
		
		$partner_name = array();
		$popup = array();
		$int_drop = array();
		$options = array(
			'Ready to Ship' => 'Ready to Ship',
			'Partial Ready to Ship' => 'Partial Ready to Ship',
			'Cancelled' => 'Cancelled',
			'dispatched'    => 'dispatched'
		);
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
			
			$popup[$order->id] = anchor('',$order->order_id,array('class'=>'topopup', 'id'=>'topopup_'.$order->id));
			
			$int_drop[$order->id] = form_dropdown('int_st_store_name',$options, $order->internal_status,'class=int_st_store_name id=int_st_store_'.$order->id);
		}
		$data['internal'] = $int_drop;
		$data['popup'] = $popup;
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function change_internal_status_by_ship($id) {
		$internal_status = $this->input->post('int_st_store_name');
		$order_id = $this->input->post('order_id');
		$data = $this->order_process_m->submit_internal_status_by_ship($id, $internal_status,$order_id);
		if($data>=1) {
			echo true;
		} else {
			echo false;
		}
	}
	
	public function change_internal_status_bulk() {
		$internal_status = $this->input->post('internal_store_status');
		$order_id = $this->input->post('order_id');
		$id = $this->input->post('pid');
		$data = $this->order_process_m->submit_internal_status_by_ship($id, $internal_status,$order_id);
		if($data) {
			echo json_encode($id);
		} else {
			echo false;
		}
	}
	
/* Others Stores Orders */
	public function get_foreign_orders() {
		$config = array();
		$config['base_url'] = base_url().'c_order_process/get_foreign_orders';
		$config['total_rows'] = $this->order_process_m->count_foreign_dispatched_orders();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$data['orders'] = $this->order_process_m->get_all_foreign_dispatched_orders($config['per_page'],$offset);
		
		$data['count_foreign_orders'] = $this->order_process_m->count_foreign_dispatched_orders();
		$this->load->view('order_process/foreign_orders.php', $data);
		
	}
	
	public function search_foreign_orders() {
		$search_order = $this->input->post('search_order');
		$search_store_name = $this->input->post('search_store_name');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_create_date = $this->input->post('search_create_date');
		$search_name = $this->input->post('search_name');
		
		$orders = $this->order_process_m->search_foreign_order($search_order, $search_create_date, $search_name, $search_store_name, $search_internal_status);
		
		$popup = array();
		$int_drop = array();
		$options = array(
			'Cancelled' => 'Cancelled',
			'dispatched'    => 'dispatched'
		);
		foreach($orders as $order) {
			$popup[$order->id] = anchor('',$order->order_id,array('class'=>'topopup', 'id'=>'topopup_'.$order->id));
			
			$int_drop[$order->id] = form_dropdown('int_st_store_name',$options, $order->internal_status,'class=int_st_store_name id=int_st_store_'.$order->id);
		}
		$data['internal'] = $int_drop;
		$data['popup'] = $popup;
		$data['orders'] = $orders;
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
		
	}
	
/* Pending Orders */
	public function get_pending_orders() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/get_pending_orders';
		$config['total_rows'] = $this->order_process_m->count_all_pending_orders();
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$orders = $this->order_process_m->get_all_pending_orders($config['per_page'],$offset);
		$partner_name = array();
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
		}
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		$data['pending_orders'] = $this->order_process_m->count_all_pending_orders();
		$this->load->view('order_process/pending_orders.php', $data);
	}
	
	public function search_pending_orders() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/search_pending_orders';
		$config['total_rows'] = $this->order_process_m->count_all_pending_orders();
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_store = $this->input->post('search_store');
		$search_order = $this->input->post('search_order');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_create_date = $this->input->post('search_create_date');
		$search_name = $this->input->post('search_name');
		
		$orders = $this->order_process_m->search_pending_order($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode,$config['per_page'],$offset);
		
		$partner_name = array();
		$popup = array();
		foreach($orders as $order) {
			
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
			
			$popup[$order->id] = anchor('',$order->order_id,array('class'=>'topopup', 'id'=>'topopup_'.$order->id));
		}
		$data['popup'] = $popup;
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
/*Holded Order*/
	public function get_holded_orders() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/get_holded_orders';
		$config['total_rows'] = $this->order_process_m->count_all_holded_orders();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$orders = $this->order_process_m->get_all_holded_orders($config['per_page'],$offset);
		$partner_name = array();
		foreach($orders as $order) {
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
		}
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		$data['holded_orders'] = $this->order_process_m->count_all_holded_orders();
		$this->load->view('order_process/holded_orders.php', $data);
	}
	
	public function search_holded_orders() {
		
		$search_store = $this->input->post('search_store');
		$search_order = $this->input->post('search_order');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_payment_mode = $this->input->post('search_payment_mode');
		$search_create_date = $this->input->post('search_create_date');
		$search_name = $this->input->post('search_name');
		
		$orders = $this->order_process_m->search_holded_order($search_store, $search_order, $search_create_date, $search_name, $search_sys_status, $search_internal_status, $search_payment_mode);
		
		$partner_name = array();
		$popup = array();
		foreach($orders as $order) {
			
			if($order->store_name=='beautykafe.com') {
				$partner_name[$order->id] = $this->get_courier_partner_name($order->order_id, $order->amount, $order->pincode, $order->payment_mode);
			} else {
				$partner_name[$order->id] = 'Not Available';
			}
			
			$popup[$order->id] = anchor('',$order->order_id,array('class'=>'topopup', 'id'=>'topopup_'.$order->id));
		}
		$data['popup'] = $popup;
		$data['partner_name'] = $partner_name;
		$data['orders'] = $orders;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	
/* order item view */
	public function order_items() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/order_items';
		$config['total_rows'] = $this->db->count_all('order_item_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$items = $this->order_process_m->get_all_order_items($config['per_page'],$offset);
		
		$inventory_item_qty  = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
		}
		
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		$this->load->view('order_process/order_items.php', $data);
	}
	
	public function less_sum_inventory($item_id) {
		
		$data['pro_sum'] = $this->order_process_m->get_procured_item_by_item_id($item_id);
		$data['pur_sum'] = $this->order_process_m->get_purchase_item_by_item_id($item_id);
		$data['memo'] = $this->order_process_m->get_credit_memo_item_by_item_id($item_id);
		if($data) {
			return ($data['pur_sum']-$data['pro_sum']-$data['memo']);
		} else {
			echo false;
		}
	}
	
	public function count_saved_inventory() {
		$item_id = $this->input->post('item_id');
		
		$pro_sum = $this->order_process_m->get_procured_item_by_item_id($item_id);
		$pur_sum = $this->order_process_m->get_purchase_item_by_item_id($item_id);
		$memo = $this->order_process_m->get_credit_memo_item_by_item_id($item_id);
		
		$data['inventory'] = ($pur_sum-$pro_sum-$memo);
		$data['item_id'] = $item_id;
		if($data) {
			echo json_encode($data);
		} else {
			echo true;
		}
	}
	
	public function procured_qty_submit($id) {
		$procured_qty = $this->input->post('text_procured_qty_val');
		$status = $this->input->post('status');
		$order_id = $this->input->post('order_id');
		$item_id = $this->input->post('item_id');
		$data = $this->order_process_m->order_item_procured_qty_submit($procured_qty, $status, $id);
		
		if($data==1) {
			$st['st'] = $this->order_process_m->get_all_order_item_by_order_id($order_id);
			$st['order_id'] = $order_id;
			echo json_encode($st);
		} else {
			echo false;
		}
	}
	
	public function search_order_item() {
	
		$config = array();
		$config['base_url'] = base_url().'c_order_process/search_order_item';
		$config['total_rows'] = $this->db->count_all('order_item_table');
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$search_order_no = $this->input->post('search_order_no');
		$search_store = $this->input->post('search_store');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_sku = $this->input->post('search_sku');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		$search_status = $this->input->post('search_status');
		$search_pro_date = $this->input->post('search_pro_date');
		
		$items = $this->order_process_m->search_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status, $config['per_page'],$offset);
		
		$inventory_item_qty  = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
		}
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function item_comment_submit($id) {
		$comment  = $this->input->post('text_comment_val');
		$data = $this->order_process_m->order_item_comment_submit($comment, $id);
		if($data==1) {
			echo true;
		} else {
			echo false;
		}
	}
	
/* get pending order items */
	public function order_pending_order_items() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/order_pending_order_items';
		$config['total_rows'] = $this->order_process_m->count_total_pending_order_items();
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
	
		$items = $this->order_process_m->get_all_pending_order_items($config['per_page'],$offset);
		$supplier = array();
		$inventory_item_qty  = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
			
			$supplier[$item->item_id] = $this->order_process_m->get_supplier_by_item_brand_id($item->brand_id);
			
			if(!$item->supplier_name) {
				//$this->order_process_m->save_supplier_name_order_item_table_by_id($item->id,max($supplier[$item->item_id]));
				$this->order_process_m->save_supplier_name_order_item_table_by_item_id($item->id,max($supplier[$item->item_id]), $item->item_id);
			}
			
		}
		$data['supplier'] = $supplier;
		$data['pending_qty'] = $this->order_process_m->get_total_pending_item_for_pending_orders();
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		$this->load->view('order_process/pending_order_items.php', $data);
	}
	
	public function search_pending_order_item() {
	
		$config = array();
		$config['base_url'] = base_url().'c_order_process/search_pending_order_item';
		$config['total_rows'] = $this->order_process_m->count_total_pending_order_items();
		$config['per_page'] = 100; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
	
		$search_order_no = $this->input->post('search_order_no');
		$search_store = $this->input->post('search_store');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_sku = $this->input->post('search_sku');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		$search_status = $this->input->post('search_status');
		$search_pro_date = $this->input->post('search_pro_date');
		
		$items = $this->order_process_m->search_pending_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status, $config['per_page'],$offset);
		
		$inventory_item_qty  = array();
		$supplier = array();
		$dropdown = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
			
			$supplier[$item->item_id] = $this->order_process_m->get_supplier_by_item_brand_id($item->brand_id);
			
			if(!$item->supplier_name) {
				//$this->order_process_m->save_supplier_name_order_item_table_by_id($item->id,max($supplier[$item->item_id]));
				$this->order_process_m->save_supplier_name_order_item_table_by_item_id($item->id,max($supplier[$item->item_id]), $item->item_id);
			}
			$class = "suppliers item_supplier_".$item->item_id;
			$dropdown[$item->id] = form_dropdown("supplier",$supplier[$item->item_id], $item->supplier_name, 'class="'.$class.'" id=supplier_'.$item->id);
			
		}
		$data['supplier'] = $dropdown;
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}
	
	public function save_supplier_name_pending_order_item_table($id) {
		$supplier = $this->input->post('supplier');
		$item_id = $this->input->post('item_id');
		$data = $this->order_process_m->save_supplier_name_order_item_table_by_item_id($id,$supplier, $item_id);
		if($data) {
			echo true;
		} else {
			echo false;
		}
	}
	
/* Procured Order items */
	public function order_procured_order_items() {
		
		$config = array();
		$config['base_url'] = base_url().'c_order_process/order_procured_order_items';
		$config['total_rows'] = $this->order_process_m->count_total_procured_order_items();
		$config['per_page'] = 50; 
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
		$config["num_links"] = 5;		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$offset = $page==0 ? 0: ($page-1)*$config["per_page"];

		$data['links'] = $this->pagination->create_links();
		
		$items = $this->order_process_m->get_all_procured_items($config['per_page'],$offset);
		
		$inventory_item_qty  = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
		}
		
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		$this->load->view('order_process/procured_order_items.php', $data);
	}
	
	public function search_procured_order_item() {
		
		$search_order_no = $this->input->post('search_order_no');
		$search_store = $this->input->post('search_store');
		$search_sys_status = $this->input->post('search_sys_status');
		$search_internal_status = $this->input->post('search_internal_status');
		$search_sku = $this->input->post('search_sku');
		$search_name = $this->input->post('search_name');
		$search_brand = $this->input->post('search_brand');
		$search_status = $this->input->post('search_status');
		$search_pro_date = $this->input->post('search_pro_date');
		
		$items = $this->order_process_m->search_procured_order_items($search_store, $search_order_no, $search_sku, $search_name, $search_brand, $search_status, $search_pro_date, $search_sys_status, $search_internal_status);
		
		$inventory_item_qty  = array();
		foreach($items as $item) {
			$inventory_item_qty[$item->item_id] = $this->less_sum_inventory($item->item_id);
		}
		$data['inventory_item_qty'] = $inventory_item_qty;
		$data['items'] = $items;
		
		if($data) {
			echo json_encode($data);
		} else {
			echo false;
		}
	}

/* export_excel_pending_order_items */

	public function export_excel_pending_order_items() {
		$data['items'] = $this->order_process_m->export_excel_pending_order_items();
		
		$this->load->view('order_process/export_pending_order_items.php', $data);
	}
	
	public function pending_orders_send_mail() {
		$suppliers = $this->order_process_m->export_excel_pending_order_items();
		$emails = array();
		foreach($suppliers as $supplier){
			$id = $supplier->id;
			$supp_comment = $supplier->supp_comment;
			$supplier_name = $supplier->supplier_name;
			$email_to = $supplier->email;
			$emails[$supplier_name] = $email_to;
			$update_comment = $this->order_process_m->update_supplier_name_order_item_comment($id,$supplier_name,$supp_comment);
		}
		foreach ($emails as $e_name => $e_mail) {
			if($e_name!='0' || $e_name!='none') {
				$data['items'] = $this->order_process_m->email_pending_order_items_to_send_by_supplier($e_name);
				$data['type'] = 'new';
				$data['email'] = $e_mail;
				$config = array ('mailtype' => 'html','charset'  => 'utf-8','priority' => '1');
				$this->email->initialize($config);
				$this->email->from( 'help@beautykafe.com', 'Beautykafe.com' );
				$this->email->to($e_mail);
				$this->email->cc('suraj@beautykafe.com,gaurav@beautykafe.com,ajay@beautykafe.com,sawan@beautykafe.com'); 
				//$this->email->bcc('them@their-example.com'); 
				$this->email->subject('Beautykafe.com order '.date("d/m/Y"). ' - ' .$e_name);
				$this->email->message( $this->load->view( 'order_process/send_pending_order_items_mail.php', $data, true ) );
				$this->email->send();
				
			} else {
				$data['items'] = $this->order_process_m->email_pending_order_items_to_send_by_supplier(null);
				$data['type'] = 'none';
				$data['email'] = $e_mail;
				$config = array ('mailtype' => 'html','charset'  => 'utf-8','priority' => '1');
				$this->email->initialize($config);
				$this->email->from( 'suraj@beautykafe.com', 'Beautykafe.com' );
				$this->email->to( 'suraj@beautykafe.com' );
				$this->email->cc('gaurav@beautykafe.com,ajay@beautykafe.com,sawan@beautykafe.com'); 
				//$this->email->bcc('them@their-example.com'); 
				$this->email->subject(date("d/m/Y").'- Order- none');
				
				$this->email->message( $this->load->view( 'order_process/send_pending_order_items_mail.php', $data, true ) );
				$this->email->send();
			}
			echo true;
		}
		echo false;
	}
	
	
	
/*Calculating COD & FRIEGHT Charge and get courier name*/
	
	public function get_courier_partner_name($order_id, $shipment_value, $pincode, $payement_mode) {
		//$payement_mode = $this->input->post('payement_mode');
		//$pincode = $this->input->post('pincode');
		//$shipment_value = $this->input->post('shipment_value');
		
		$weight = $this->order_process_m->get_order_item_weight($order_id);
		
		$data = $this->order_process_m->get_all_partners($payement_mode, $pincode);
		
		$options = array();
		$flag = 0;
		if($data) {
			foreach($data as $key) {
				if($key->rating >= 3){
					if($flag == 0){
						$options = array();
					}
					$options[] =  array('name'=>$key->partner, 'zone'=>$key->zone, 'rating'=> $key->rating);
					$flag = 1;
					
				}else if ($flag == 0){
					if(isset($options[0])){
						if($options[0]['rating'] >= $key->rating)
							continue;
					}
					$options[0] = array('name'=>$key->partner, 'zone'=>$key->zone,'rating'=> $key->rating);
					
				}
			}
			
			if(sizeof($options) >0) {
				$charge = array();
				if($payement_mode=='cod') {
					$cod_charges = $this->order_process_m->get_cod_charges($options);
					foreach($cod_charges as $key) {
						$max = max(($shipment_value*$key->charges)/100, $key->min);
						$charge[$key->partner] = $max;
					}
				}
				$fr_charges = $this->order_process_m->get_freight_charges($options);
				
				$charges;
				if($fr_charges) {
					foreach($fr_charges as $key) {
						$base = $key->base_charge;
						$mult = 0;
						if($weight-$key->base_weight >0 ){
							$mult = ceil(($weight-$key->base_weight)/($key->multiplier_weight));
						}
						$multiplier = $mult*$key->multiplier_charge;
						if($payement_mode!='cod'){
							$charge[$key->partner] = 0;
						}
						$charge[$key->partner] += ceil($base + $multiplier);
						
					}
					$charges = array_search(min($charge), $charge);
				} else {
					$charges = array();
					$charges = 'Not Available';
				}
				return $charges;
			}
		}
		else {
			$charges = array();
			$charges = 'Not Available';
			return $charges;
		}
	}
	
	
}